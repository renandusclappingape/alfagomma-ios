//
//  RandomString.swift
//  Alfagomma
//
//  Created by Clapping Ape on 07/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation
import UIKit

struct Keychain {
    
    // MARK: - Device Identifier Keychain Method
    func deviceIdentifier() -> String {
        var deviceId: String! = self.requestDeviceIdentifierFromKeychain()
        print("DEVICE ID FROM KEYCHAIN: \(deviceId)")
        
        if deviceId == "" {
            deviceId = self.createRandomIdentifier()
            self.saveDeviceIdentifierFromKeychain(deviceIdentifier: deviceId)
        }
        
        return deviceId
    }
    
    func createRandomIdentifier() -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0..<20 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM-dd-yy-hh-mm-ss"
        let now = dateformatter.string(from: NSDate() as Date)
        
        randomString += now
        randomString += UIDevice.current.identifierForVendor?.uuidString ?? ""
        
        return randomString
    }
    
    func requestDeviceIdentifierFromKeychain() -> String {
        let dictionary : [String: Any] = [kSecClass as String : kSecClassGenericPassword,
                                          kSecAttrService as String : "trigo",
                                          kSecAttrAccount as String : "trigo",
                                          kSecReturnData as String : kCFBooleanTrue,
                                          kSecMatchLimit as String : kSecMatchLimitOne]
        
        var result : AnyObject?
        let err: OSStatus = SecItemCopyMatching(dictionary as CFDictionary, &result)
        if (err == errSecSuccess) {
            // on success cast the result to a dictionary and extract the
            // username and password from the dictionary.
            if let retrievedData = result as? NSData {
                return NSString(data: retrievedData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            }
        } else {
            // probably a program error,
            // print and lookup err code (e.g., -50 = bad parameter)
            
            return ""
        }
        
        return ""
    }
    
    func saveDeviceIdentifierFromKeychain(deviceIdentifier: String) {
        let data: Data = deviceIdentifier.data(using: String.Encoding.utf8)!
        let dictionary : [String: Any] = [kSecClass as String : kSecClassGenericPassword,
                                          kSecAttrService as String : "trigo",
                                          kSecAttrAccount as String : "trigo",
                                          kSecValueData as String : data]
        
        SecItemAdd(dictionary as CFDictionary, nil)
    }
    
}
