//
//  TimeStampConverter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 29/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

func convertUnixTimeStamp(timestamp: Int) -> Date{
    let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
    return date
}

func convertDateToUnixTimeStamp(date: Date) -> Int{
    return Int(date.timeIntervalSince1970)
}
