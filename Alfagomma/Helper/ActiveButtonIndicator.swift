//
//  ActiveButtonIndicator.swift
//  Alfagomma
//
//  Created by Clapping Ape on 28/03/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ActiveButtonIndicator: NSObject {

    var viewIndicator : UIView!
    
    override init() {
        super.init()
        self.createView()
    }
    
    func createView() {
        viewIndicator = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 2))
        viewIndicator.backgroundColor = .red
    }
    
    func addToView(view: UIView) {
        viewIndicator.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y+view.frame.size.height-2, width: view.frame.size.width, height: 2)
        view.superview?.addSubview(viewIndicator)
    }
    
    func moveToView(view: UIView) {
        UIView.animate(withDuration: 0.2) {
            self.viewIndicator.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y+view.frame.size.height-2, width: view.frame.size.width, height: 2)
        }
    }
}
