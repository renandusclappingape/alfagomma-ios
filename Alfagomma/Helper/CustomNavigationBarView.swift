//
//  CustomNavigationBarView.swift
//  Alfagomma
//
//  Created by Clapping Ape on 06/04/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation
import UIKit

class CustomNavigationBarView: UIView {
    
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
}
