//
//  UserLocation.swift
//  Alfagomma
//
//  Created by Clapping Ape on 29/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation
import CoreLocation

class UserLocation: NSObject, CLLocationManagerDelegate{
    
    var locationManager = CLLocationManager()
    var userLocation : CLLocation? = nil
    
    override init() {
        super.init()
        self.getUserLocation()
    }
    static let shared = UserLocation()
    
    func getUserLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            self.userLocation = location
        }
    }
}

