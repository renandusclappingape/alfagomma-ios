//
//  SearchTextField.swift
//  Alfagomma
//
//  Created by Clapping Ape on 23/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation
import UIKit

protocol SearchFieldDelegate : NSObjectProtocol {
    func doSearchWithKeyword(keyword : String)
}

class SearchTextField: NSObject, UITextFieldDelegate {
    
    weak fileprivate var searchDelegate : SearchFieldDelegate?
    
    private override init() {}
    static let shared = SearchTextField()
    
    var text : String? = ""
    var activeViewController : UIViewController?
    var isDoingSearch : Bool = false
    
    func setActiveViewController(viewController: UIViewController!){
        if viewController != nil{
            self.activeViewController = viewController
        }
    }
    
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
//        self.isDoingSearch = true
        textField.resignFirstResponder()
        
        let storyboard = UIStoryboard(name: "SearchProduct", bundle: nil)
        let searchVC = storyboard.instantiateViewController(withIdentifier: "SearchProductViewController")
        self.activeViewController?.navigationController?.pushViewController(searchVC, animated: true)
        
        if (self.activeViewController?.navigationController?.presentingViewController as? SearchProductViewController) != nil {
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        self.text = updatedString
        
        return true
    }
}
