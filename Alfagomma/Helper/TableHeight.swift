//
//  TableHeight.swift
//  Alfagomma
//
//  Created by Clapping Ape on 07/04/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func refreshTableHeight(heightConstraint: NSLayoutConstraint, viewController: UIViewController) {
        self.layoutIfNeeded()
        heightConstraint.constant = self.contentSize.height
        viewController.view.layoutIfNeeded()
    }
}
