//
//  APIManager.swift
//  iOS-Base
//
//  Created by Lutfi Azhar on 8/24/17.
//  Copyright © 2017 Lutfi Azhar. All rights reserved.
//

import UIKit
import Alamofire

class APIManager {
    
    private init() {}
    static let sharedInstance = APIManager()
    
    typealias SuccessHandlerObject = (_ responseObject: [String: Any]) -> Void
    typealias SuccessHandlerArray = (_ responseObject: Array<Any>) -> Void
    typealias FailHandler = (_ errorMessage: String) -> Void
    
    
    // MARK: - GET
    func GETAPIWithResponseObject(route: String,parameter: [String: Any]!,headers: [String:String]? = nil, successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headersDynamic : [String:String] = [
            "Content-Type": "application/json"
        ]
        
        if headers != nil {
            headersDynamic = headersDynamic.merging(headers!, uniquingKeysWith: { (_, last) in last })
        }
        
        Alamofire.request(Constant.BaseURL+route, method: .get, parameters: parameter, encoding: JSONEncoding.default, headers: headersDynamic).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            
            switch response.result {
            case .success:
                print("RESPONSE: \(response)")
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                
                if  let metaResponse : [String:Any] = responseJSON["meta"] as? [String : Any] {
                    let status = metaResponse["status"] as! Int
                    let code = metaResponse["code"] as! Int
                    if status == 0 {
                        if code == 401{
                                NotificationCenter.default.post(name: Notification.Name("logoutNotification"), object: nil)
                            failureBlock(metaResponse["message"] as! String)
                        }
                        else{
                            if metaResponse["message"] != nil {
                                failureBlock(metaResponse["message"] as! String)
                            }else{
                                failureBlock("Unknown Error")
                            }
                        }
                    }else{
                        successBlock(responseJSON)
                    }
                }else {
                    successBlock(responseJSON)
                }
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func GETAPIWithResponseArray(route: String, successBlock:@escaping SuccessHandlerArray, failureBlock:@escaping FailHandler){
        
        Alamofire.request(Constant.BaseURL+route).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            
            switch response.result {
            case .success:
                if ((response.result.value as? Array<Any>) != nil) {
                    let responseJSON = response.result.value as! Array<Any>
                    
                    print("RESPONSE.BODY: \(responseJSON)")
                    
                    successBlock(responseJSON)
                }else{
                    failureBlock("Error, Response is an Object")
                }
                
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    
    
    // MARK: - POST
//    func POSTAPIWithResponseObject(route: String, parameter: [String: Any], successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
//        
//        Alamofire.request(Constant.BaseURL+route, method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { response in
//            
//            print("REQUEST.ROUTE: \(route)")
//            print("REQUEST.BODY: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))")
//            
//            switch response.result {
//            case .success:
//                let responseJSON = response.result.value as! [String:Any]
//                
//                print("RESPONSE.BODY: \(responseJSON)")
//                
//                if responseJSON["status"] != nil {
//                    let status = responseJSON["status"] as! Bool
//                    if status == false {
//                        if responseJSON["message"] != nil {
//                            failureBlock(responseJSON["message"] as! String)
//                        }else{
//                            failureBlock("Unknown Error")
//                        }
//                    }else{
//                        successBlock(responseJSON)
//                    }
//                }else {
//                    failureBlock("Error with undefined status")
//                }
//                break
//                
//            case .failure(let error):
//                print("ERROR: \(error)")
//                failureBlock((response.error?.localizedDescription)!)
//                break
//            }
//        }
//    }
    
    func POSTAPIWithResponseObject(route: String, parameter: [String: Any]? = nil,headers: [String:String]? = nil, successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headersDynamic : [String:String] = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "App-Key" : Constant.AppKey
        ]
        
        if headers != nil {
            headersDynamic = headersDynamic.merging(headers!, uniquingKeysWith: { (_, last) in last })
        }
        
        Alamofire.request(Constant.BaseURL+route, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headersDynamic).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("REQUEST.BODY: \(String(describing: NSString(data: (response.request?.httpBody ?? NSData() as Data), encoding: String.Encoding.utf8.rawValue)))")
            print("HTTP RESPONSE CODE: \(String(describing: response.response?.statusCode))")
            
            switch response.result {
            case .success:
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                
                if  let metaResponse : [String:Any] = responseJSON["meta"] as? [String : Any] {
                    let status = metaResponse["status"] as! Int
                    let code = metaResponse["code"] as! Int
                    if status == 0 {
                        if code == 401{
                            NotificationCenter.default.post(name: Notification.Name("logoutNotification"), object: nil)
                            failureBlock(metaResponse["message"] as! String)
                        }
                        else{
                            if metaResponse["message"] != nil {
                                failureBlock(metaResponse["message"] as! String)
                            }else{
                                failureBlock("Unknown Error")
                            }
                        }
                    }else{
                        successBlock(responseJSON)
                    }
                }else {
                    successBlock(responseJSON)
                }
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    func POSTAPIWithResponseArray(route: String, parameter: [String: Any], successBlock:@escaping SuccessHandlerArray, failureBlock:@escaping FailHandler){
        
        Alamofire.request(Constant.BaseURL+route, method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("REQUEST.BODY: \(String(describing: NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue)))")
            
            switch response.result {
            case .success:
                if ((response.result.value as? Array<Any>) != nil) {
                    let responseJSON = response.result.value as! Array<Any>
                    print("RESPONSE.BODY: \(responseJSON)")
                    successBlock(responseJSON)
                }else{
                    failureBlock("Error, Response is an Object")
                }
                
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
    
    
    func DELETEAPIWithResponseObject(route: String, parameter: [String: Any]? = nil,headers: [String:String]? = nil, successBlock:@escaping SuccessHandlerObject, failureBlock:@escaping FailHandler){
        
        var headersDynamic : [String:String] = [
            "App-Key" : Constant.AppKey,
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if headers != nil {
            headersDynamic = headersDynamic.merging(headers!, uniquingKeysWith: { (_, last) in last })
        }
        
        Alamofire.request(Constant.BaseURL+route, method: .delete, parameters: parameter, encoding: JSONEncoding.default, headers: headersDynamic).responseJSON { response in
            
            print("REQUEST.ROUTE: \(route)")
            print("REQUEST.BODY: \(String(describing: NSString(data: (response.request?.httpBody ?? NSData() as Data), encoding: String.Encoding.utf8.rawValue)))")
            print("HTTP RESPONSE CODE: \(String(describing: response.response?.statusCode))")
            
            switch response.result {
            case .success:
                let responseJSON = response.result.value as! [String:Any]
                
                print("RESPONSE.BODY: \(responseJSON)")
                
                if  let metaResponse : [String:Any] = responseJSON["meta"] as? [String : Any] {
                    let status = metaResponse["status"] as! Int
                    let code = metaResponse["code"] as! Int
                    if status == 0 {
                        if code == 401{
                            NotificationCenter.default.post(name: Notification.Name("logoutNotification"), object: nil)
                            failureBlock(metaResponse["message"] as! String)
                        }
                        else{
                            if metaResponse["message"] != nil {
                                failureBlock(metaResponse["message"] as! String)
                            }else{
                                failureBlock("Unknown Error")
                            }
                        }
                    }else{
                        successBlock(responseJSON)
                    }
                }else {
                    successBlock(responseJSON)
                }
                break
                
            case .failure(let error):
                print("ERROR: \(error)")
                failureBlock((response.error?.localizedDescription)!)
                break
            }
        }
    }
}
