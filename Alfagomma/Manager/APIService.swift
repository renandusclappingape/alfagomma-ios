//
//  APIService.swift
//  iOS-Base
//
//  Created by Lutfi Azhar on 11/2/17.
//  Copyright © 2017 Lutfi Azhar. All rights reserved.
//

import Foundation

class APIService {
    
    private init() {}
    static let sharedInstance = APIService()
    
    // Mark: Auth
    func tokenRequestAPI(callBack: @escaping (User) -> Void, message: @escaping (String) -> Void) {
        let params : [String:Any] = [ "grant_type": "password",
                                      "username": "clappingape",
                                      "password": "password1234"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteAuth,
            parameter: params,headers: nil,
            successBlock: { (responseObject: [String : Any]) in
                
                let userShared = User.shared
                userShared.setToken(data: responseObject)
                
                
                callBack(userShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func refreshTokenRequestAPI(callBack: @escaping (User) -> Void,retryBlock: (() -> Swift.Void)? = nil, message: @escaping (String) -> Void) {
        let params : [String:Any] = [ "grant_type": "refresh",
                                      "refresh_token": User.shared.access_token]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteRefreshToken,
            parameter: params,headers: nil,
            successBlock: { (responseObject: [String : Any]) in
                
                let userShared = User.shared
                userShared.setToken(data: responseObject)
                callBack(userShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // Mark: Login
    func loginAPI(email : String, password: String ,callBack: @escaping (User) -> Void, message: @escaping (String) -> Void) {
        let params : [String:Any] = [ "email": email,
                                      "password": password,
                                      "firebase_token": User.shared.firebase_token]
        
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteLogin,
            parameter: params,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let userShared = User.shared
                userShared.setData(data: responseObject)
                callBack(userShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func forgotPasswordAPI(email: String,callBack: @escaping () -> Void, message: @escaping (String) -> Void) {
        let params : [String:Any] = [ "email": email ]
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteForgotPassword,
            parameter: params,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                callBack()
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Location
    func LocationRequestAPI(callBack: @escaping (Location) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: Constant.RouteLocation,
            parameter: nil,headers: header,
            successBlock: { (responseObject: [String : Any]) in
        
                let locationShared = Location.shared
                locationShared.location_list = []
                locationShared.setData(data: responseObject)
                callBack(locationShared)
                
        }) { (errorMessage: String) in

            message(errorMessage)

        }
    }
    
    // MARK: My Product
    func MyProductRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (ProductList) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteMyProduct,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = ProductList.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Search Product
    func SearchProductRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (ProductList) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteSearchProduct,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = ProductList.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Banner
    
    func BannerRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (BannerList) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: Constant.RouteBanner,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let bannerShared = BannerList.init(data: responseObject)
                callBack(bannerShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Profile
    func ProfileRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (User) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: "\(Constant.RouteGetProfile)\(User.shared.id!)",
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                User.shared.setData(data: responseObject)
                callBack(User.shared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    
    func updateProfileAPI(params: [String:Any],callBack: @escaping () -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteUpdateProfile,
            parameter: params,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                callBack()
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func ChangePasswordAPI(params: [String:Any],callBack: @escaping () -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteChangePassword,
            parameter: params,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                callBack()
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    
    // MARK: Sort
    
    func SortRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (SortList) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: Constant.RouteSort,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let sortShared = SortList.init(data: responseObject)
                callBack(sortShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Filter
    
    func FilterLocationRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (FilterList) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: Constant.RouteFilterLocation,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let filterShared = FilterList.init(data: responseObject)
                callBack(filterShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func FilterCategoryRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (FilterList) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: Constant.RouteFilterCategory,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let filterShared = FilterList.init(data: responseObject)
                callBack(filterShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Product
    func ProductDetailsRequestAPI(id:String! ,parameters: [String:Any]? = nil, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.GETAPIWithResponseObject(
            route: "\(Constant.RouteSearchProductDetails)\(id!)",
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func SelectProductLocationAPI(parameters: [String:Any]? = nil, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteSelectProductLocation,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func SetProductServiceAPI(parameters: [String:Any]!, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteSetProductService,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func SetProductNoteAPI(parameters: [String:Any]? = nil, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteAddNote,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func AddProductServiceAPI(parameters: [String:Any]!, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteAddProduct,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func UpdateProductServiceAPI(parameters: [String:Any]!, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteUpdateProduct,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    func ClaimProductServiceAPI(parameters: [String:Any]!, callBack: @escaping (Product) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteClaimProduct,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = Product.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Calendar
    func CalendarRequestAPI(parameters: [String:Any]? = nil, callBack: @escaping (CalendarProduct) -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteCalendar,
            parameter: parameters,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                let productShared = CalendarProduct.init(data: responseObject)
                callBack(productShared)
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
    
    // MARK: Contact us
    func ContactAPI(params: [String:Any],callBack: @escaping () -> Void, message: @escaping (String) -> Void) {
        let header = ["Authorization":"Bearer \(User.shared.access_token!)"]
        
        APIManager.sharedInstance.POSTAPIWithResponseObject(
            route: Constant.RouteContactUs,
            parameter: params,headers: header,
            successBlock: { (responseObject: [String : Any]) in
                
                callBack()
                
        }) { (errorMessage: String) in
            
            message(errorMessage)
            
        }
    }
}
