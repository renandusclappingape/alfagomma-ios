//
//  Color.swift
//  Alfagomma
//
//  Created by Clapping Ape on 28/03/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

struct Color {
    static let orange = UIColor.init(red: 255.0/255.0, green: 204.0/255.0, blue: 51.0/255.0, alpha: 1)
    static let darkBlue = UIColor.init(red: 6.0/255.0, green: 37.0/255.0, blue: 74.0/255.0, alpha: 1)
    static let redErrorColor = UIColor.init(red: 236.0/255.0, green: 29.0/255.0, blue: 36/255.0, alpha: 1)
    
}

extension UIColor {

    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (r, g, b, a) = ((int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17, 255)
        case 6: // RGB (24-bit)
            (r, g, b, a) = (int >> 16, int >> 8 & 0xFF, int & 0xFF, 255)
        case 8: // ARGB (32-bit)
            (r, g, b, a) = (int >> 24 & 0xFF, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
        
    }
    
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
