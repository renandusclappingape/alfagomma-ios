//
//  Constant.swift
//  iOS-Base
//
//  Created by Lutfi Azhar on 8/24/17.
//  Copyright © 2017 Lutfi Azhar. All rights reserved.
//

import UIKit

struct Constant {
    
    private struct Domains {
        static let Development = "http://alfagomma.herokuapp.com"
        static let Production = "http://178.128.57.23"
    }
    
    private struct Routes {
        static let Login = "/v1/auth/login"
        static let Auth = "/v1/oauth/token/request"
        static let RefreshToken = "/v1/oauth/token/refresh"
        static let ForgotPassword = "/v1/auth/forgot_password"
        
        static let Banner = "/v1/banners/"
        
        static let Location = "/v1/country"
        static let MyProduct = "/v1/product/myproduct"
        static let SearchProduct = "/v1/product/search"
        static let ProductDetail = "/v1/product/"
        static let SelectProductLocation = "/v1/product/set-location"
        static let SetProductService = "/v1/product/set-service"
        static let AddNote = "/v1/product/add-notes"
        static let AddProduct = "/v1/product/add"
        static let UpdateProduct = "/v1/product/update"
        static let ClaimProduct = "/v1/product/claim"
        
        static let GetProfile = "/v1/profile/"
        static let UpdateProfile = "/v1/profile/update"
        static let ChangePassword = "/v1/profile/change-password"
        
        static let Sort = "/v1/sort"
        
        static let FilterCategory = "/v1/category"
        static let FilterLocation = "/v1/country"
        
        static let Calendar = "/v1/calendar"
        
        static let ContactUs = "/v1/contact-us"
    }
    
    struct Notification {
        static let googleSignIn = "googleSignInNotification"
    }
    
    static let LocaleIdentifier = "id_ID"
    static let AppKey = "mobile|mandiriclick"
    static let GoogleAPIKey = "AIzaSyB7aklQRjxcLcf4IqdSH2EuHQlshWkvcDE"
    
    static let BaseURL = Domains.Production
    static let RouteLogin = Routes.Login
    static let RouteAuth = Routes.Auth
    static let RouteRefreshToken = Routes.RefreshToken
    static let RouteForgotPassword = Routes.ForgotPassword
    
    static let RouteBanner = Routes.Banner
    
    static let RouteLocation = Routes.Location
    static let RouteMyProduct = Routes.MyProduct
    static let RouteSearchProduct = Routes.SearchProduct
    static let RouteSearchProductDetails = Routes.ProductDetail
    static let RouteSelectProductLocation = Routes.SelectProductLocation
    static let RouteSetProductService = Routes.SetProductService
    static let RouteAddNote = Routes.AddNote
    static let RouteAddProduct = Routes.AddProduct
    static let RouteUpdateProduct = Routes.UpdateProduct
    static let RouteClaimProduct = Routes.ClaimProduct
    
    static let RouteGetProfile = Routes.GetProfile
    static let RouteUpdateProfile = Routes.UpdateProfile
    static let RouteChangePassword = Routes.ChangePassword
    
    static let RouteSort = Routes.Sort
    
    static let RouteFilterCategory = Routes.FilterCategory
    static let RouteFilterLocation = Routes.FilterLocation
    
    static let RouteCalendar = Routes.Calendar
    static let RouteContactUs = Routes.ContactUs
}



