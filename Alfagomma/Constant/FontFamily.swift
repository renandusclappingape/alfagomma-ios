//
//  FontFamily.swift
//  Alfagomma
//
//  Created by Clapping Ape on 06/04/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct FontFamilyName {
    static let ClanOTBlack = "ClanOT-Black"
    static let ClanOTBold = "ClanOT-Bold"
    static let ClanOTBook = "ClanOT-Book"
    static let ClanOTMedium = "ClanOT-Medium"
    static let ClanOTNews = "ClanOT-News"
    static let ClanOTThin = "ClanOT-Thin"
    static let ClanOTUltra = "ClanOT-Ultra"
    static let clanpronews = "clanpro-news"
}
