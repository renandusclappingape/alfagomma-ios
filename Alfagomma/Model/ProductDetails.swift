//
//  ProductDetails.swift
//  Alfagomma
//
//  Created by Clapping Ape on 26/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class Product {
    
    var id : String!
    var number : String!
    var title : String!
    var qr_image : String!
    var length : length_object!
    var hwp_psi : String!
    var hwp_bar : String!
    var hwp_id : hwp_id_object!
    var product_images : [product_images_object]? = []
    var service : service_object!
    var location : location_object!
    var notes : [notes_object]? = []
    var work_order_number : String!
    var service_date : String!
    var retest_date : String!
    var first_scan_date : String!
    var product_operator : String!
    var comments : String!
    var sales_order_number : String!
    var stock_code : String!
    var assembled_date : String!
    var workshop : String!
    var description : String!
    var service_type : String!
    var overall_length_mm : String!
    var nominal_bore : String!
    var max_working_pressure : String!
    var test_pressure : String!
    var orientation_1 : String!
    var orientation_2 : String!
    var retention_1 : String!
    var retention_2 : String!
    var end_1 : String!
    var end_2 : String!
    var customer_name : String!
    var customer_PO : String!
    var customers_stock_code : String!
    
    
    init(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let productData : [String:Any] = returnData["product"] as? [String : Any] ?? [:]
        
        
        var productID = ""
        
        if let idInt = productData["id"] as? Int{
            productID = String(idInt)
        }
        else{
            productID = productData["id"] as? String ?? ""
        }
        self.id = productID
        self.number = productData["number"] as? String ?? ""
        self.title = productData["title"] as? String ?? ""
        self.qr_image = productData["qr_image"] as? String ?? ""
        
        let lengthObject = productData["length"] as? [String:Any] ?? [:]
        var length_value_string = ""
        
        if let length_value_Int = lengthObject["value"] as? Int{
            length_value_string = String(length_value_Int)
        }
        else{
            length_value_string = lengthObject["value"] as? String ?? ""
        }
        self.length = length_object(value: length_value_string, unit: lengthObject["unit"] as? String ?? "")
        
        var hwp_psi_string = ""
        
        if let hwp_psi_Int = productData["hwp_psi"] as? Int{
            hwp_psi_string = String(hwp_psi_Int)
        }
        else{
            hwp_psi_string = productData["hwp_psi"] as? String ?? ""
        }
        self.hwp_psi = hwp_psi_string
        
        var hwp_bar_string = ""
        
        if let hwp_bar_Int = productData["hwp_bar"] as? Int{
            hwp_bar_string = String(hwp_bar_Int)
        }
        else{
            hwp_bar_string = productData["hwp_bar"] as? String ?? ""
        }
        self.hwp_bar = hwp_bar_string
        
        let hwpidObject = productData["hwp_id"] as? [String:Any] ?? [:]
        var hwpid_value_string = ""
        
        if let hwpid_value_Int = hwpidObject["value"] as? Int{
            hwpid_value_string = String(hwpid_value_Int)
        }
        else{
            hwpid_value_string = hwpidObject["value"] as? String ?? ""
        }
        self.hwp_id = hwp_id_object(value: hwpid_value_string, unit: hwpidObject["unit"] as? String ?? "")
        
        let imagesArray : [[String:Any]] = productData["product_images"] as? [[String:Any]] ?? []
        
        for image in imagesArray{
            self.product_images?.append(product_images_object(image: image["image"] as? String ?? "", image_thumb: image["image_thumb"] as? String ?? ""))
        }
        
        let serviceObject = productData["service"] as? [String:Any] ?? [:]
        let immediateServiceObject = serviceObject["immediate_service"] as? [String:Any] ?? [:]
        self.service = service_object(last_service: serviceObject["last_service"] as? Int ?? 0, next_service: serviceObject["next_service"] as? Int ?? 0, service_info: serviceObject["service_info"] as? String ?? "", maintenance_info: serviceObject["maintenance_info"] as? String ?? "", immediate_service: immediate_service_object(status: immediateServiceObject["status"] as? Bool ?? false, date: immediateServiceObject["date"] as? Int ?? 0, notes: immediateServiceObject["notes"] as? String ?? ""))
        
        let locationObject = productData["location"] as? [String:Any] ?? [:]
        var latitude_value_double = 0.0
        
        if var latitude_value_string = locationObject["latitude"] as? String{
            latitude_value_string = latitude_value_string.replacingOccurrences(of: ",", with: ".")
            latitude_value_double = Double(latitude_value_string) ?? 0.0
        }
        else{
            latitude_value_double = locationObject["latitude"] as? Double ?? 0
        }
        var longitude_value_double = 0.0
        
        if var longitude_value_string = locationObject["longitude"] as? String{
            longitude_value_string = longitude_value_string.replacingOccurrences(of: ",", with: ".")
            longitude_value_double = Double(longitude_value_string)!
        }
        else{
            longitude_value_double = locationObject["longitude"] as? Double ?? 0
        }
        
        var countryID = ""
        
        if let countryidInt = locationObject["country_id"] as? Int{
            countryID = String(countryidInt)
        }
        else{
            countryID = locationObject["country_id"] as? String ?? ""
        }
        self.location = location_object(country_id: countryID, latitude: latitude_value_double, longitude: longitude_value_double, address: locationObject["address"] as? String ?? "")
        
        let notesArray : [[String:Any]] = productData["notes"] as? [[String:Any]] ?? []
        
        for note in notesArray{
            self.notes?.append(notes_object(photos: note["photos"] as? [String] ?? [], description: note["description"] as? String ?? ""))
        }
        
        self.work_order_number = "\(productData["work_order_number"] as? String ?? "-")"
        self.service_date = "\(productData["service_date"] as? String ?? "-")"
        self.retest_date = "\(productData["retest_date"] as? String ?? "-")"
        self.first_scan_date = "\(productData["first_scan_date"] as? String ?? "-")"
        self.product_operator = "\(productData["product_operator"] as? String ?? "-")"
        self.comments = "\(productData["comments"] as? String ?? "-")"
        
        if (productData["sales_order_number"] as? Int) == nil {
            self.sales_order_number = "-"
        }
        else{
            self.sales_order_number = "\(productData["sales_order_number"] as? Int ?? 0)"
        }
        
        self.stock_code = "\(productData["stock_code"] as? String ?? "-")"
        self.assembled_date = "\(productData["assembled_date"] as? String ?? "-")"
        self.workshop = "\(productData["workshop"] as? String ?? "-")"
        self.description = "\(productData["description"] as? String ?? "-")"
        self.service_type = "\(productData["service_type"] as? String ?? "-")"
        
        if (productData["overall_length_mm"] as? Int) == nil{
            self.overall_length_mm = "-"
        }
        else{
            self.overall_length_mm = "\(productData["overall_length_mm"] as? Int ?? 0)"
        }
        
        self.overall_length_mm = "\(productData["overall_length_mm"] as? String ?? "-")"
        self.nominal_bore = "\(productData["nominal_bore"] as? String ?? "-")"
        self.max_working_pressure = "\(productData["max_working_pressure"] as? String ?? "-")"
        self.test_pressure = "\(productData["test_pressure"] as? String ?? "-")"
        
        if (productData["orientation_1"] as? Int) == nil {
            self.orientation_1 = "-"
        }
        else{
            self.orientation_1 = "\(productData["orientation_1"] as? Int ?? 0)"
        }
        
        if (productData["orientation_2"] as? Int) == nil {
            self.orientation_2 = "-"
        }
        else{
            self.orientation_2 = "\(productData["orientation_2"] as? Int ?? 0)"
        }
        
        self.retention_1 = "\(productData["retention_1"] as? String ?? "-")"
        self.retention_2 = "\(productData["retention_2"] as? String ?? "-")"
        self.end_1 = "\(productData["end_1"] as? String ?? "-")"
        self.end_2 = "\(productData["end_2"] as? String ?? "-")"
        self.customer_name = "\(productData["customer_name"] as? String ?? "-")"
        self.customer_PO = "\(productData["customer_PO"] as? String ?? "-")"
        self.customers_stock_code = "\(productData["customers_stock_code"] as? String ?? "-")"
    }
    
    struct length_object {
        var value : String!
        var unit : String!
    }
    
    struct hwp_id_object {
        var value : String!
        var unit : String!
    }
    
    struct product_images_object {
        var image : String? = ""
        var image_thumb : String? = ""
    }
    
    struct service_object {
        var last_service : Int!
        var next_service : Int!
        var service_info : String!
        var maintenance_info : String!
        var immediate_service : immediate_service_object!
    }
        
    struct immediate_service_object {
        var status : Bool!
        var date : Int!
        var notes : String!
    }
    
    struct location_object{
        var country_id : String!
        var latitude : Double!
        var longitude : Double!
        var address : String!
    }
    
    struct notes_object {
        var photos : [String]!
        var description : String!
    }
    
}
