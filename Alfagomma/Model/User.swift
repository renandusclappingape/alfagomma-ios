//
//  User.swift
//  TAMS
//
//  Created by Clapping Ape on 18/04/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class User {
    
    private init() {}
    static let shared = User()
    
    var access_token : String! = UserDefaults.standard.string(forKey: "access_token") ?? ""
    var refresh_token : String! = UserDefaults.standard.string(forKey: "refresh_token") ?? ""
    var token_type : String! = UserDefaults.standard.string(forKey: "token_type") ?? ""
    var expires_in : Int! = UserDefaults.standard.integer(forKey: "expires_in")
    
    var firebase_token : String! = UserDefaults.standard.string(forKey: "firebase_token") ?? ""
    var id : String! = UserDefaults.standard.string(forKey: "id") ?? ""
    var name : String! = UserDefaults.standard.string(forKey: "name") ?? ""
    var email : String! = UserDefaults.standard.string(forKey: "email") ?? ""
    var role : String! = UserDefaults.standard.string(forKey: "role") ?? ""
    var phone : String! = UserDefaults.standard.string(forKey: "phone") ?? ""
    var image : String! = UserDefaults.standard.string(forKey: "image") ?? ""
    var image_thumb : String! = UserDefaults.standard.string(forKey: "image_thumb") ?? ""
    var country : String! = UserDefaults.standard.string(forKey: "country") ?? ""
    var company_name : String! = UserDefaults.standard.string(forKey: "company_name") ?? ""
    
    func setData(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let profileData : [String:Any] = returnData["profile"] as? [String : Any] ?? [:]
        
        var userID = ""
        
        if let idInt = profileData["id"] as? Int{
            userID = String(idInt)
        }
        else{
            userID = profileData["id"] as? String ?? ""
        }
        
        self.save(id: userID,
                  name: profileData["name"] as? String ?? "",
                  email: profileData["email"] as? String ?? "",
                  role: profileData["role"] as? String ?? "",
                  phone: profileData["phone"] as? String ?? "",
                  image: profileData["image"] as? String ?? "",
                  image_thumb: profileData["image_thumb"] as? String ?? "",
                  country: profileData["country"] as? String ?? "",
                  company_name: profileData["company_name"] as? String ?? "")
        self.load()
    }
    
    func save(id:String,name:String,email:String,role:String,phone:String,image:String,image_thumb:String,country:String,company_name:String){
        
        UserDefaults.standard.set(id, forKey: "id")
        
        UserDefaults.standard.set(name, forKey: "name")
        
        UserDefaults.standard.set(email, forKey: "email")
        
        UserDefaults.standard.set(role, forKey: "role")
        
        UserDefaults.standard.set(phone, forKey: "phone")
        
        UserDefaults.standard.set(image, forKey: "image")
        
        UserDefaults.standard.set(image_thumb, forKey: "image_thumb")
        
        UserDefaults.standard.set(country, forKey: "country")
        
        UserDefaults.standard.set(company_name, forKey: "company_name")
        
    }
    
    func saveFirebaseToken (firebase_token:String){
        if firebase_token != ""{
            UserDefaults.standard.set(firebase_token, forKey: "firebase_token")
        }
        self.load()
    }
    
    func setToken(data: [String: Any]) {
        let profileData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        self.saveToken(access_token: profileData["access_token"] as? String ?? "",
                  refresh_token: profileData["refresh_token"] as? String ?? "",
                  token_type: profileData["token_type"] as? String ?? "",
                  expires_in: profileData["expires_in"] as? Int ?? 0)
        self.load()
    }
    
    func saveToken(access_token:String,refresh_token:String,token_type:String,expires_in:Int){
        UserDefaults.standard.set(access_token, forKey: "access_token")
        
        UserDefaults.standard.set(refresh_token, forKey: "refresh_token")
        
        UserDefaults.standard.set(token_type, forKey: "token_type")
        
        UserDefaults.standard.set(expires_in, forKey: "expires_in")
        
    }
    
    func load(){
        self.access_token = UserDefaults.standard.string(forKey: "access_token")
        self.refresh_token = UserDefaults.standard.string(forKey: "refresh_token")
        self.token_type = UserDefaults.standard.string(forKey: "token_type")
        self.expires_in = UserDefaults.standard.integer(forKey: "expires_in")
        
        self.firebase_token = UserDefaults.standard.string(forKey: "firebase_token")
        self.id = UserDefaults.standard.string(forKey: "id")
        self.name = UserDefaults.standard.string(forKey: "name")
        self.email = UserDefaults.standard.string(forKey: "email")
        self.role = UserDefaults.standard.string(forKey: "role")
        self.phone = UserDefaults.standard.string(forKey: "phone")
        self.image = UserDefaults.standard.string(forKey: "image")
        self.image_thumb = UserDefaults.standard.string(forKey: "image_thumb")
        self.country = UserDefaults.standard.string(forKey: "country")
        self.company_name = UserDefaults.standard.string(forKey: "company_name")
    }
    
    func clearToken(){
        UserDefaults.standard.set("", forKey: "access_token")
        UserDefaults.standard.set("", forKey: "refresh_token")
        UserDefaults.standard.set("", forKey: "token_type")
        UserDefaults.standard.set(0, forKey: "expires_in")
        
        UserDefaults.standard.set("", forKey: "id")
        UserDefaults.standard.set("", forKey: "name")
        UserDefaults.standard.set("", forKey: "email")
        UserDefaults.standard.set("", forKey: "role")
        UserDefaults.standard.set("", forKey: "phone")
        UserDefaults.standard.set("", forKey: "image")
        UserDefaults.standard.set("", forKey: "image_thumb")
        UserDefaults.standard.set("", forKey: "country")
        UserDefaults.standard.set("", forKey: "company_name")
        
        self.access_token = ""
        self.refresh_token = ""
        self.token_type = ""
        self.expires_in = 0
        
        self.id = ""
        self.name = ""
        self.email = ""
        self.role = ""
        self.phone = ""
        self.image = ""
        self.image_thumb = ""
        self.country = ""
        self.company_name = ""
    }
    
}
