//
//  Location.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class Location {
    
    private init() {}
    static let shared = Location()
    
    var location_list : [location] = []
    
    func setData(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let locationData : [[String:Any]] = returnData["country"] as? [[String : Any]] ?? []
        
        for locationObject in locationData{
            var locID = ""
            
            if let idInt = locationObject["id"] as? Int{
                locID = String(idInt)
            }
            else{
                locID = locationObject["id"] as? String ?? ""
            }
            
            self.location_list.append(location(id: locID, title: locationObject["title"] as? String ?? ""))
        }
    }
    
    struct location{
        var id : String? = ""
        var title : String? = ""
    }

}
