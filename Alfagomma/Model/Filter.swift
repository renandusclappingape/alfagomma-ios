//
//  Filter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 26/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class FilterList {
    
    var filter_category_list : [category] = []
    var filter_location_list : [location] = []
    
    init(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let categoryData : [[String:Any]] = returnData["category"] as? [[String : Any]] ?? []
        
        if !categoryData.isEmpty{
            for categoryObject in categoryData{
                var categoryID = ""
                
                if let idInt = categoryObject["id"] as? Int{
                    categoryID = String(idInt)
                }
                else{
                    categoryID = categoryObject["id"] as? String ?? ""
                }
                
                self.filter_category_list.append(category(id: categoryID, title: categoryObject["title"] as? String ?? ""))
            }
        }
        
        let locationData : [[String:Any]] = returnData["country"] as? [[String : Any]] ?? []
        if !locationData.isEmpty{
            for locationObject in locationData{
                var locationID = ""
                
                if let idInt = locationObject["id"] as? Int{
                    locationID = String(idInt)
                }
                else{
                    locationID = locationObject["id"] as? String ?? ""
                }
                
                self.filter_location_list.append(location(id: locationID, title: locationObject["title"] as? String ?? ""))
            }
        }
    }
    
    struct category{
        var id : String = ""
        var title : String = ""
    }
    
    struct location{
        var id : String = ""
        var title : String = ""
    }
    
}
