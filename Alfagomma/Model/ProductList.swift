//
//  ProductList.swift
//  Alfagomma
//
//  Created by Clapping Ape on 23/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class ProductList {
    
    var product_list : [product] = []
    
    init(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let productData : [[String:Any]] = returnData["product"] as? [[String : Any]] ?? []
        
        for productObject in productData{
            
            let lengthData : [String:Any] = productObject["length"] as? [String : Any] ?? [:]
            
            var floatValue : Double = 0
            
            if let value = lengthData["value"] as? Int{
                floatValue = Double(value)
            }
            else if let value = lengthData["value"] as? Double{
                floatValue = value
            }
            
            var productID = ""
            
            if let idInt = productObject["id"] as? Int{
                productID = String(idInt)
            }
            else{
                productID = productObject["id"] as? String ?? ""
            }
            
            let productImages : [[String:String]] = productObject["product_images"] as? [[String:String]] ?? []
            var imageArray : [image_object] = []
            for image in productImages{
                imageArray.append(image_object(image: image["image"] ?? "", image_thumb: image["image_thumb"] ?? ""))
            }
            
            self.product_list.append(product(id: productID,
                                             image: imageArray,
                                             number: productObject["number"] as? String ?? "",
                                             title: productObject["title"] as? String ?? "",
                                             available_status: productObject["available_status"] as? Bool ?? false,
                                             length_value: floatValue,
                                             length_unit: lengthData["unit"] as? String ?? ""))
        }
    }
    
    struct product{
        var id : String = ""
        var image : [image_object] = []
        var number : String = ""
        var title : String = ""
        var available_status : Bool = false
        var length_value : Double = 0
        var length_unit : String = ""
    }
    
    struct image_object{
        var image : String = ""
        var image_thumb : String = ""
    }
}
