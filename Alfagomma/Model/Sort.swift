//
//  Sort.swift
//  Alfagomma
//
//  Created by Clapping Ape on 26/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class SortList {
    
    var sort_list : [sort] = []
    
    init(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let sortData : [[String:Any]] = returnData["sort"] as? [[String : Any]] ?? []
        
        for sortObject in sortData{
            var sortID = ""
            
            if let idInt = sortObject["id"] as? Int{
                sortID = String(idInt)
            }
            else{
                sortID = sortObject["id"] as? String ?? ""
            }
            
            self.sort_list.append(sort(id: sortID, title: sortObject["title"] as? String ?? ""))
        }
    }
    
    struct sort{
        var id : String = ""
        var title : String = ""
    }
    
}
