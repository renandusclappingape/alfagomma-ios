//
//  CalendarProduct.swift
//  Alfagomma
//
//  Created by Clapping Ape on 04/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class CalendarProduct {
    
    var product_list : [product] = []
    
    init(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let productData : [[String:Any]] = returnData["product"] as? [[String : Any]] ?? []
        
        for productObject in productData{
            let immediate_service_data = productObject["immediate_service"] as? [String : Any] ?? [:]
            self.product_list.append(product(id: productObject["id"] as? String ?? "",
                                             number: productObject["number"] as? String ?? "",
                                             title: productObject["title"] as? String ?? "",
                                             last_service: productObject["last_service"] as? Int ?? 0,
                                             next_service: productObject["next_service"] as? Int ?? 0,
                                             service_info: productObject["service_info"] as? String ?? "",
                                             immediate_service: immediate_service_object(status: immediate_service_data["status"] as? Bool ?? false,
                                                                                         date: immediate_service_data["date"] as? Int ?? 0,
                                                                                         notes: immediate_service_data["notes"] as? String ?? "")))
        }
    }
    
    struct product{
        var id : String = ""
        var number : String = ""
        var title : String = ""
        var last_service : Int = 0
        var next_service : Int = 0
        var service_info : String = ""
        var immediate_service : immediate_service_object
    }
    
    struct immediate_service_object{
        var status : Bool = false
        var date : Int = 0
        var notes : String = ""
    }
    
}
