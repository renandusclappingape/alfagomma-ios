//
//  Banner.swift
//  Alfagomma
//
//  Created by Clapping Ape on 25/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

class BannerList {
    
    var banner_list : [banner] = []
    
    init(data: [String: Any]) {
        let returnData : [String:Any] = data["data"] as? [String : Any] ?? [:]
        let bannerData : [[String:Any]] = returnData["banner"] as? [[String : Any]] ?? []
        
        for bannerObject in bannerData{
            self.banner_list.append(banner(image: bannerObject["image"] as? String ?? "", image_thumb:  bannerObject["image_thumb"] as? String ?? "", link_url:  bannerObject["link_url"] as? String ?? ""))
        }
    }
    
    struct banner{
        var image : String = ""
        var image_thumb : String = ""
        var link_url : String = ""
    }
    
}
