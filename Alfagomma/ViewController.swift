//
//  ViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 21/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ViewController: BaseViewController, AuthView {

    override func viewDidLoad() {
        super.viewDidLoad()
        ViewControllerPresenter.sharedInstance.attachView(view: self)
        ViewControllerPresenter.sharedInstance.loadToken()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Mark: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setUserToken(data: TokenData) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginNavigationController")
        self.present(LoginVC, animated: false) {
            
        }
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {},retryBlock: {ViewControllerPresenter.sharedInstance.loadToken()})
    }
    
}

