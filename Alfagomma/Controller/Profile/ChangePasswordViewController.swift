//
//  ChangePasswordViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 06/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController, ChangePasswordView {
    
    //UITextField
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    //UIView
    @IBOutlet weak var currentPasswordSectionView: UIView!
    @IBOutlet weak var newPasswordSectionView: UIView!
    @IBOutlet weak var confirmPasswordSectionView: UIView!
    
    //UIButton
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var currentPasswordeyeButton: UIButton!
    @IBOutlet weak var newPasswordeyeButton: UIButton!
    @IBOutlet weak var confirmPasswordeyeButton: UIButton!
    
    //Other
    var showCurrentPassword : Bool = false
    var showNewPassword : Bool = false
    var showConfirmPassword : Bool = false
    var tap : UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChangePasswordViewControllerPresenter.sharedInstance.attachView(view: self)

        self.setupDisplay()
        self.registerNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay() {
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Change Password"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.currentPasswordTextField.delegate = self
        self.newPasswordTextField.delegate = self
        self.confirmPasswordTextField.delegate = self
        
        self.currentPasswordSectionView.layer.borderWidth = 1
        self.currentPasswordSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.currentPasswordSectionView.layer.cornerRadius = 4
        self.currentPasswordSectionView.layer.masksToBounds = true
        
        self.newPasswordSectionView.layer.borderWidth = 1
        self.newPasswordSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.newPasswordSectionView.layer.cornerRadius = 4
        self.newPasswordSectionView.layer.masksToBounds = true
        
        self.confirmPasswordSectionView.layer.borderWidth = 1
        self.confirmPasswordSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.confirmPasswordSectionView.layer.cornerRadius = 4
        self.confirmPasswordSectionView.layer.masksToBounds = true
    }
    
    @IBAction func currentPasswordeyeButtonAction(_ sender: Any) {
        if showCurrentPassword {
            self.currentPasswordeyeButton.setImage(UIImage(named: "form-icon-eye-inactive"), for: .normal)
            self.currentPasswordTextField.isSecureTextEntry = true
            showCurrentPassword = false
        }
        else{
            self.currentPasswordeyeButton.setImage(UIImage(named: "form-icon-eye-active"), for: .normal)
            self.currentPasswordTextField.isSecureTextEntry = false
            showCurrentPassword = true
        }
    }
    
    @IBAction func newPasswordeyeButtonAction(_ sender: Any) {
        if showNewPassword {
            self.newPasswordeyeButton.setImage(UIImage(named: "form-icon-eye-inactive"), for: .normal)
            self.newPasswordTextField.isSecureTextEntry = true
            showNewPassword = false
        }
        else{
            self.newPasswordeyeButton.setImage(UIImage(named: "form-icon-eye-active"), for: .normal)
            self.newPasswordTextField.isSecureTextEntry = false
            showNewPassword = true
        }
    }
    
    @IBAction func confirmPasswordeyeButtonAction(_ sender: Any) {
        if showConfirmPassword {
            self.confirmPasswordeyeButton.setImage(UIImage(named: "form-icon-eye-inactive"), for: .normal)
            self.confirmPasswordTextField.isSecureTextEntry = true
            showConfirmPassword = false
        }
        else{
            self.confirmPasswordeyeButton.setImage(UIImage(named: "form-icon-eye-active"), for: .normal)
            self.confirmPasswordTextField.isSecureTextEntry = false
            showConfirmPassword = true
        }
    }
    
    @IBAction func changePasswordButtonAction(_ sender: Any) {
        let param : [String:Any] = ["id":Int(User.shared.id) ?? 0,
        "current_password":self.currentPasswordTextField.text!,
                     "new_password":self.newPasswordTextField.text!,
                     "confirm_new_password":self.confirmPasswordTextField.text!]
        ChangePasswordViewControllerPresenter.sharedInstance.changePassword(param: param)
        
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func changePasswordSuccess() {
        let alert = UIAlertController(title: "SUCCESS", message: "Your Password successfully changed.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
        }
        ))
        
        self.present(alert, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
}

extension ChangePasswordViewController : UITextFieldDelegate{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.currentPasswordTextField {
            self.newPasswordTextField.becomeFirstResponder()
        } else if textField == self.newPasswordTextField {
            self.confirmPasswordTextField.becomeFirstResponder()
        } else if textField == self.confirmPasswordTextField {
            self.changePasswordButtonAction(self)
        }
        
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
    }
}
