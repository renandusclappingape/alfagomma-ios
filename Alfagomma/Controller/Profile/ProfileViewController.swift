//
//  ProfileViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 06/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController, ProfileView {
    
    //UILabel
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    
    //UIButton
    @IBOutlet weak var logoutButton: UIButton!
    
    //UITextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UIView
    @IBOutlet weak var searchTextFieldView: UIView!
    
    var userData : UserData?
    var tap : UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()

        ProfileViewControllerPresenter.sharedInstance.attachView(view: self)
        ProfileViewControllerPresenter.sharedInstance.loadProfile()
        
        self.setupDisplay()
        self.registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Profile"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchTextField.text = SearchTextField.shared.text
        SearchTextField.shared.setActiveViewController(viewController: self)
        
        if self.navigationController?.navigationBar.backItem == nil{
            self.navigationItem.leftBarButtonItem = self.backButton()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Profile"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let myimage = UIImage(named: "icon-bar-qr")?.withRenderingMode(.alwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: myimage, style: .plain, target: self, action: #selector(ScanQrButtonAction))
        
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageViewSearchBox = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageViewSearchBox.image = image;
        viewField.addSubview(imageViewSearchBox)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        self.searchTextField.delegate = SearchTextField.shared
        self.searchTextField.text = SearchTextField.shared.text
        
        self.view.layoutIfNeeded()
        
        self.logoutButton.isHidden = true
        
    }
    
    func backButton() -> UIBarButtonItem {
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "icon-back"), style: .plain, target: self, action: #selector(dismissVC))
        
        return leftBarButtonItem
    }
    
    @objc func dismissVC(){
        self.tabBarController?.selectedIndex = 0
    }

    @objc func ScanQrButtonAction() {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: {
                (alert: UIAlertAction!) in
                self.tabBarController?.dismiss(animated: true, completion: {
                    User.shared.clearToken()
                })
            }
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) in
                
            }
            ))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setUserData(data: UserData) {
        self.userData = data
        self.showData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    func showData(){
        self.nameLabel.text = self.userData?.name
        self.emailLabel.text = self.userData?.email
        self.phoneLabel.text = self.userData?.phone
        self.countryLabel.text = self.userData?.country
        self.companyNameLabel.text = self.userData?.company_name
    }
    
}

extension ProfileViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}
