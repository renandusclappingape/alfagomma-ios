//
//  EditProfileViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 27/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol EditProfileView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setLocationSuccess()
    func setErrorMessageFromAPI(errorMessage: String)
}

class EditProfileViewControllerPresenter {
    
    weak fileprivate var editProfileView : EditProfileView?
    
    private init() {}
    static let sharedInstance = EditProfileViewControllerPresenter()
    
    func attachView(view: EditProfileView) {
        self.editProfileView = view
    }
    
    func detachView() {
        self.editProfileView = nil
    }
    
    
    func setLocation(locationID: String){
        self.editProfileView?.startLoading(text : "Loading")
        
        let param : [String:Any] = ["id":Int(User.shared.id) ?? 0,"country_id":locationID]
        
        APIService.sharedInstance.updateProfileAPI(params: param,callBack: { () in
            self.editProfileView?.finishLoading()
            self.editProfileView?.setLocationSuccess()
        }) { (message: String) in
            self.editProfileView?.finishLoading()
            self.editProfileView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
