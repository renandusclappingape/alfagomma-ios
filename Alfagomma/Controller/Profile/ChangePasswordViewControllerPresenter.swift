//
//  ChangePasswordViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 02/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol ChangePasswordView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func changePasswordSuccess()
    func setErrorMessageFromAPI(errorMessage: String)
}

class ChangePasswordViewControllerPresenter {
    
    weak fileprivate var changePasswordView : ChangePasswordView?
    
    private init() {}
    static let sharedInstance = ChangePasswordViewControllerPresenter()
    
    func attachView(view: ChangePasswordView) {
        self.changePasswordView = view
    }
    
    func detachView() {
        self.changePasswordView = nil
    }
    
    func changePassword(param: [String:Any]) {
        self.changePasswordView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ChangePasswordAPI(params: param,callBack: { () in
            self.changePasswordView?.finishLoading()
            self.changePasswordView?.changePasswordSuccess()
        }) { (message: String) in
            self.changePasswordView?.finishLoading()
            self.changePasswordView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
}
