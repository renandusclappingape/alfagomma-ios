//
//  EditProfileViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 06/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController, EditProfileView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    
    var selectedLocation : Location.location?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        EditProfileViewControllerPresenter.sharedInstance.attachView(view: self)
        self.showData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Edit Profile"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    @IBAction func selectCountryButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LocationSelect", bundle: nil)
        let locationVC = storyboard.instantiateViewController(withIdentifier: "LocationSelectViewController") as! LocationSelectViewController
        locationVC.completion = { (selectedLocation: Location.location) in
            self.countryLabel.text = selectedLocation.title
            self.selectedLocation = selectedLocation
            self.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    func showData(){
        self.nameLabel.text = User.shared.name
        self.emailLabel.text = User.shared.email
        self.phoneLabel.text = User.shared.phone
        self.countryLabel.text = User.shared.country
        self.companyNameLabel.text = User.shared.company_name
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setLocationSuccess() {
        let alert = UIAlertController(title: "SUCCESS", message: "Your profile changes has been saved.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) in
        }
        ))
        
        self.present(alert, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if self.selectedLocation != nil{
            EditProfileViewControllerPresenter.sharedInstance.setLocation(locationID: (self.selectedLocation?.id)!)
        }
    }
}
