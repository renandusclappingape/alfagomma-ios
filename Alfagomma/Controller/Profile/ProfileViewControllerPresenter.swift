//
//  ProfileViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 27/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol ProfileView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setUserData (data: UserData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class ProfileViewControllerPresenter {
    
    weak fileprivate var profileView : ProfileView?
    
    private init() {}
    static let sharedInstance = ProfileViewControllerPresenter()
    
    func attachView(view: ProfileView) {
        self.profileView = view
    }
    
    func detachView() {
        self.profileView = nil
    }
    
    func loadProfile() {
        self.profileView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ProfileRequestAPI(callBack: { [weak self](data) in
            self?.profileView?.finishLoading()
            self?.profileView?.setUserData(data: UserData(id: data.id, name: data.name, email: data.email, role: data.role, phone: data.phone, image: data.image, image_thumb: data.image_thumb, country: data.country, company_name: data.company_name))
        }) { (message: String) in
            self.profileView?.finishLoading()
            self.profileView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
}
