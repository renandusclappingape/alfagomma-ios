//
//  FilterLocationViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

protocol FilterLocationViewControllerDelegate: NSObjectProtocol {
    func completionLocation(data:[String:Any])
}

class FilterLocationViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, FilterLocationView {
    
    @IBOutlet weak var tableViewLocation: UITableView!
    
    var filterLocationViewControllerDelegate : FilterLocationViewControllerDelegate?
    
    var locationItem : FilterLocationData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupDisplay()
        
        FilterLocationViewControllerPresenter.sharedInstance.attachView(view: self)
        FilterLocationViewControllerPresenter.sharedInstance.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupDisplay(){
        self.navigationItem.title = "Location"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
    }
    
    // MARK: Presenter Delegate
    func startLoading(text: String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setFilterLocationData(data: FilterLocationData) {
        self.locationItem = data
        self.tableViewLocation.reloadData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    // UItableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locationItem?.filter_location_list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionTableViewCell") as! OptionTableViewCell
        
        cell.titleLabel.text = self.locationItem?.filter_location_list[indexPath.row].title ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filterLocationViewControllerDelegate?.completionLocation(data: ["location_id":self.locationItem?.filter_location_list[indexPath.row].id ?? "",
                                                                             "location_title":self.locationItem?.filter_location_list[indexPath.row].title ?? ""])
        self.navigationController?.popViewController(animated: true)
    }
}
