//
//  FilterCategoryViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

protocol FilterCategoryViewControllerDelegate: NSObjectProtocol {
    func completionCategory(data:[String:Any])
}

class FilterCategoryViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, FilterCategoryView {
    
    @IBOutlet weak var tableViewCategory: UITableView!
    
    var filterCategoryViewControllerDelegate : FilterCategoryViewControllerDelegate?
    
    var categoryItem : FilterCategoryData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupDisplay()
        
        FilterCategoryViewControllerPresenter.sharedInstance.attachView(view: self)
        FilterCategoryViewControllerPresenter.sharedInstance.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupDisplay(){
        self.navigationItem.title = "Category"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
    }
    
    // MARK: Presenter Delegate
    func startLoading(text: String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setFilterCategoryData(data: FilterCategoryData) {
        self.categoryItem = data
        self.tableViewCategory.reloadData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    // UItableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryItem?.filter_category_list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionTableViewCell") as! OptionTableViewCell
        
        cell.titleLabel.text = self.categoryItem?.filter_category_list[indexPath.row].title ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filterCategoryViewControllerDelegate?.completionCategory(data: ["category_id":self.categoryItem?.filter_category_list[indexPath.row].id ?? "",
                                                                             "category_title":self.categoryItem?.filter_category_list[indexPath.row].title ?? ""])
        self.navigationController?.popViewController(animated: true)
    }
}
