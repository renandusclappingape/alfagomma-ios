//
//  FilterCategoryViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 26/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct FilterCategoryData{
    var filter_category_list : [FilterList.category] = []
    
}

protocol FilterCategoryView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setFilterCategoryData (data: FilterCategoryData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class FilterCategoryViewControllerPresenter {
    
    weak fileprivate var filterCategoryView : FilterCategoryView?
    
    private init() {}
    static let sharedInstance = FilterCategoryViewControllerPresenter()
    
    func attachView(view: FilterCategoryView) {
        self.filterCategoryView = view
    }
    
    func detachView() {
        self.filterCategoryView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        self.filterCategoryView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.FilterCategoryRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            self?.filterCategoryView?.finishLoading()
            self?.filterCategoryView?.setFilterCategoryData(data: FilterCategoryData(filter_category_list: data.filter_category_list))
        }) { (message: String) in
            self.filterCategoryView?.finishLoading()
            self.filterCategoryView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
}
