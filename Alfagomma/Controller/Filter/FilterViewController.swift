//
//  FilterViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

protocol FilterViewControllerDelegate: NSObjectProtocol {
    func completion(data:[String:Any])
}

class FilterViewController: UIViewController, FilterCategoryViewControllerDelegate, FilterLocationViewControllerDelegate {
    
    @IBOutlet weak var selectedCategoryLabel: UILabel!
    @IBOutlet weak var selectedLocationLabel: UILabel!
    
    var filterViewControllerDelegate : FilterViewControllerDelegate?
    
    var param : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let filterParam : [String:Any] = self.param?["filter"] as? [String : Any] ?? [:]
        self.selectedCategoryLabel.text = filterParam["category_title"] as? String ?? ""
        self.selectedLocationLabel.text = filterParam["country_title"] as? String ?? ""
        self.setupDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationItem.title = "Filter"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
    }
    
    @IBAction func applyFilterButtonAction(_ sender: Any) {
        self.filterViewControllerDelegate?.completion(data: self.param!["filter"] as! [String : Any])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "category" {
            let vc = segue.destination as? FilterCategoryViewController
            vc?.filterCategoryViewControllerDelegate = self
            
        }
        if segue.identifier == "location" {
            let vc = segue.destination as? FilterLocationViewController
            vc?.filterLocationViewControllerDelegate = self
        }
    }
    
    func completionCategory(data: [String : Any]) {
        let filterParam : [String:Any] = self.param?["filter"] as? [String : Any] ?? [:]
        self.param!["filter"] = filterParam.merging(["category":data["category_id"] ?? "","category_title":data["category_title"] ?? ""], uniquingKeysWith: { (_, last) in last })

        self.selectedCategoryLabel.text = data["category_title"] as? String ?? ""
    }
    
    func completionLocation(data: [String : Any]) {
        let filterParam : [String:Any] = self.param?["filter"] as? [String : Any] ?? [:]
        self.param!["filter"] = filterParam.merging(["country":data["location_id"] ?? "","country_title":data["location_title"] ?? ""], uniquingKeysWith: { (_, last) in last })
        self.selectedLocationLabel.text = data["location_title"] as? String ?? ""
    }
    
}
