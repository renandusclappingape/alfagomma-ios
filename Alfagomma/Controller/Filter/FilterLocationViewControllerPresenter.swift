//
//  FilterLocationViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 26/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct FilterLocationData{
    var filter_location_list : [FilterList.location] = []
    
}

protocol FilterLocationView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setFilterLocationData (data: FilterLocationData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class FilterLocationViewControllerPresenter {
    
    weak fileprivate var filterLocationView : FilterLocationView?
    
    private init() {}
    static let sharedInstance = FilterLocationViewControllerPresenter()
    
    func attachView(view: FilterLocationView) {
        self.filterLocationView = view
    }
    
    func detachView() {
        self.filterLocationView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        self.filterLocationView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.FilterLocationRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            self?.filterLocationView?.finishLoading()
            self?.filterLocationView?.setFilterLocationData(data: FilterLocationData(filter_location_list: data.filter_location_list))
        }) { (message: String) in
            self.filterLocationView?.finishLoading()
            self.filterLocationView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
}
