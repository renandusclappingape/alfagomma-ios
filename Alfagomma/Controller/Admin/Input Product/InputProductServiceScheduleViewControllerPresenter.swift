//
//  InputProductServiceScheduleViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 23/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation


protocol InputProductView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setServiceSuccess(data : Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class InputProductServiceScheduleViewControllerPresenter {
    
    weak fileprivate var inputProductView : InputProductView?
    
    private init() {}
    static let sharedInstance = InputProductServiceScheduleViewControllerPresenter()
    
    func attachView(view: InputProductView) {
        self.inputProductView = view
    }
    
    func detachView() {
        self.inputProductView = nil
    }
    
    
    func setService(param: [String:Any], isEditMode: Bool){
        if isEditMode{
            self.editProduct(param: param)
        }
        else{
            self.addProduct(param: param)
        }
        
    }
    
    func addProduct(param: [String:Any]){
        self.inputProductView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.AddProductServiceAPI(parameters: param,callBack: { [weak self](data) in
            self?.addService(param: param,productID: data.id)
        }) { (message: String) in
            self.inputProductView?.finishLoading()
            self.inputProductView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
    
    func editProduct(param: [String:Any]){
        self.inputProductView?.startLoading(text : "Loading")
        APIService.sharedInstance.UpdateProductServiceAPI(parameters: param,callBack: { [weak self](data) in
            self?.addService(param: param,productID: data.id)
        }) { (message: String) in
            self.inputProductView?.finishLoading()
            self.inputProductView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
    
    func addService(param: [String:Any], productID: String){
        let productParam = param["product"] as! [String:Any]
        var serviceParam = productParam["service"] as! [String:Any]
        serviceParam["product_id"] = productID
        
        let params : [String:Any]! = ["user": ["user_id":param["user_id"],
                                              "user_latitude":param["latitude"],
                                              "user_longitude":param["longitude"]],
                                      "service":serviceParam]
        
        APIService.sharedInstance.SetProductServiceAPI(parameters: params,callBack: { [weak self](data) in
            self?.inputProductView?.finishLoading()
            self?.inputProductView?.setServiceSuccess(data: data)
        }) { (message: String) in
            self.inputProductView?.finishLoading()
            self.inputProductView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
