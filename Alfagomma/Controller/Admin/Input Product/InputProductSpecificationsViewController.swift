//
//  InputProductSpecificationsViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 18/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import SDWebImage

class InputProductSpecificationsViewController: BaseViewController, InputProductSpecificationView {

    //UITextField
    @IBOutlet weak var productCodeTextField: UITextField!
    @IBOutlet weak var productNumberTextField: UITextField!
    @IBOutlet weak var lengthTextField: UITextField!
    @IBOutlet weak var hwpPsiTextField: UITextField!
    @IBOutlet weak var hwpBarTextField: UITextField!
    @IBOutlet weak var idTextField: UITextField!
    
    //UIView
    @IBOutlet weak var productCodeView: UIView!
    @IBOutlet weak var productNumberView: UIView!
    @IBOutlet weak var lengthView: UIView!
    @IBOutlet weak var hwpPsiView: UIView!
    @IBOutlet weak var hwpBarView: UIView!
    @IBOutlet weak var idView: UIView!
    
    @IBOutlet weak var collectionViewUploadPhoto: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var picker : UIImagePickerController? = UIImagePickerController()
    let cellSpacing : CGFloat = 16
    
    var imageList : [UIImage] = [#imageLiteral(resourceName: "icon-addphoto")]
    var tap : UITapGestureRecognizer!
    var productNumber : String! = ""
    var productParam : [String:Any]?
    var encodeImage : [String] = []
    var lastService : Int = 0
    var nextService : Int = 0
    var serviceInfo = ""
    var maintenanceInfo = ""
    var isEditMode = false
    var productID = ""
    var keyboardHeight : CGFloat = 226.0
    
    var delegate : AddProductDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        InputProductSpecificationsViewControllerPresenter.sharedInstance.attachView(view: self)
        
        picker?.delegate = self
        self.setupDisplay()
        self.registerNotification()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Input Product"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dsispose of any resources that can be recreated.
    }
    
    func loadData(){
        let param : [String : Any] = [
            "keyword": self.productNumber ?? "",
            "user_id": Int(User.shared.id!) ?? 0,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
            "filter": [:
            ]
            ]
        InputProductSpecificationsViewControllerPresenter.sharedInstance.load(parameter: param)
        
        productParam = [
            "user_id": User.shared.id!,
            "country_id": User.shared.country!,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
            "product": [
                "number": productNumber,
                "title": "",
                "qr_image": "",
                "length": [
                    "value": 0,
                    "unit": "milimeter"
                ],
                "hwp_psi": 0,
                "hwp_bar": 0,
                "hwp_id": [
                    "value": 0,
                    "unit": "milimeter"
                ],
                "product_images": [
                ],
                "service": [
                    "last_service": 0,
                    "next_service": 0,
                    "service_info": "",
                    "maintenance_info": ""
                ]
            ]
        ]
    }
    
    func setupDisplay() {
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Input Product"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        
        let myimage = UIImage(named: "icon-back")?.withRenderingMode(.alwaysOriginal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: myimage, style: .plain, target: self, action: #selector(dismissVC))
        
        self.addBorder(view: productCodeView)
        self.addBorder(view: productNumberView)
        self.addBorder(view: lengthView)
        self.addBorder(view: hwpPsiView)
        self.addBorder(view: hwpBarView)
        self.addBorder(view: idView)
        
        self.productNumberTextField.text = self.productNumber
    }

    func addBorder(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
    }
    
    @objc func dismissVC(){
        self.navigationController?.dismiss(animated: true, completion: {
            
        })
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        if productCodeTextField.text == "" {
            let ac = UIAlertController(title: "", message: "Product Code cannot be blank", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            return
        }
        
        if productNumberTextField.text == "" {
            let ac = UIAlertController(title: "", message: "Product Number cannot be blank", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            return
        }
        
        if lengthTextField.text == "" {
            let ac = UIAlertController(title: "", message: "Length cannot be blank", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            return
        }
        
        if hwpPsiTextField.text == "" {
            let ac = UIAlertController(title: "", message: "Horse Working Pressure Psi cannot be blank", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            return
        }
        
        if hwpBarTextField.text == "" {
            let ac = UIAlertController(title: "", message: "Horse Working Pressure bar cannot be blank", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            return
        }
        
        if idTextField.text == "" {
            let ac = UIAlertController(title: "", message: "Horse Working Pressure ID cannot be blank", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            return
        }
        
        self.loadLocation () {
            self.performSegue(withIdentifier: "inputservice", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "inputservice" {
            for image in self.imageList{
                if image != #imageLiteral(resourceName: "icon-addphoto"){
                    let imageData:NSData = UIImageJPEGRepresentation(image, 0.5)! as NSData
                    let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
                    encodeImage.append("data:image/jpeg;base64,\(strBase64)")
                }
            }
        }
        
        var countryID = ""
        if let country = Location.shared.location_list.first(where: {$0.title == User.shared.country!}) {
            countryID = country.id!
        }
        self.productParam = [
            "user_id": User.shared.id!,
            "country_id": countryID,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
            "product": [
                "number": self.productNumberTextField.text!,
                "title": self.productCodeTextField.text!,
                "qr_image": "",
                "length": [
                    "value": self.lengthTextField.text!,
                    "unit": "milimeter"
                ],
                "hwp_psi": self.hwpPsiTextField.text!,
                "hwp_bar": self.hwpBarTextField.text!,
                "hwp_id": [
                    "value": self.idTextField.text!,
                    "unit": "milimeter"
                ],
                "product_images": self.encodeImage,
                "service": [
                    "last_service": self.lastService,
                    "next_service": self.nextService,
                    "service_info": self.serviceInfo,
                    "maintenance_info": self.maintenanceInfo
                ]
            ]
        ]
        
        let vc = segue.destination as? InputProductServiceScheduleViewController
        vc?.productParam = self.productParam!
        vc?.delegate = self.delegate
        vc?.productID = self.productID
        vc?.isEditMode = self.isEditMode
    }
    
    func loadLocation(success: @escaping () -> Void) {
        if Location.shared.location_list.count > 0 {
            success()
        }
        
        self.showLoadingIndicator(text: "Loading")
        
        APIService.sharedInstance.LocationRequestAPI(callBack: { [weak self](data) in
            self?.hideLoadingIndicator()
            success()
        }) { (message: String) in
             self.hideLoadingIndicator()
            self.basicAlertView(title: "", message: message, successBlock: {
                
            }, retryBlock: {
                self.nextButtonAction(self)
            })
        }
    }
    
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func productDetailsCompletion(data: Product?) {
        if data != nil {
            self.productCodeTextField.text = data?.title ?? ""
            self.lengthTextField.text = data?.length.value ?? ""
            self.hwpPsiTextField.text = data?.hwp_psi ?? ""
            self.hwpBarTextField.text = data?.hwp_bar ?? ""
            self.idTextField.text = data?.hwp_id.value
            
            for imageObject in (data?.product_images)! {
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: NSURL.init(string: Constant.BaseURL+imageObject.image! ) as URL?, options: .highPriority, progress: { (recieved, expected, nil) in
                    
                }, completed: { (image, data, error, true) in
                    if error == nil && image != nil{
                        self.imageList.insert(image!, at: 0)
                        self.collectionViewUploadPhoto.reloadData()
                        self.collectionViewHeightConstraint.constant = self.collectionViewUploadPhoto.collectionViewLayout.collectionViewContentSize.height
                        self.view.layoutIfNeeded()
                    }
                })
            }
            
            maintenanceInfo = data?.service.maintenance_info ?? ""
            serviceInfo = data?.service.service_info ?? ""
            lastService = data?.service.last_service ?? 0
            nextService = data?.service.next_service ?? 0
            productParam = [
                    "user_id": User.shared.id!,
                    "country_id": User.shared.country!,
                    "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
                    "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
                    "product": [
                        "number": productNumber,
                        "title": data?.title ?? "",
                        "qr_image": "",
                        "length": [
                            "value": data?.length.value ?? "",
                            "unit": "milimeter"
                        ],
                        "hwp_psi": data?.hwp_psi ?? "",
                        "hwp_bar": data?.hwp_bar ?? "",
                        "hwp_id": [
                            "value": data?.hwp_id.value,
                            "unit": "milimeter"
                        ],
                        "product_images": [
                        ],
                        "service": [
                            "last_service": lastService,
                            "next_service": nextService,
                            "service_info": serviceInfo,
                            "maintenance_info": maintenanceInfo
                        ]
                    ]
            ]
            
            self.productID = "\(data?.id ?? "")"
            self.isEditMode = true
        }
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
}

extension InputProductSpecificationsViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.productCodeTextField {
            self.lengthTextField.becomeFirstResponder()
        } else if textField == self.lengthTextField {
            self.hwpPsiTextField.becomeFirstResponder()
        } else if textField == self.hwpPsiTextField {
            self.hwpBarTextField.becomeFirstResponder()
        } else if textField == self.hwpBarTextField {
            self.idTextField.becomeFirstResponder()
        } else if textField == self.idTextField {
            performSegue(withIdentifier: "inputservice", sender: nil)
        }
        
        return false
    }
}

extension InputProductSpecificationsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return imageList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadPhotoCollectionViewCell",
                                                            for: indexPath) as? UploadPhotoCollectionViewCell else {
                                                                preconditionFailure("Unregistered table view cell")
        }
        
        cell.uploadImageView.image = self.imageList[indexPath.row]
        
        if cell.uploadImageView.image != #imageLiteral(resourceName: "icon-addphoto") {
            cell.deleteButton.isHidden = false
            cell.deleteButton.tag = indexPath.row+3333
            cell.uploadImageView.contentMode = .scaleAspectFill
            cell.deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
        }
        else{
            cell.deleteButton.isHidden = true
            cell.deleteButton.tag = indexPath.row+3333
            cell.uploadImageView.contentMode = .center
        }
        
        return cell
    }
    
    @objc func deleteImage(sender: UIButton!) {
        let index = sender.tag-3333
        self.imageList.remove(at: index)
        self.collectionViewUploadPhoto.reloadData()
        self.collectionViewHeightConstraint.constant = self.collectionViewUploadPhoto.collectionViewLayout.collectionViewContentSize.height
        self.view.layoutIfNeeded()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == self.imageList.count-1 {
            let alert:UIAlertController = UIAlertController(title: "Profile Picture Options", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.openCamera()
            }
            
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
                UIAlertAction in self.openGallery()
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in self.cancel()
            }
            
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker!, animated: true, completion: nil)
        }
        else
        {
            let alertController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker!.allowsEditing = false
        present(picker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageList.insert(pickedImage, at: 0)
            self.collectionViewUploadPhoto.reloadData()
            self.collectionViewHeightConstraint.constant = self.collectionViewUploadPhoto.collectionViewLayout.collectionViewContentSize.height
            self.view.layoutIfNeeded()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func cancel(){
        
    }
}

extension InputProductSpecificationsViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let itemSize = (collectionView.frame.size.width-(cellSpacing*2)-32)/3
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
}

extension InputProductSpecificationsViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.view.layoutIfNeeded()
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        if keyboardSize?.height ?? 0 > 0 {
            self.keyboardHeight = keyboardSize?.height ?? 0
        }
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,  self.keyboardHeight, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if self.scrollView.getSelectedTextField() != nil
        {
            let textField = self.scrollView.getSelectedTextField()
            var resultFrame = CGRect.zero
            resultFrame = self.scrollView.convert((textField?.frame)!, from: textField?.superview)
            
            if (!aRect.contains(resultFrame))
            {
                self.scrollView.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        
        
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
}
