//
//  InputProductServiceScheduleViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 18/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol AddProductDelegate : NSObjectProtocol {
    func didAddedProduct(data : Product)
    func didUpdatedProduct(data : Product)
}

class InputProductServiceScheduleViewController: BaseViewController, InputProductView {

    //UITextField
    @IBOutlet weak var latestServiceTextField: UITextField!
    @IBOutlet weak var nextServiceTextField: UITextField!
    @IBOutlet weak var yearTextField: UITextField!
    
    //UIView
    @IBOutlet weak var latestServiceView: UIView!
    @IBOutlet weak var nextServiceView: UIView!
    @IBOutlet weak var dateCalendarView: UIView!
    @IBOutlet weak var dateViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var calendarView: JTAppleCalendarView!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var prevMonthLabel: UILabel!
    @IBOutlet weak var nextMonthLabel: UILabel!
    
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var noteTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var maintenanceInformationTextView: UITextView!
    @IBOutlet weak var maintenanceInformationTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var shouldScroll = false
    var tap : UITapGestureRecognizer!
    var productParam : [String:Any]? = nil
    var picker: UIPickerView!
    var yearList : [Int] = []
    var selectedYear : Int?
    var visibleDateOnCalendar : DateSegmentInfo?
    var lastServiceSelected : Date? = nil
    var nextServiceSelected : Date? = nil
    let formatter = DateFormatter()
    var iii: Date?
    var activeTextField : UITextField?
    var isEditMode = false
    var productID = ""
    var keyboardHeight : CGFloat = 226.0
    
     var delegate : AddProductDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        InputProductServiceScheduleViewControllerPresenter.sharedInstance.attachView(view: self)
        
        self.latestServiceTextField.delegate = self
        self.nextServiceTextField.delegate = self
        self.noteTextView.delegate = self
        self.maintenanceInformationTextView.delegate = self
        self.setupDisplay()
        self.registerNotification()
        
        self.calendarView.reloadData()
        
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
            self.calendarView.scrollToDate(Date())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay(){
        self.addBorder(view: latestServiceView)
        self.addBorder(view: nextServiceView)
        self.addBorder(view: noteTextView)
        self.addBorder(view: maintenanceInformationTextView)
        
        self.dateCalendarView.isHidden = true
        self.dateViewBottomConstraint.constant = -self.dateViewHeightConstraint.constant
        
        self.picker = UIPickerView()
        self.picker.delegate = self
        self.picker.dataSource = self
        self.yearTextField.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.yearTextField.inputView = picker
        self.yearTextField.inputAccessoryView = toolBar
        
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = NSLocale.current
        
        let serviceObject : [String:Any] = (self.productParam!["product"] as? [String:Any] ?? [:])["service"] as? [String : Any] ?? [:]
        if serviceObject["last_service"] as! Int != 0 {
            self.lastServiceSelected = convertUnixTimeStamp(timestamp: serviceObject["last_service"] as! Int)
            self.latestServiceTextField.text = formatter.string(from: self.lastServiceSelected!)
        }

        if serviceObject["next_service"] as! Int != 0 {
            self.nextServiceSelected = convertUnixTimeStamp(timestamp: serviceObject["next_service"] as! Int)
            self.nextServiceTextField.text = formatter.string(from: self.nextServiceSelected!)
        }

        self.noteTextView.text = serviceObject["service_info"] as? String ?? ""
        self.maintenanceInformationTextView.text = serviceObject["maintenance_info"] as? String ?? ""
        
        self.calendarView.reloadData()
    }
    
    @objc func cancelPicker(){
        self.yearTextField.resignFirstResponder()
    }
    
    @objc func donePicker(){
        if self.selectedYear == nil || self.selectedYear == 0 {
            self.yearTextField.resignFirstResponder()
            return
        }
        self.yearTextField.text = String(self.selectedYear ?? 0)
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from: self.visibleDateOnCalendar?.monthDates.first?.date ?? Date())
        dateComponents.year = selectedYear
        
        let date: Date = calendar.date(from: dateComponents)!
        self.calendarView.scrollToDate(date, triggerScrollToDateDelegate: true, animateScroll: true, preferredScrollPosition: nil, extraAddedOffset: 0) {
            
        }
        self.yearTextField.resignFirstResponder()
    }
    
    func addBorder(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
    }
    
    @IBAction func submitProductButtonAction(_ sender: Any) {
        let lastServiceDate = "\(convertDateToUnixTimeStamp(date: self.lastServiceSelected!))"
        let nextServiceDate = "\(convertDateToUnixTimeStamp(date: self.nextServiceSelected!))"
        let serviceInfo = self.noteTextView.text
        let maintenanceInfo = self.maintenanceInformationTextView.text
        
        var paramObject = self.productParam!["product"] as! [String:Any]
        
        paramObject["service"] = [
        "last_service": lastServiceDate,
        "next_service": nextServiceDate,
        "service_info": serviceInfo ?? "",
            "maintenance_info": maintenanceInfo ?? ""] as [String:Any]
        
        if self.productID != ""{
            paramObject["id"] = self.productID
        }
        
        self.productParam!["product"] = paramObject
        
        InputProductServiceScheduleViewControllerPresenter.sharedInstance.setService(param : self.productParam!, isEditMode: self.isEditMode)
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setServiceSuccess(data: Product) {
        self.navigationController?.dismiss(animated: true, completion: {
            if self.isEditMode{
                self.delegate?.didUpdatedProduct(data: data)
            }
            else{
                self.delegate?.didAddedProduct(data: data)
            }
            
        })
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
}

extension InputProductServiceScheduleViewController: UITextViewDelegate, UITextFieldDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let sizeToFitIn = CGSize(width : textView.frame.size.width,height: CGFloat(MAXFLOAT))
        let newSize = textView.sizeThatFits(sizeToFitIn)
        
        let constraint : NSLayoutConstraint?
        if textView == self.noteTextView{
            constraint = self.noteTextViewHeightConstraint
        }
        else{
            constraint = self.maintenanceInformationTextViewHeightConstraint
        }
        
        if (constraint?.constant)! < newSize.height{
            shouldScroll = true
        }
        else{
            shouldScroll = false
        }
        
        constraint?.constant = newSize.height
        
        if shouldScroll{
            var frame = textView.frame
            frame.size.height = frame.size.height+30
            scrollView.scrollRectToVisible(frame, animated: true)
            
//            let bottomOffset = CGPoint(x: 0, y: textView.frame.size.height+textView.frame.origin.y-50)
//            print(bottomOffset.y)
//            scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.latestServiceTextField || textField == self.nextServiceTextField{
            self.noteTextView.resignFirstResponder()
            self.maintenanceInformationTextView.resignFirstResponder()
            self.dateCalendarView.isHidden = false
            self.dateViewBottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.scrollView.addGestureRecognizer(tap)
            self.activeTextField = textField
            
            self.calendarView.deselectAllDates(triggerSelectionDelegate: false)
            if textField == self.latestServiceTextField{
                if self.lastServiceSelected != nil {
                    self.calendarView.selectDates([self.lastServiceSelected ?? Date()], triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: false)
                }
                self.calendarView.scrollToDate(self.lastServiceSelected ?? Date())
            }
            else{
                if self.nextServiceSelected != nil {
                    self.calendarView.selectDates([self.nextServiceSelected ?? Date()], triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: false)
                }
                self.calendarView.scrollToDate(self.nextServiceSelected ?? Date())
            }
            
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.latestServiceTextField || textField == self.nextServiceTextField{
            self.dateViewBottomConstraint.constant = -self.dateViewHeightConstraint.constant
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finish) in
                self.dateCalendarView.isHidden = false
                self.scrollView.removeGestureRecognizer(self.tap)
            }
        }
    }
    
}

extension InputProductServiceScheduleViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        if keyboardSize?.height ?? 0 > 0 {
            self.keyboardHeight = keyboardSize?.height ?? 0
        }
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,  self.keyboardHeight, 0.0)
        
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if self.scrollView.getSelectedTextField() != nil
        {
            let textField = self.scrollView.getSelectedTextField()
            var resultFrame = CGRect.zero
            resultFrame = self.scrollView.convert((textField?.frame)!, from: textField?.superview)
            
            if (!aRect.contains(resultFrame))
            {
                self.scrollView.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        else if self.scrollView.getSelectedTextView() != nil
        {
            let textView = self.scrollView.getSelectedTextView()
            var resultFrame = CGRect.zero
            resultFrame = self.scrollView.convert((textView?.frame)!, from: textView?.superview)
            
            if (!aRect.contains(resultFrame))
            {
                self.scrollView.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        
        
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
        self.textFieldDidEndEditing(self.latestServiceTextField)
        self.textFieldDidEndEditing(self.nextServiceTextField)
    }
    
}

extension InputProductServiceScheduleViewController{
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        self.visibleDateOnCalendar = visibleDates
        
        self.setCurrentMonth(visibleDates: visibleDates)
        self.setPreviousMonth(visibleDates: visibleDates)
        self.setNextMonth(visibleDates: visibleDates)
        
    }
    
    func setCurrentMonth(visibleDates: DateSegmentInfo){
        guard let startDate = visibleDates.monthDates.first?.date else {
            monthLabel.text = ""
            yearTextField.text = ""
            return
        }
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = Calendar.current.component(.year, from: startDate)
        monthLabel.text = monthName
        yearTextField.text = String(year)
    }
    
    func setPreviousMonth(visibleDates: DateSegmentInfo){
        guard let prevDate = visibleDates.monthDates.first?.date else {
            prevMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:prevDate)
        dateComponents.month = dateComponents.month!-1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        prevMonthLabel.text = monthName
    }
    
    func setNextMonth(visibleDates: DateSegmentInfo){
        guard let nextDate = visibleDates.monthDates.first?.date else {
            nextMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:nextDate)
        dateComponents.month = dateComponents.month!+1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        nextMonthLabel.text = monthName
    }
    
    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else { return }
        handleCellTextColor(view: myCustomCell, cellState: cellState)
        handleCellSelection(view: myCustomCell, cellState: cellState)
    }
    
    func handleCellSelection(view: CellView, cellState: CellState) {
        if calendarView.allowsMultipleSelection {
            switch cellState.selectedPosition() {
            case .full: view.backgroundColor = .green
            case .left: view.backgroundColor = .yellow
            case .right: view.backgroundColor = .red
            case .middle: view.backgroundColor = .blue
            case .none: view.backgroundColor = nil
            }
        } else {
            if cellState.isSelected {
                view.selectedView.backgroundColor = UIColor.red
                view.dayLabel.textColor = .white
            } else {
                view.selectedView?.backgroundColor = UIColor.white
                handleCellTextColor(view: view, cellState: cellState)
            }
        }
    }
    func handleCellTextColor(view: CellView, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth{
            view.dayLabel.textColor = UIColor.black
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTMedium, size: 14)
        } else {
            view.dayLabel.textColor = UIColor.gray
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTBook, size: 14)
        }
        
        switch cellState.day{
        case .sunday: view.dayLabel.textColor = UIColor.red
        case .monday: break
        case .tuesday: break
        case .wednesday: break
        case .thursday: break
        case .friday: break
        case .saturday: break
        }
    }
    
    func setSelectedDate(date: Date){
        self.calendarView.selectDates([date])
    }
    
    @IBAction func nextMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.next)
    }
    
    @IBAction func prevMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.previous)
    }
}

extension InputProductServiceScheduleViewController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "cell", for: indexPath) as! CellView
        configureCell(view: cell, cellState: cellState)
        cell.dayLabel .text = cellState.text
        
        return cell
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let endDate = formatter.date(from: "2100 02 01")!
        
        let startYear = Calendar.current.component(.year, from: formatter.date(from: "2000 01 01")!)
        let endYear = Calendar.current.component(.year, from: endDate)
        for i in Int(startYear)..<Int(endYear)+1{
            self.yearList.append(i)
        }
        
        if yearList.count > 0{
            self.yearTextField?.text = String(yearList[0])
        }
        else{
            self.yearTextField?.text = String(startYear)
        }
        
        var myCalendar = Calendar(identifier: .gregorian)
        myCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
        myCalendar.locale = Calendar.current.locale
        
        let parameters = ConfigurationParameters(startDate: formatter.date(from: "2000 01 01")!,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: myCalendar)
        return parameters
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        return true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMMM yyyy"
        if self.activeTextField == self.latestServiceTextField{
            self.lastServiceSelected = date
            self.latestServiceTextField.text = dateFormatter.string(from:date)
        }
        else{
            self.nextServiceSelected = date
            self.nextServiceTextField.text = dateFormatter.string(from:date)
        }
        
        self.handleTap()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        calendarView.viewWillTransition(to: size, with: coordinator, anchorDate: iii)
    }
    
    
}

extension InputProductServiceScheduleViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(yearList[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedYear = yearList[row]
    }
}

