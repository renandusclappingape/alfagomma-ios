//
//  InputProductSpecificationsViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 01/08/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct InputProductSpecificationData{
    var search_product_list : [ProductList.product] = []
    
}

protocol InputProductSpecificationView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func productDetailsCompletion(data: Product?)
    func setErrorMessageFromAPI(errorMessage: String)
}

class InputProductSpecificationsViewControllerPresenter {
    
    weak fileprivate var inputProductSpecificationView : InputProductSpecificationView?
    
    private init() {}
    static let sharedInstance = InputProductSpecificationsViewControllerPresenter()
    
    var isLoading = false
    
    func attachView(view: InputProductSpecificationView) {
        self.inputProductSpecificationView = view
    }
    
    func detachView() {
        self.inputProductSpecificationView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        if isLoading{return}
        isLoading = true
        self.inputProductSpecificationView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SearchProductRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            let productData = InputProductSpecificationData(search_product_list: data.product_list)
            if productData.search_product_list.count > 0{
                self?.loadProductDetails(id: productData.search_product_list[0].id)
            }
            else{
                self?.inputProductSpecificationView?.productDetailsCompletion(data: nil)
                self?.inputProductSpecificationView?.finishLoading()
                self?.isLoading = false
            }
            
        }) { (message: String) in
            self.inputProductSpecificationView?.finishLoading()
            self.inputProductSpecificationView?.setErrorMessageFromAPI(errorMessage: message)
            self.isLoading = false
        }
        
    }
    
    func loadProductDetails(id: String!) {
        APIService.sharedInstance.ProductDetailsRequestAPI(id:id,callBack: { [weak self](data) in
            self?.inputProductSpecificationView?.finishLoading()
            self?.inputProductSpecificationView?.productDetailsCompletion(data: data)
            self?.isLoading = false
        }) { (message: String) in
            self.inputProductSpecificationView?.finishLoading()
            self.inputProductSpecificationView?.setErrorMessageFromAPI(errorMessage: message)
            self.isLoading = false
        }
        
    }
}
