//
//  AdminDashboardViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 17/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import SideMenu
import AVFoundation



class AdminDashboardViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    //UITextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UIView
    @IBOutlet weak var searchTextFieldView: UIView!
    var blurView : UIView?
    @IBOutlet weak var scanAreaView: UIView!
    @IBOutlet weak var popupView: UIView!
    
    //UILabel
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var popupMessageLabel: UILabel!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(logout(notification:)), name:  Notification.Name("logoutNotification"), object: nil)
        self.setupScanner()
        self.setupDisplay()
        self.setupSideMenu()
        self.showData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchTextField.text = SearchTextField.shared.text
        SearchTextField.shared.setActiveViewController(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func setupScanner(){
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .qr]
        } else {
            failed()
            return
        }
        
        self.view.layoutIfNeeded()
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        var frame = self.scanAreaView.frame
        frame.origin.x = 0
        frame.origin.y = 0
        previewLayer.frame = frame
        previewLayer.videoGravity = .resizeAspectFill
        self.scanAreaView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let logo = UIImage(named: "logobar-alfatraka")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let myimage = UIImage(named: "icon-burger-menu")?.withRenderingMode(.alwaysOriginal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: myimage, style: .plain, target: self, action: #selector(openSideMenu))
        
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageViewSearchBox = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageViewSearchBox.image = image;
        viewField.addSubview(imageViewSearchBox)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        self.searchTextField.delegate = SearchTextField.shared
        self.searchTextField.text = SearchTextField.shared.text
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurView = UIView(frame: self.view.bounds)
        blurView?.backgroundColor = .clear
        blurView?.alpha = 0.7
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = blurView!.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.blurView?.addSubview(blurEffectView)
        
        self.popupView.layer.cornerRadius = 12
        self.popupView.isHidden = true
    }
    
    func setupSideMenu(){
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuWidth = self.view.bounds.size.width*0.75

    }
    
    func showData(){
        self.nameLabel.text = User.shared.name
        self.emailLabel.text = User.shared.email
        self.phoneLabel.text = User.shared.phone
        self.roleLabel.text = User.shared.role
    }
    
    @objc func openSideMenu(){
        self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true) {
            
        }
    }
}

extension AdminDashboardViewController{
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
        
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
    }
    
    func found(code: String) {
        let storyboard = UIStoryboard(name: "InputProduct", bundle: nil)
        let navvc = storyboard.instantiateInitialViewController() as! UINavigationController
        let vc = navvc.viewControllers[0] as! InputProductSpecificationsViewController
        vc.productNumber = code
        vc.delegate = self
        self.present(navvc, animated: true) {
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

extension AdminDashboardViewController: UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
        blurView?.alpha = 0
        self.navigationController?.view.addSubview(blurView!)
        UIView.animate(withDuration: 0.2) {
            self.blurView?.alpha = 1
        }
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
        UIView.animate(withDuration: 0.2, animations: {
            self.blurView?.alpha = 0
        }) { (finished) in
            self.blurView?.removeFromSuperview()
        }
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
}

extension AdminDashboardViewController{
    @objc func logout(notification: NSNotification) {
        User.shared.clearToken()
        self.navigationController?.dismiss(animated: true) {
            
        }
    }
}

extension AdminDashboardViewController: AddProductDelegate{
    func didAddedProduct(data : Product){
        self.popupMessageLabel.text = "Product has been added to “Product ”. Tap “Products” below to  check your products ."
        self.navigationController?.view.layoutIfNeeded()
        self.showPopup()
    }
    
    func didUpdatedProduct(data : Product){
        self.popupMessageLabel.text = "Product has been updated. Tap “Products” below to  check your products ."
        self.navigationController?.view.layoutIfNeeded()
        self.showPopup()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.closePopup()
    }
    
    @IBAction func productsButtonAction(_ sender: Any) {
        self.closePopup()
        let storyboard = UIStoryboard(name: "MyProduct", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyProductViewController") as! MyProductViewController
        vc.enableRightItem = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showPopup(){
        blurView?.alpha = 0
        self.popupView.alpha = 0
        self.popupView.isHidden = false
        self.navigationController?.view.addSubview(blurView!)
        self.navigationController?.view.addSubview(self.popupView)
        UIView.animate(withDuration: 0.2) {
            self.blurView?.alpha = 1
            self.popupView.alpha = 1
        }
    }
    
    func closePopup(){
        UIView.animate(withDuration: 0.2, animations: {
            self.blurView?.alpha = 0
            self.popupView.alpha = 0
        }) { (finished) in
            self.blurView?.removeFromSuperview()
            self.popupView.isHidden = true
        }
    }
}
