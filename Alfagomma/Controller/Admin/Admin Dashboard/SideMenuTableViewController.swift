//
//  SideMenuTableViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 17/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuTableViewController: UITableViewController {

    //UILabel
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func showData(){
        self.nameLabel.text = User.shared.name
        self.emailLabel.text = User.shared.email
        self.phoneLabel.text = User.shared.phone
        self.roleLabel.text = User.shared.role
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.selectionStyle = .none

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let storyboard = UIStoryboard(name: "MyProduct", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyProductViewController") as! MyProductViewController
            vc.enableRightItem = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if indexPath.row == 3 {
            let storyboard = UIStoryboard(name: "AlfagommaAbout", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AlfagommaAboutViewController") as! AlfagommaAboutViewController
            vc.enableRightItem = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if indexPath.row == 4 {
            let alert = UIAlertController(title: "Logout", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: {
                (alert: UIAlertAction!) in
                self.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: Notification.Name("logoutNotification"), object: nil)
                })
            }
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) in
                
            }
            ))
            
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
    }
}
