//
//  ForgotPasswordViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol ForgotPasswordView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func forgotPassSuccess()
    func setErrorMessageFromAPI(errorMessage: String)
}

class ForgotPasswordViewControllerPresenter {
    
    weak fileprivate var forgotPasswordView : ForgotPasswordView?
    
    private init() {}
    static let sharedInstance = ForgotPasswordViewControllerPresenter()
    
    func attachView(view: ForgotPasswordView) {
        self.forgotPasswordView = view
    }
    
    func detachView() {
        self.forgotPasswordView = nil
    }
    
    func submit(email: String) {
        self.forgotPasswordView?.startLoading(text : "Loading")
        
        if User.shared.access_token == "" || User.shared.access_token == nil{
            self.loadToken(callBack: {
                APIService.sharedInstance.forgotPasswordAPI(email: email,callBack: { () in
                    self.forgotPasswordView?.finishLoading()
                    self.forgotPasswordView?.forgotPassSuccess()
                }) { (message: String) in
                    self.forgotPasswordView?.finishLoading()
                    self.forgotPasswordView?.setErrorMessageFromAPI(errorMessage: message)
                }
            }) { (messageError) in
                self.forgotPasswordView?.finishLoading()
                self.forgotPasswordView?.setErrorMessageFromAPI(errorMessage: messageError)
            }
        }
            
        else{
            APIService.sharedInstance.forgotPasswordAPI(email: email,callBack: { () in
                self.forgotPasswordView?.finishLoading()
                self.forgotPasswordView?.forgotPassSuccess()
            }) { (message: String) in
                self.forgotPasswordView?.finishLoading()
                self.forgotPasswordView?.setErrorMessageFromAPI(errorMessage: message)
            }
        }
        
    }
    
    func loadToken(callBack: @escaping () -> Void, messageError: @escaping (String) -> Void) {
        APIService.sharedInstance.tokenRequestAPI(callBack: { (data)  in
            callBack()
        }) { (message: String) in
            messageError(message)
        }
        
    }
}
