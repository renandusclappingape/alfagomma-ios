//
//  ForgotPasswordSuccessViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ForgotPasswordSuccessViewController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay(){
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Email Confirmation"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        
        self.okButton.layer.cornerRadius = 4
        self.okButton.layer.masksToBounds = true
        
    }

    @IBAction func okButtonAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
