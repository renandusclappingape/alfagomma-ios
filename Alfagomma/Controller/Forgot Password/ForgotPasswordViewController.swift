//
//  ForgotPasswordViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController, UITextFieldDelegate, ForgotPasswordView {
    
    //UITextField
    @IBOutlet weak var emailTextField: UITextField!
    
    //UIView
    @IBOutlet weak var emailSectionView: UIView!
    
    //UIButton
    @IBOutlet weak var sendButton: UIButton!
    
    //UILabel
    @IBOutlet weak var errorLabel: UILabel!
    
    //Other
    var tap : UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ForgotPasswordViewControllerPresenter.sharedInstance.attachView(view: self)

        self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.emailTextField.delegate = self
        
        self.setupDisplay()
        self.registerNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Forgot Password"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        
        self.emailSectionView.layer.borderWidth = 1
        self.emailSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.emailSectionView.layer.cornerRadius = 4
        self.emailSectionView.layer.masksToBounds = true
        
        self.sendButton.layer.cornerRadius = 4
        self.sendButton.layer.masksToBounds = true
        
        self.errorLabel.isHidden = true
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        let isEmailValid : Bool = self.emailTextField.text?.isValidEmail() ?? false
        if isEmailValid{
            ForgotPasswordViewControllerPresenter.sharedInstance.submit(email: self.emailTextField.text!)
        }
        else {
            self.emailSectionView.layer.borderColor = Color.redErrorColor.cgColor
            self.emailTextField.textColor = Color.redErrorColor
            self.errorLabel.isHidden = false
            self.errorLabel.text = "Email is invalid"
        }
        
    }
    
    // Mark: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func forgotPassSuccess() {
        let storyboard = UIStoryboard(name: "ForgotPassword", bundle: nil)
        let forgotSuccessVC = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordSuccessViewController")
        self.navigationController?.pushViewController(forgotSuccessVC, animated: true)
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }

    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.emailTextField {
            self.sendButtonAction(self)
        }
        
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.errorLabel.isHidden = true
        
        self.emailSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        
        self.emailTextField.textColor = .black
    }
}
