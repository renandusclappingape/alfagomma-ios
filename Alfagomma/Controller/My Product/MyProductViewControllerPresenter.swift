//
//  MyProductViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 23/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct MyProductData{
    var my_product_list : [ProductList.product] = []
    
}

protocol MyProductView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setProductData (data: MyProductData)
    func productDetailsCompletion(data: Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class MyProductViewControllerPresenter {
    
    weak fileprivate var myProductView : MyProductView?
    
    private init() {}
    static let sharedInstance = MyProductViewControllerPresenter()
    
    func attachView(view: MyProductView) {
        self.myProductView = view
    }
    
    func detachView() {
        self.myProductView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        self.myProductView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.MyProductRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            self?.myProductView?.finishLoading()
            self?.myProductView?.setProductData(data: MyProductData(my_product_list: data.product_list))
        }) { (message: String) in
            self.myProductView?.finishLoading()
            self.myProductView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
    func loadProductDetails(id: String!) {
        self.myProductView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ProductDetailsRequestAPI(id:id,callBack: { [weak self](data) in
            self?.myProductView?.finishLoading()
            self?.myProductView?.productDetailsCompletion(data: data)
        }) { (message: String) in
            self.myProductView?.finishLoading()
            self.myProductView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
}
