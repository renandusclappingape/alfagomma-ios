//
//  MyProductCollectionViewCell.swift
//  Alfagomma
//
//  Created by Clapping Ape on 23/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class MyProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productUnitLabel: UILabel!
    @IBOutlet weak var productCodeLabel: UILabel!
}
