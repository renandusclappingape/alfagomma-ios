//
//  LoginViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 21/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, UITextFieldDelegate, LoginView {
    
    //UITextField
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //UIView
    @IBOutlet weak var emailSectionView: UIView!
    @IBOutlet weak var passwordSectionView: UIView!
    @IBOutlet weak var loadingView: UIView!
    
    //UIButton
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var eyeButton: UIButton!
    
    //UILabel
    @IBOutlet weak var errorLabel: UILabel!
    
    //Other
    var showPassword : Bool = false
    
    var tap : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LoginViewControllerPresenter.sharedInstance.attachView(view: self)
        LoginViewControllerPresenter.sharedInstance.sessionValid()
        
        self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.setupDisplay()
        self.registerNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationItem.title = ""
        
        self.emailSectionView.layer.borderWidth = 1
        self.emailSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.emailSectionView.layer.cornerRadius = 4
        self.emailSectionView.layer.masksToBounds = true
        
        self.passwordSectionView.layer.borderWidth = 1
        self.passwordSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.passwordSectionView.layer.cornerRadius = 4
        self.passwordSectionView.layer.masksToBounds = true
        
        self.loginButton.layer.cornerRadius = 4
        self.loginButton.layer.masksToBounds = true
        
        self.errorLabel.isHidden = true

    }
    
    @IBAction func eyeButtonAction(_ sender: Any) {
        if showPassword {
            self.eyeButton.setImage(UIImage(named: "form-icon-eye-inactive"), for: .normal)
            self.passwordTextField.isSecureTextEntry = true
            showPassword = false
        }
        else{
            self.eyeButton.setImage(UIImage(named: "form-icon-eye-active"), for: .normal)
            self.passwordTextField.isSecureTextEntry = false
            showPassword = true
        }
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ForgotPassword", bundle: nil)
        let forgotVC = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController")
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        let isEmailValid : Bool = self.emailTextField.text?.isValidEmail() ?? false
        if isEmailValid && self.passwordTextField.text != ""{
            LoginViewControllerPresenter.sharedInstance.login(email: self.emailTextField.text!, password: self.passwordTextField.text!)
        }
        else if !isEmailValid{
            self.emailSectionView.layer.borderColor = Color.redErrorColor.cgColor
            self.emailTextField.textColor = Color.redErrorColor
            self.errorLabel.isHidden = false
            self.errorLabel.text = "Email is invalid"
        }
        else if self.passwordTextField.text == "" {
            self.passwordSectionView.layer.borderColor = Color.redErrorColor.cgColor
            self.passwordTextField.textColor = Color.redErrorColor
            self.errorLabel.isHidden = false
            self.errorLabel.text = "Sorry, Incorrect Password"
        }
        
    }
    
    // Mark: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
        self.loadingView.isHidden = true
    }
    
    func setUserData(data: UserData) {
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
        
        if data.role != "administrator"{
            let storyboard = UIStoryboard(name: "LocationSelect", bundle: nil)
            let locationVC = storyboard.instantiateViewController(withIdentifier: "LocationSelectViewController")
            self.navigationController?.pushViewController(locationVC, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name: "AdminDashboard", bundle: nil)
            let locationVC = storyboard.instantiateInitialViewController()
            self.present(locationVC!, animated: true) {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
    }
    
    func checkSession(isValid: Bool){
        if isValid {
            if User.shared.role != "administrator"{
                let storyboard = UIStoryboard(name: "Host", bundle: nil)
                let dashboardVC = storyboard.instantiateViewController(withIdentifier: "RootViewController")
                self.present(dashboardVC, animated: true) {
                }
            }
            else{
                let storyboard = UIStoryboard(name: "AdminDashboard", bundle: nil)
                let locationVC = storyboard.instantiateInitialViewController()
                self.present(locationVC!, animated: true) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            self.loginButtonAction(self)
        }
        
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.errorLabel.isHidden = true
        
        self.emailSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        self.passwordSectionView.layer.borderColor = UIColor.init(red: 208.0/255.0, green: 207.0/255.0, blue: 210.0/255.0, alpha: 1).cgColor
        
        self.emailTextField.textColor = .black
        self.passwordTextField.textColor = .black
    }
}
