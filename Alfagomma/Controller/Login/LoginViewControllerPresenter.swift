//
//  LoginViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 21/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct UserData{
    var id : String?
    var name : String?
    var email : String?
    var role : String?
    var phone : String?
    var image : String?
    var image_thumb : String?
    var country : String?
    var company_name : String?
}

protocol LoginView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setUserData (data: UserData)
    func checkSession(isValid: Bool)
    func setErrorMessageFromAPI(errorMessage: String)
}

class LoginViewControllerPresenter {
    
    weak fileprivate var loginView : LoginView?
    
    private init() {}
    static let sharedInstance = LoginViewControllerPresenter()
    
    func attachView(view: LoginView) {
        self.loginView = view
    }
    
    func detachView() {
        self.loginView = nil
    }
    
    func login(email: String, password: String) {
        self.loginView?.startLoading(text : "Loading")
        if User.shared.access_token == "" || User.shared.access_token == nil{
            self.loadToken(callBack: {
                APIService.sharedInstance.loginAPI(email: email, password: password, callBack: { [weak self](data) in
                    self?.loginView?.finishLoading()
                    self?.loginView?.setUserData(data: UserData(id: data.id,
                                                                name: data.name,
                                                                email: data.email,
                                                                role: data.role,
                                                                phone: data.phone,
                                                                image: data.image, image_thumb: data.image_thumb,
                                                                country: data.country, company_name: data.company_name))
                }) { (message: String) in
                    self.loginView?.finishLoading()
                    self.loginView?.setErrorMessageFromAPI(errorMessage: message)
                }
                
            }) { (messageError) in
                self.loginView?.finishLoading()
                self.loginView?.setErrorMessageFromAPI(errorMessage: messageError)
            }
        }
        
        else{
            APIService.sharedInstance.loginAPI(email: email, password: password, callBack: { [weak self](data) in
                self?.loginView?.finishLoading()
                self?.loginView?.setUserData(data: UserData(id: data.id,
                                                            name: data.name,
                                                            email: data.email,
                                                            role: data.role,
                                                            phone: data.phone,
                                                            image: data.image, image_thumb: data.image_thumb,
                                                            country: data.country, company_name: data.company_name))
            }) { (message: String) in
                self.loginView?.finishLoading()
                self.loginView?.setErrorMessageFromAPI(errorMessage: message)
            }
        }
        
        
    }
    
    func sessionValid(){
        self.loginView?.startLoading(text : "Loading")
        if User.shared.access_token != "" && User.shared.access_token != nil && User.shared.id != "" && User.shared.id != nil{
            
            APIService.sharedInstance.ProfileRequestAPI(callBack: { [weak self](data) in
                self?.loginView?.finishLoading()
                self?.loginView?.checkSession(isValid: true)
            }) { (message: String) in
                self.loginView?.finishLoading()
                self.loginView?.checkSession(isValid: false)
            }
        }
        else{
            self.loginView?.finishLoading()
            self.loginView?.checkSession(isValid: false)
        }
    }
    
    func loadToken(callBack: @escaping () -> Void, messageError: @escaping (String) -> Void) {
        APIService.sharedInstance.tokenRequestAPI(callBack: { (data)  in
            callBack()
        }) { (message: String) in
            messageError(message)
        }
        
    }
}
