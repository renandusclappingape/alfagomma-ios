//
//  SearchProductViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct SearchProductData{
    var search_product_list : [ProductList.product] = []
    
}

protocol SearchProductView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setProductData (data: SearchProductData)
    func productDetailsCompletion(data: Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class SearchProductViewControllerPresenter {
    
    weak fileprivate var searchProductView : SearchProductView?
    
    private init() {}
    static let sharedInstance = SearchProductViewControllerPresenter()
    
    var isLoading = false
    
    func attachView(view: SearchProductView) {
        self.searchProductView = view
    }
    
    func detachView() {
        self.searchProductView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        if isLoading{return}
        isLoading = true
        self.searchProductView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SearchProductRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            self?.searchProductView?.finishLoading()
            self?.searchProductView?.setProductData(data: SearchProductData(search_product_list: data.product_list))
            self?.isLoading = false
        }) { (message: String) in
            self.searchProductView?.finishLoading()
            self.searchProductView?.setErrorMessageFromAPI(errorMessage: message)
            self.isLoading = false
        }
        
    }
    
    func loadProductDetails(id: String!) {
        if isLoading{return}
        isLoading = true
        self.searchProductView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ProductDetailsRequestAPI(id:id,callBack: { [weak self](data) in
            self?.searchProductView?.finishLoading()
            self?.searchProductView?.productDetailsCompletion(data: data)
            self?.isLoading = false
        }) { (message: String) in
            self.searchProductView?.finishLoading()
            self.searchProductView?.setErrorMessageFromAPI(errorMessage: message)
            self.isLoading = false
        }
        
    }
}
