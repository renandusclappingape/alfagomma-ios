//
//  SearchProductViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class SearchProductViewController: BaseViewController, SearchProductView {
    
    //UICollectionView
    @IBOutlet weak var collectionViewProduct: UICollectionView!
    
    //UITextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UIView
    @IBOutlet weak var searchTextFieldView: UIView!
    @IBOutlet weak var searchControlView: UIView!
    
    //Other
    var productToShow : SearchProductData?
    var lastContentOffset: CGFloat = 0
    var tap : UITapGestureRecognizer!
    var productParam : [String:Any]?
    var searchControlViewAnimating : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productParam = [
            "keyword": SearchTextField.shared.text ?? "",
            "user_id": Int(User.shared.id!) ?? 0,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
            "filter": [:
            ]
        ]
        
        SearchProductViewControllerPresenter.sharedInstance.attachView(view: self)
        self.productToShow = nil
        SearchProductViewControllerPresenter.sharedInstance.load(parameter: self.productParam)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.load),
            name: NSNotification.Name("load_search"),
            object: nil)
        
        self.setupDisplay()
        self.registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Search Product"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchTextField.text = SearchTextField.shared.text
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    override func viewDidLayoutSubviews() {
        self.collectionViewProduct.setContentOffset(CGPoint(x: 0, y: -54), animated: false)
    }
    
    @objc func load(notification: NSNotification){
        self.getSearchResult()
    }
    
    func getSearchResult(){
        self.productParam = [
            "keyword": SearchTextField.shared.text ?? "",
            "user_id": Int(User.shared.id!) ?? 0,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
            "filter": [:
            ]
        ]
        
        self.productToShow = nil
        SearchProductViewControllerPresenter.sharedInstance.load(parameter: self.productParam)
    }

    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageViewSearchBox = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageViewSearchBox.image = image;
        viewField.addSubview(imageViewSearchBox)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        self.searchTextField.delegate = self
        self.searchTextField.text = SearchTextField.shared.text
        
        self.view.layoutIfNeeded()
        self.collectionViewProduct.contentInset = UIEdgeInsets(top: 54, left: 0, bottom: 71, right: 0)
    }
    
    @objc func ScanQrButtonAction() {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func sortButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "SearchControlHost", bundle: nil)
        let searchControlHostNav = storyboard.instantiateViewController(withIdentifier: "SearchControlHostNavVC") as! UINavigationController
        let searchControlHost = searchControlHostNav.viewControllers[0] as! SearchControlHostViewController
        
        searchControlHost.showSort = true
        searchControlHost.searchControlHostDelegate = self
        
        self.present(searchControlHostNav, animated: true) {
            
        }
    }
    
    @IBAction func filterButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "SearchControlHost", bundle: nil)
        let searchControlHostNav = storyboard.instantiateViewController(withIdentifier: "SearchControlHostNavVC") as! UINavigationController
        let searchControlHost = searchControlHostNav.viewControllers[0] as! SearchControlHostViewController
        
        searchControlHost.showFilter = true
        searchControlHost.searchControlHostDelegate = self
        searchControlHost.param = self.productParam
        
        self.present(searchControlHostNav, animated: true) {
            
        }
    }
    
    // Mark: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setProductData(data: SearchProductData) {
        if self.productToShow == nil {
            self.productToShow = data
        }
        else{
            self.productToShow?.search_product_list += data.search_product_list
        }
        self.collectionViewProduct.reloadData()
    }
    
    func productDetailsCompletion(data: Product) {
        let storyboard = UIStoryboard(name: "ProductDetailsHost", bundle: nil)
        let headerController = storyboard
            .instantiateViewController(withIdentifier: "ProductDetailsHostNav") as! UINavigationController
        
        let hostController = headerController.viewControllers.first as! ProductDetailsHostViewController
        hostController.productDetailsData = data
        hostController.isFromSearch = true
        
        self.present(headerController, animated: true) {
            
        }
        
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
}

extension SearchProductViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productToShow?.search_product_list.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyProductCollectionViewCell", for: indexPath as IndexPath) as! MyProductCollectionViewCell
        
        cell.productImageView.image = UIImage(named: "placeholder-image")
        if (self.productToShow?.search_product_list[indexPath.row].image.count)! > 0 {
            let thumbImageURL = URL(string:"\(Constant.BaseURL)\(self.productToShow?.search_product_list[indexPath.row].image[0].image_thumb ?? "")")
            let imageURL = URL(string:"\(Constant.BaseURL)\(self.productToShow?.search_product_list[indexPath.row].image[0].image ?? "")")
            cell.productImageView?.sd_setImage(with: thumbImageURL, completed: {
                (image, error, cacheType, url) in
                cell.productImageView?.sd_setImage(with: imageURL, completed: {
                    (image, error, cacheType, url) in
                })
            })
        }
        cell.productNameLabel.text = self.productToShow?.search_product_list[indexPath.row].title
        cell.productUnitLabel.text = "Length :\n\(self.productToShow?.search_product_list[indexPath.row].length_value ?? 0) \(self.productToShow?.search_product_list[indexPath.row].length_unit ?? "")"
        cell.productCodeLabel.text = "Product Number :\n\(self.productToShow?.search_product_list[indexPath.row].number ?? "")"
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SearchProductViewControllerPresenter.sharedInstance.loadProductDetails(id: self.productToShow?.search_product_list[indexPath.row].id ?? "")
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.size.width-(8*3))/2
        
        return CGSize(width: width, height: width*1.67)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionViewProduct{
            
            var sbFrame: CGRect = self.searchTextFieldView.frame
            
            //Set frame from scroll distance
            if (self.lastContentOffset > scrollView.contentOffset.y) {
                sbFrame.origin.y = self.searchTextFieldView.frame.origin.y - (scrollView.contentOffset.y-self.lastContentOffset)
                
                if !self.searchControlViewAnimating{
                    self.searchControlViewAnimating = true
                    UIView.animate(withDuration: 0.2, animations: {
                        self.searchControlView.alpha = 1
                    }) { (finish : Bool) in
                        self.searchControlViewAnimating = false
                    }
                }
            }
            else if (self.lastContentOffset < scrollView.contentOffset.y) {
                sbFrame.origin.y = self.searchTextFieldView.frame.origin.y + (self.lastContentOffset-scrollView.contentOffset.y)
                
                if !self.searchControlViewAnimating && self.lastContentOffset >= scrollView.contentInset.top{
                    self.searchControlViewAnimating = true
                    UIView.animate(withDuration: 0.2, animations: {
                        self.searchControlView.alpha = 0
                    }) { (finish : Bool) in
                        self.searchControlViewAnimating = false
                    }
                }
            }
            
            self.lastContentOffset = scrollView.contentOffset.y
            
            //SearchView Y cannot be more than 0
            if sbFrame.origin.y > 0 {
                sbFrame.origin.y = 0
            }
            
            //SearchView Y cannot be less than height it self
            if sbFrame.origin.y < -self.searchTextFieldView.frame.size.height{
                sbFrame.origin.y = -self.searchTextFieldView.frame.size.height
            }
            
            //Condition when scroll bounce on top
            if (scrollView.contentOffset.y < 0) {
                sbFrame.origin.y = 0
                
                if !self.searchControlViewAnimating{
                    self.searchControlViewAnimating = true
                    UIView.animate(withDuration: 0.2, animations: {
                        self.searchControlView.alpha = 1
                    }) { (finish : Bool) in
                        self.searchControlViewAnimating = false
                    }
                }
            }
            
            //Condition when scroll bounce on bottom
            if (scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height)) {
                sbFrame.origin.y = -self.searchTextFieldView.frame.size.height
            }
            
            self.searchTextFieldView.frame = sbFrame
        }
    }
    
}

extension SearchProductViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}

extension SearchProductViewController: UITextFieldDelegate{
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        self.getSearchResult()
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        SearchTextField.shared.text = updatedString
        
        return true
    }
}

extension SearchProductViewController: SearchControlHostDelegate{
    func completion(data: [String : Any]) {
        let filterParam : [String:Any] = self.productParam?["filter"] as? [String : Any] ?? [:]
        self.productParam!["filter"] = filterParam.merging(data, uniquingKeysWith: { (_, last) in last })
        self.productToShow = nil
        self.collectionViewProduct.reloadData()
        SearchProductViewControllerPresenter.sharedInstance.load(parameter: self.productParam)
    }
}
