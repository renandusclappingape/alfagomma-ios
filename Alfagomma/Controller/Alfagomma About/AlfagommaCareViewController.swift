//
//  AlfagommaCareViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 07/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class AlfagommaCareViewController: BaseViewController, CareView {
    
    //UITextField
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    var tap : UITapGestureRecognizer!
    
    var shouldScroll = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AlfagommaCareViewControllerPresenter.sharedInstance.attachView(view: self)
        self.messageTextView.delegate = self

        self.registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Presenter delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func contactSuccess() {
        self.nameTextField.text = ""
        self.emailTextField.text = ""
        self.subjectTextField.text = ""
        self.messageTextView.text = ""
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    @IBAction func sendMessageButtonAction(_ sender: Any) {
        if self.nameTextField.text == "" || self.nameTextField.text == nil {
            return
        }
        if self.emailTextField.text == "" || self.emailTextField.text == nil {
            return
        }
        if self.messageTextView.text == "" || self.messageTextView.text == nil {
            return
        }
        
        let userLat = "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)"
        let userLong = "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)"
        let param : [String:Any] = ["user_id":Int(User.shared.id) ?? 0,
                     "name":self.nameTextField.text!,
                     "email":self.emailTextField.text!,
                     "latitude":userLat,
                     "longitude":userLong,
                     "subject":self.subjectTextField.text!,
                     "message":self.messageTextView.text!]
        AlfagommaCareViewControllerPresenter.sharedInstance.sendMessage(param: param)
    }
}

extension AlfagommaCareViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let sizeToFitIn = CGSize(width : self.messageTextView.frame.size.width,height: CGFloat(MAXFLOAT))
        let newSize = self.messageTextView.sizeThatFits(sizeToFitIn)
        
        if self.messageTextViewHeightConstraint.constant < newSize.height{
            shouldScroll = true
        }
        else{
            shouldScroll = false
        }
        
        self.messageTextViewHeightConstraint.constant = newSize.height
        
        if shouldScroll{
            let bottomOffset = CGPoint(x: 0, y: (scrollView.contentSize.height+scrollView.contentInset.bottom) - scrollView.frame.size.height)
            scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
}

extension AlfagommaCareViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if self.scrollView.getSelectedTextField() != nil
        {
            let textField = self.scrollView.getSelectedTextField()
            var resultFrame = CGRect.zero
            resultFrame = self.scrollView.convert((textField?.frame)!, from: textField?.superview)
            
            if (!aRect.contains(resultFrame))
            {
                self.scrollView.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        else if self.scrollView.getSelectedTextView() != nil
        {
            let textView = self.scrollView.getSelectedTextView()
            var resultFrame = CGRect.zero
            resultFrame = self.scrollView.convert((textView?.frame)!, from: textView?.superview)
            
            if (!aRect.contains(resultFrame))
            {
                self.scrollView.scrollRectToVisible(resultFrame, animated: true)
            }
        }
        
        
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
}
