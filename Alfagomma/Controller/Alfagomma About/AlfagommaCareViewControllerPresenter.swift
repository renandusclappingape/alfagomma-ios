//
//  AlfagommaCareViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 05/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol CareView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func contactSuccess()
    func setErrorMessageFromAPI(errorMessage: String)
}

class AlfagommaCareViewControllerPresenter {
    
    weak fileprivate var careView : CareView?
    
    private init() {}
    static let sharedInstance = AlfagommaCareViewControllerPresenter()
    
    func attachView(view: CareView) {
        self.careView = view
    }
    
    func detachView() {
        self.careView = nil
    }
    
    func sendMessage(param: [String:Any]) {
        self.careView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ContactAPI(params: param,callBack: { () in
            self.careView?.finishLoading()
            self.careView?.contactSuccess()
        }) { (message: String) in
            self.careView?.finishLoading()
            self.careView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
}
