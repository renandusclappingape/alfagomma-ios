//
//  AlfagommaAboutViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 07/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class AlfagommaAboutViewController: UIViewController {
    
    @IBOutlet weak var aboutUsButton: UIButton!
    @IBOutlet weak var careButton: UIButton!
    
    //UITextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UIView
    @IBOutlet weak var searchTextFieldView: UIView!
    @IBOutlet weak var aboutUsView: UIView!
    @IBOutlet weak var careView: UIView!
    
    let indicatorView = ActiveButtonIndicator.init()
    var tap : UITapGestureRecognizer!
    var enableRightItem = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupDisplay()
        self.registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Alfagomma"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.indicatorView.addToView(view: self.careButton)
        self.searchTextField.text = SearchTextField.shared.text
        SearchTextField.shared.setActiveViewController(viewController: self)
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Alfagomma"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let myimage = UIImage(named: "icon-bar-qr")?.withRenderingMode(.alwaysOriginal)
        if enableRightItem{
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: myimage, style: .plain, target: self, action: #selector(ScanQrButtonAction))
        }
        
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageViewSearchBox = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageViewSearchBox.image = image;
        viewField.addSubview(imageViewSearchBox)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        self.searchTextField.delegate = SearchTextField.shared
        self.searchTextField.text = SearchTextField.shared.text
        
        self.aboutUsView.isHidden = true
    }
    
    @objc func ScanQrButtonAction() {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func aboutUsButtonAction(_ sender: Any) {
        let url = URL(string: "http://www.alfagomma.com/en/")
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
//        self.indicatorView.moveToView(view: self.aboutUsButton)
//        self.careView.isHidden = true
//        self.aboutUsView.isHidden = false
    }
    
    @IBAction func careButtonAction(_ sender: Any) {
        self.indicatorView.moveToView(view: self.careButton)
        self.careView.isHidden = false
        self.aboutUsView.isHidden = true
    }
    
}

extension AlfagommaAboutViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}
