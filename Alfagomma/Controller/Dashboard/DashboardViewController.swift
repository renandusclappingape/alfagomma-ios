//
//  DashboardViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import FSPagerView
import SDWebImage

class DashboardViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, DashboardView {
    
    //UITextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UICollectionView
    @IBOutlet weak var collectionVIewMenu: UICollectionView!
    
    //UIView
    @IBOutlet weak var searchTextFieldView: UIView!
    @IBOutlet weak var bannerView: UIView!
    
    //UIScrollView
    @IBOutlet weak var scrollView: UIScrollView!
    
    //UILabel
    @IBOutlet weak var greetLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    
    //Other
    @IBOutlet weak var collectionviewHeightConstraint: NSLayoutConstraint!
    var tap : UITapGestureRecognizer!
    
    var pagerView : FSPagerView = FSPagerView()
    var pageControl : FSPageControl = FSPageControl()
    
    private var lastContentOffset: CGFloat = 0
    var menuList = [["image" : "icon-dashboard-myproduct",
                    "title" : "My Product"],
                    ["image" : "icon-dashboard-scan",
                     "title" : "Scan Product"],
//                    ["image" : "icon-dashboard-alfagomma",
//                     "title" : "Alfagomma"],
                    ["image" : "icon-setting",
                     "title" : "Settings"],
                    ["image" : "icon-calendar",
                     "title" : "Calendar"],
                    ["image" : "icon-mail",
                     "title" : "Contact Us"],
                    ["image" : "icon-logout",
                     "title" : "Logout"]]
    
    var bannerList : BannerData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupDisplay()
        self.registerNotification()
        NotificationCenter.default.addObserver(self, selector: #selector(logout(notification:)), name:  Notification.Name("logoutNotification"), object: nil)
        DashboardViewControllerPresenter.sharedInstance.attachView(view: self)
        DashboardViewControllerPresenter.sharedInstance.loadBanner()
        DashboardViewControllerPresenter.sharedInstance.loadProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBarController?.tabBar.isHidden = true
        self.setGreetText()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchTextField.text = SearchTextField.shared.text
        SearchTextField.shared.setActiveViewController(viewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let logo = UIImage(named: "logo-bar-alfagoma")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.AlfagommaTapped))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(recognizer)
        
        let myimage = UIImage(named: "icon-bar-qr")?.withRenderingMode(.alwaysOriginal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: myimage, style: .plain, target: self, action: #selector(ScanQrButtonAction))
        
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageViewSearchBox = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageViewSearchBox.image = image;
        viewField.addSubview(imageViewSearchBox)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        self.searchTextField.delegate = SearchTextField.shared
        self.searchTextField.text = SearchTextField.shared.text
        
        self.view.layoutIfNeeded()
        self.scrollView.contentInset = UIEdgeInsets(top: 38, left: 0, bottom: 0, right: 0)
        
        self.collectionVIewMenu.isScrollEnabled = false
        self.collectionVIewMenu.reloadData()
        self.view.layoutIfNeeded()
        self.setCollectionViewHeight()
        
        self.scrollView.bounces = false
        self.scrollView.isScrollEnabled = false
        
        self.initBanner()
    }
    
    @objc private func AlfagommaTapped() {
        let url = URL(string: "http://www.alfagomma.com/en/")
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    func setGreetText(){
        let greetText = "Hello, Good "
        let hour = Calendar.current.component(.hour, from: Date())
        switch hour {
        case 0..<12 : self.greetLabel.text = greetText + "Morning!"
        case 12..<18 : self.greetLabel.text = greetText + "Afternoon!"
        default: self.greetLabel.text = greetText + "Evening!"
        }
    }
    
    func initBanner(){
        // Create a pager view
        let bannerFreme = CGRect(x: 0, y: 0, width: self.bannerView.frame.size.width, height: self.bannerView.frame.size.height-24)
        self.pagerView = FSPagerView(frame: bannerFreme)
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
        self.pagerView.isInfinite = true
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.automaticSlidingInterval = 5.0
        self.bannerView.addSubview(self.pagerView)
        // Create a page control
        let pageControlFrame = CGRect(x: 0, y: bannerFreme.height, width: bannerFreme.width, height: 24)
        self.pageControl = FSPageControl(frame: pageControlFrame)
        self.pageControl.numberOfPages = self.bannerList?.banner_list.count ?? 0
        self.pageControl.contentHorizontalAlignment = .center
        self.pageControl.currentPage = 0
        self.pageControl.setFillColor(UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1), for: .normal)
        self.pageControl.setFillColor(UIColor(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 1), for: .selected)
        self.bannerView.addSubview(self.pageControl)
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setUserData(data: UserData) {
        self.nameLabel.text = data.name
        self.emailLabel.text = data.email
        self.phoneNumberLabel.text = data.phone
    }
    
    func setBannerData(data: BannerData) {
        self.bannerList = data
        self.pagerView.reloadData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    @objc func ScanQrButtonAction() {
        self.tabBarController?.selectedIndex = 2
    }
    
    override func viewDidLayoutSubviews() {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: -38), animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView{
            
            var sbFrame: CGRect = self.searchTextFieldView.frame
            
            //Set frame from scroll distance
            if (self.lastContentOffset > scrollView.contentOffset.y) {
                sbFrame.origin.y = self.searchTextFieldView.frame.origin.y - (scrollView.contentOffset.y-self.lastContentOffset)
            }
            else if (self.lastContentOffset < scrollView.contentOffset.y) {
                sbFrame.origin.y = self.searchTextFieldView.frame.origin.y + (self.lastContentOffset-scrollView.contentOffset.y)
            }
            
            self.lastContentOffset = scrollView.contentOffset.y
            
            //SearchView Y cannot be more than 0
            if sbFrame.origin.y > 0 {
                sbFrame.origin.y = 0
            }
            
            //SearchView Y cannot be less than height it self
            if sbFrame.origin.y < -self.searchTextFieldView.frame.size.height{
                sbFrame.origin.y = -self.searchTextFieldView.frame.size.height
            }
            
            //Condition when scroll bounce on top
            if (scrollView.contentOffset.y < 0) {
                sbFrame.origin.y = 0
            }
            
            //Condition when scroll bounce on bottom
            if (scrollView.contentOffset.y > (scrollView.contentSize.height - scrollView.frame.size.height)) {
                sbFrame.origin.y = -self.searchTextFieldView.frame.size.height
            }
            
            self.searchTextFieldView.frame = sbFrame
        }
    }

    
    //Set CollectionVIew Height
    
    func setCollectionViewHeight(){
        self.collectionviewHeightConstraint.constant = self.collectionVIewMenu.contentSize.height
    }
    
    // MARK: - UICollectionViewDataSource protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath as IndexPath) as! MenuCollectionViewCell
        
        cell.menuImageView.image = UIImage(named: menuList[indexPath.row]["image"]!)
        cell.menuTitleLabel.text = menuList[indexPath.row]["title"]
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            self.tabBarController?.selectedIndex = 1
        }
        
        if indexPath.row == 1 {
            self.tabBarController?.selectedIndex = 2
        }
        
        if indexPath.row == 2 {
            self.tabBarController?.selectedIndex = 3
        }
        
        if indexPath.row == 3 {
            let storyboard = UIStoryboard(name: "Calendar", bundle: nil)
            let calendarVC = storyboard.instantiateViewController(withIdentifier: "CalendarViewController")
            self.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        if indexPath.row == 4 {
            let storyboard = UIStoryboard(name: "ContactUs", bundle: nil)
            let calendarVC = storyboard.instantiateViewController(withIdentifier: "ContactUsViewController")
            self.navigationController?.pushViewController(calendarVC, animated: true)
        }
        
        if indexPath.row == 5 {
            let alert = UIAlertController(title: "Logout", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: {
                (alert: UIAlertAction!) in
                self.tabBarController?.dismiss(animated: true, completion: {
                    User.shared.clearToken()
                })
            }
            ))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) in
                
            }
            ))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.size.width/3)
        
        return CGSize(width: width, height: 92)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension DashboardViewController: FSPagerViewDataSource, FSPagerViewDelegate{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.bannerList?.banner_list.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        let imageURL = URL(string:"\(self.bannerList?.banner_list[index].image ?? "")")
        cell.imageView?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder-image"))
        cell.imageView?.contentMode = .scaleAspectFill
        return cell
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        guard let url = URL(string: self.bannerList?.banner_list[index].link_url ?? "") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

extension DashboardViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}

extension DashboardViewController{
    @objc func logout(notification: NSNotification) {
        User.shared.clearToken()
        self.tabBarController?.dismiss(animated: true) {
            
        }
    }
}
