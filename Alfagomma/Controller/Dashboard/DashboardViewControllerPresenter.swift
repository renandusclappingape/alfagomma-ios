//
//  DashboardViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 25/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct BannerData{
    var banner_list : [BannerList.banner] = []
    
}

protocol DashboardView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setUserData (data: UserData)
    func setBannerData (data: BannerData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class DashboardViewControllerPresenter {
    
    weak fileprivate var dashboardView : DashboardView?
    
    private init() {}
    static let sharedInstance = DashboardViewControllerPresenter()
    
    func attachView(view: DashboardView) {
        self.dashboardView = view
    }
    
    func detachView() {
        self.dashboardView = nil
    }
    
    func loadProfile() {
        self.dashboardView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ProfileRequestAPI(callBack: { [weak self](data) in
            self?.dashboardView?.finishLoading()
            self?.dashboardView?.setUserData(data: UserData(id: data.id, name: data.name, email: data.email, role: data.role, phone: data.phone, image: data.image, image_thumb: data.image_thumb, country: data.country, company_name: data.company_name))
        }) { (message: String) in
            self.dashboardView?.finishLoading()
            self.dashboardView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
    func loadBanner(){
        APIService.sharedInstance.BannerRequestAPI(callBack: { [weak self](data) in
            self?.dashboardView?.setBannerData(data: BannerData(banner_list: data.banner_list))
        }) { (message: String) in
            self.dashboardView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
