//
//  MenuCollectionViewCell.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuTitleLabel: UILabel!
}
