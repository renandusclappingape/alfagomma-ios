//
//  ScanQRViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 23/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import AVFoundation

class ScanQRViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate, ScanQRView {
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var scanAreaView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ScanQRViewControllerPresenter.sharedInstance.attachView(view: self)
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .qr]
        } else {
            failed()
            return
        }
        
        self.view.layoutIfNeeded()
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = self.scanAreaView.layer.frame
        previewLayer.videoGravity = .resizeAspectFill
        self.scanAreaView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
        
        self.setupDisplay()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        SearchTextField.shared.setActiveViewController(viewController: self)
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay(){
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
        
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
    }
    
    func found(code: String) {
        let param = [
            "product_number": code,
            "user" : [
            "user_id": User.shared.id!,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)"]
            ] as [String : Any]
        ScanQRViewControllerPresenter.sharedInstance.claimProductDetails(parameters: param)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // MARK: Presnter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func productDetailsCompletion(data: Product) {
        let storyboard = UIStoryboard(name: "ProductDetailsHost", bundle: nil)
        let headerController = storyboard
            .instantiateViewController(withIdentifier: "ProductDetailsHostNav") as! UINavigationController
        
        let hostController = headerController.viewControllers.first as! ProductDetailsHostViewController
        hostController.productDetailsData = data
        
        self.present(headerController, animated: true) {
            
        }
    }
    
    func productClaimCompletion(data: Product) {
        let alert = UIAlertController(title: "Congratulations", message: "This product successfully claimed.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
            let param : [String : Any] = [
                "keyword": data.number,
                "user_id": Int(User.shared.id!) ?? 0,
                "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
                "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
                "filter": [:
                ]
            ]
            ScanQRViewControllerPresenter.sharedInstance.searchProduct(parameter: param)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func productDetailsCompletion(data: Product?){
        if data == nil{
            let ac = UIAlertController(title: "", message: "Product not found.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert) in
                self.captureSession.startRunning()
            }))
            present(ac, animated: true)
            return
        }
        
        let storyboard = UIStoryboard(name: "ProductDetailsHost", bundle: nil)
        let headerController = storyboard
            .instantiateViewController(withIdentifier: "ProductDetailsHostNav") as! UINavigationController
        
        let hostController = headerController.viewControllers.first as! ProductDetailsHostViewController
        hostController.productDetailsData = data
        
        self.present(headerController, animated: true) {
            
        }
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {self.captureSession.startRunning()})
        
    }
    
}
