//
//  ScanQRViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 02/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol ScanQRView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func productDetailsCompletion(data: Product)
    func productClaimCompletion(data: Product)
    func setErrorMessageFromAPI(errorMessage: String)
    func productDetailsCompletion(data: Product?)
}

class ScanQRViewControllerPresenter {
    
    weak fileprivate var scanQRView : ScanQRView?
    
    private init() {}
    static let sharedInstance = ScanQRViewControllerPresenter()
    
    func attachView(view: ScanQRView) {
        self.scanQRView = view
    }
    
    func detachView() {
        self.scanQRView = nil
    }
    
    func loadProductDetails(id: String!) {
        APIService.sharedInstance.ProductDetailsRequestAPI(id:id,callBack: { [weak self](data) in
            self?.scanQRView?.finishLoading()
            self?.scanQRView?.productDetailsCompletion(data: data)
        }) { (message: String) in
            self.scanQRView?.finishLoading()
            self.scanQRView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
    func claimProductDetails(parameters: [String:Any]!) {
        self.scanQRView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ClaimProductServiceAPI(parameters: parameters,callBack: { [weak self](data) in
            self?.scanQRView?.finishLoading()
            self?.scanQRView?.productClaimCompletion(data: data)
        }) { (message: String) in
            self.scanQRView?.finishLoading()
            self.scanQRView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
    func searchProduct(parameter: [String:Any]? = nil) {
        self.scanQRView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SearchProductRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            let productData = InputProductSpecificationData(search_product_list: data.product_list)
            if productData.search_product_list.count > 0{
                self?.loadProductDetails(id: productData.search_product_list[0].id)
            }
            else{
                self?.scanQRView?.productDetailsCompletion(data: nil)
                self?.scanQRView?.finishLoading()
            }
        }) { (message: String) in
            self.scanQRView?.finishLoading()
            self.scanQRView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
}
