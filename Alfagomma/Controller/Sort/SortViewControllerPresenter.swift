//
//  SortViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 26/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct SortData{
    var sort_list : [SortList.sort] = []
    
}

protocol SortView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setSortData (data: SortData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class SortViewControllerPresenter {
    
    weak fileprivate var sortView : SortView?
    
    private init() {}
    static let sharedInstance = SortViewControllerPresenter()
    
    func attachView(view: SortView) {
        self.sortView = view
    }
    
    func detachView() {
        self.sortView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        self.sortView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SortRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            self?.sortView?.finishLoading()
            self?.sortView?.setSortData(data: SortData(sort_list: data.sort_list))
        }) { (message: String) in
            self.sortView?.finishLoading()
            self.sortView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
}
