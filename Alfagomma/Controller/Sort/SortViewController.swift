//
//  SortViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

protocol SortViewControllerDelegate: NSObjectProtocol {
    func completion(data:[String:Any])
}

class SortViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, SortView {
    
    @IBOutlet weak var tableViewSort: UITableView!
    
    var sortViewControllerDelegate : SortViewControllerDelegate?
    
    var sortItem : SortData?
    
    var param : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupDisplay()
        
        SortViewControllerPresenter.sharedInstance.attachView(view: self)
        SortViewControllerPresenter.sharedInstance.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Sort"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
    }
    
    // MARK: Presenter Delegate
    func startLoading(text: String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setSortData(data: SortData) {
        self.sortItem = data
        self.tableViewSort.reloadData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }

    // UItableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortItem?.sort_list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OptionTableViewCell") as! OptionTableViewCell
        
        cell.titleLabel.text = self.sortItem?.sort_list[indexPath.row].title ?? ""
        cell.iconIndicatorImageView.isHidden = true
        
        let filterParam : [String:Any] = self.param?["filter"] as? [String : Any] ?? [:]
        if let selectedSort = filterParam["sort"] as? String{
            if self.sortItem?.sort_list[indexPath.row].id == selectedSort{
                cell.accessoryType = .checkmark
                cell.tintColor = Color.redErrorColor
            }
            else{
                cell.accessoryType = .disclosureIndicator
                cell.tintColor = UIColor.lightGray
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sortViewControllerDelegate?.completion(data: ["sort":sortItem?.sort_list[indexPath.row].id ?? ""])
    }
    
}
