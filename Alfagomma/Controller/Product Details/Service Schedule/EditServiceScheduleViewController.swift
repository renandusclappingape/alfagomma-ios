//
//  EditServiceScheduleViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 31/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol ServiceChangeDelegate : NSObjectProtocol {
    func productServiceDidChange(data : Product)
}

class EditServiceScheduleViewController: BaseViewController, EditServiceScheduleView {
    
    @IBOutlet var calendarView: JTAppleCalendarView!
    
    //UITextField
    @IBOutlet weak var yearTextField: UITextField!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var prevMonthLabel: UILabel!
    @IBOutlet weak var nextMonthLabel: UILabel!
    
    @IBOutlet weak var nextServiceButton: UIButton!
    @IBOutlet weak var lastServiceButton: UIButton!
    @IBOutlet weak var activeServiceIndicatorView: UIView!
    
    @IBOutlet weak var activeServiceIndicatorLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var activeServiceIndicatorRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var activeServiceIndicatorLeftNextServiceConstraint: NSLayoutConstraint!
    @IBOutlet weak var activeServiceIndicatorRightNextServiceConstraint: NSLayoutConstraint!
    
    var picker: UIPickerView!
    
    var tap : UITapGestureRecognizer!
    var isLastServiceActive = true
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var yearList : [Int] = []
    var selectedYear : Int?
    var visibleDateOnCalendar : DateSegmentInfo?
    
    var lastServiceSelected : Date? = nil
    var nextServiceSelected : Date? = nil
    
    var delegate : ServiceChangeDelegate?
    
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData

        EditServiceScheduleViewControllerPresenter.sharedInstance.attachView(view: self)
        
        self.lastServiceSelected = convertUnixTimeStamp(timestamp: self.productDetailsData?.service.last_service ?? 0)
        self.nextServiceSelected = convertUnixTimeStamp(timestamp: self.productDetailsData?.service.next_service ?? 0)
        
        self.calendarView.reloadData()
        
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
            self.setSelectedDate(date: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.last_service ?? 0))
            self.calendarView.scrollToDate(convertUnixTimeStamp(timestamp: self.productDetailsData?.service.last_service ?? 0))
        }
        
        self.setupDisplay()
        self.registerNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay(){
        self.picker = UIPickerView()
        self.picker.delegate = self
        self.picker.dataSource = self
        self.yearTextField.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.yearTextField.inputView = picker
        self.yearTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func cancelPicker(){
        self.yearTextField.resignFirstResponder()
    }
    
    @objc func donePicker(){
        if self.selectedYear == nil || self.selectedYear == 0 {
            self.yearTextField.resignFirstResponder()
            return
        }
        self.yearTextField.text = String(self.selectedYear ?? 0)
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from: self.visibleDateOnCalendar?.monthDates.first?.date ?? Date())
        dateComponents.year = selectedYear
        
        let date: Date = calendar.date(from: dateComponents)!
        self.calendarView.scrollToDate(date, triggerScrollToDateDelegate: true, animateScroll: true, preferredScrollPosition: nil, extraAddedOffset: 0) {
            
        }
        self.yearTextField.resignFirstResponder()
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setServiceSuccess(data: Product) {
        data.service.last_service = convertDateToUnixTimeStamp(date: self.lastServiceSelected!)
        data.service.next_service =  convertDateToUnixTimeStamp(date: self.nextServiceSelected!)
        self.delegate?.productServiceDidChange(data: data)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        self.visibleDateOnCalendar = visibleDates
        
        self.setCurrentMonth(visibleDates: visibleDates)
        self.setPreviousMonth(visibleDates: visibleDates)
        self.setNextMonth(visibleDates: visibleDates)
        
    }
    
    func setCurrentMonth(visibleDates: DateSegmentInfo){
        guard let startDate = visibleDates.monthDates.first?.date else {
            monthLabel.text = ""
            yearTextField.text = ""
            return
        }
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = Calendar.current.component(.year, from: startDate)
        monthLabel.text = monthName
        yearTextField.text = String(year)
    }
    
    func setPreviousMonth(visibleDates: DateSegmentInfo){
        guard let prevDate = visibleDates.monthDates.first?.date else {
            prevMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:prevDate)
        dateComponents.month = dateComponents.month!-1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        prevMonthLabel.text = monthName
    }
    
    func setNextMonth(visibleDates: DateSegmentInfo){
        guard let nextDate = visibleDates.monthDates.first?.date else {
            nextMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:nextDate)
        dateComponents.month = dateComponents.month!+1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        nextMonthLabel.text = monthName
    }

    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else { return }
        handleCellTextColor(view: myCustomCell, cellState: cellState)
        handleCellSelection(view: myCustomCell, cellState: cellState)
    }
    
    func handleCellSelection(view: CellView, cellState: CellState) {
        if calendarView.allowsMultipleSelection {
            switch cellState.selectedPosition() {
            case .full: view.backgroundColor = .green
            case .left: view.backgroundColor = .yellow
            case .right: view.backgroundColor = .red
            case .middle: view.backgroundColor = .blue
            case .none: view.backgroundColor = nil
            }
        } else {
            if cellState.isSelected {
                view.selectedView.backgroundColor = UIColor.red
                view.dayLabel.textColor = .white
            } else {
                view.selectedView?.backgroundColor = UIColor.white
                handleCellTextColor(view: view, cellState: cellState)
            }
        }
    }
    func handleCellTextColor(view: CellView, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth {
            view.dayLabel.textColor = UIColor.black
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTMedium, size: 14)
        } else {
            view.dayLabel.textColor = UIColor.gray
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTBook, size: 14)
        }
        
        switch cellState.day{
        case .sunday: view.dayLabel.textColor = UIColor.red
        case .monday: break
        case .tuesday: break
        case .wednesday: break
        case .thursday: break
        case .friday: break
        case .saturday: break
        }
    }

    var iii: Date?
    
    
    
    @IBAction func lastServiceButtonAction(_ sender: Any) {
        if self.isLastServiceActive {
            return
        }
        
        self.calendarView.deselectAllDates()
        self.calendarView.reloadData()
        
        UIView.animate(withDuration: 0.1) {
            self.activeServiceIndicatorLeftConstraint.priority = UILayoutPriority(rawValue: 999)
            self.activeServiceIndicatorRightConstraint.priority = UILayoutPriority(rawValue: 999)
            self.activeServiceIndicatorLeftNextServiceConstraint.priority = .defaultLow
            self.activeServiceIndicatorRightNextServiceConstraint.priority = .defaultLow
            self.view.layoutIfNeeded()
        }
        
        self.isLastServiceActive = true

        if lastServiceSelected != nil{
            self.setSelectedDate(date: self.lastServiceSelected!)
            self.calendarView.scrollToDate(self.lastServiceSelected!)
        }
        else{
            self.calendarView.scrollToSegment(.start)
        }
        
        
    }
    
    @IBAction func nextServiceButtonAction(_ sender: Any) {
        if !self.isLastServiceActive {
            return
        }
        
        self.calendarView.deselectAllDates()
        self.calendarView.reloadData()
        
        UIView.animate(withDuration: 0.1) {
            self.activeServiceIndicatorLeftConstraint.priority = .defaultLow
            self.activeServiceIndicatorRightConstraint.priority = .defaultLow
            self.activeServiceIndicatorLeftNextServiceConstraint.priority = UILayoutPriority(rawValue: 999)
            self.activeServiceIndicatorRightNextServiceConstraint.priority = UILayoutPriority(rawValue: 999)
            self.view.layoutIfNeeded()
        }
        
        self.isLastServiceActive = false
        
        if nextServiceSelected != nil{
            self.setSelectedDate(date: self.nextServiceSelected!)
            self.calendarView.scrollToDate(self.nextServiceSelected!)
        }
        else{
            self.calendarView.scrollToSegment(.start)
        }
        
    }
    
    func setSelectedDate(date: Date){
        self.calendarView.selectDates([date])
    }
    
    @IBAction func nextMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.next)
    }
    
    @IBAction func prevMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.previous)
    }
    
    @IBAction func setProductServiceButtonAction(_ sender: Any) {
        if self.lastServiceSelected == nil && self.nextServiceSelected == nil{
            return
        }
            
        let userLat = "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)"
        let userLong = "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)"
        
        let lastServiceDate = "\(convertDateToUnixTimeStamp(date: self.lastServiceSelected!))"
        let nextServiceDate = "\(convertDateToUnixTimeStamp(date: self.nextServiceSelected!))"
        let immediateServiceStatus = "\(NSNumber(value:self.productDetailsData?.service.immediate_service.status ?? false).intValue)"
        let immediateServiceDate = "\(self.productDetailsData?.service.immediate_service.date ?? 0)"
        let serviceInfo = self.productDetailsData?.service.service_info ?? ""
        let maintenanceInfo = self.productDetailsData?.service.maintenance_info ?? ""
        let notes = self.productDetailsData?.service.immediate_service.notes ?? ""
        
        let params : [String:Any]! = ["user":["user_id":Int(User.shared.id) ?? 0,
                              "user_latitude":userLat,
                              "user_longitude":userLong],
                                      "service":["product_id": self.productDetailsData?.id ?? "",
                                 "last_service": lastServiceDate,
                                 "next_service": nextServiceDate,
                                 "service_info": serviceInfo,
                                 "maintenance_info": maintenanceInfo,
                                 "immediate_service": ["status": immediateServiceStatus,
                                                       "date": immediateServiceDate,
                                                       "notes": notes]]]
        
        EditServiceScheduleViewControllerPresenter.sharedInstance.setService(param : params)
    }
    
    
}

extension EditServiceScheduleViewController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "cell", for: indexPath) as! CellView
        configureCell(view: cell, cellState: cellState)
        cell.dayLabel .text = cellState.text
        
        return cell
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        var startDate = Date()
        if isLastServiceActive{
            startDate = convertUnixTimeStamp(timestamp: self.productDetailsData?.service.last_service ?? 0)
        }
        else{
            startDate = lastServiceSelected!
        }
        
        let endDate = formatter.date(from: "2100 02 01")!
        
        let startYear = Calendar.current.component(.year, from: startDate)
        let endYear = Calendar.current.component(.year, from: endDate)
        for i in Int(startYear)..<Int(endYear)+1{
            self.yearList.append(i)
        }
        
        if yearList.count > 0{
            self.yearTextField?.text = String(yearList[0])
        }
        else{
            self.yearTextField?.text = String(startYear)
        }
        
        var myCalendar = Calendar(identifier: .gregorian)
        myCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
        myCalendar.locale = Calendar.current.locale
        
        let parameters = ConfigurationParameters(startDate: convertUnixTimeStamp(timestamp: 0),
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: myCalendar)
        return parameters
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        if !isLastServiceActive{
            if lastServiceSelected != nil{
                if date < self.lastServiceSelected!{
                    return false
                }
            }
        }
        return true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
        if isLastServiceActive{
            self.lastServiceSelected = date
        }
        else{
            self.nextServiceSelected = date
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        calendarView.viewWillTransition(to: size, with: coordinator, anchorDate: iii)
    }
}

extension EditServiceScheduleViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(yearList[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedYear = yearList[row]
    }
}

extension EditServiceScheduleViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}
