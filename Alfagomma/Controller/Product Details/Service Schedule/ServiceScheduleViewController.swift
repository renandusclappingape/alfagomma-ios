//
//  ServiceScheduleViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 31/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ServiceScheduleViewController: BaseViewController, ServiceScheduleView {

    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productNumberLabel: UILabel!
    @IBOutlet weak var lastServiceValueLabel: UILabel!
    @IBOutlet weak var nextServiceValueLabel: UILabel!
    @IBOutlet weak var serviceInfoLabel: UILabel!
    @IBOutlet weak var maintenanceInformationLabel: UILabel!
    @IBOutlet weak var immediateServiceDateLabel: UILabel!
    @IBOutlet weak var immediateServiceNoteLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var requestImmidiateServiceButton: UIButton!
    @IBOutlet weak var requestImmidiateServiceButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var immidiateServiceView: UIView!
    @IBOutlet weak var immidiateServiceViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var maintenanceInformationBottomConstraint: NSLayoutConstraint!
    
    var isImmidiateServiceActive = false
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData

        ServiceScheduleViewControllerPresenter.sharedInstance.attachView(view: self)
        
        self.immidiateServiceView.isHidden = true
        self.immidiateServiceViewBottomConstraint.priority = .defaultLow
        self.view.layoutIfNeeded()
        self.setupDisplay()
        self.showData()
        isFirstLoad = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isFirstLoad{
            self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
            self.showData()
        }
    }
    
    func setupDisplay() {
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func showData(){
        self.productLabel.text = self.productDetailsData?.title
        self.productNumberLabel.text = "Product Number: \(self.productDetailsData?.number ?? "")"
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMMM yyyy"
        self.lastServiceValueLabel.text = dateFormatter.string(from: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.last_service ?? 0))
        self.nextServiceValueLabel.text = dateFormatter.string(from: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.next_service ?? 0))
        self.serviceInfoLabel.text = self.productDetailsData?.service.service_info
        self.maintenanceInformationLabel.text = self.productDetailsData?.service.maintenance_info
        
        self.immediateServiceDateLabel.text = dateFormatter.string(from: convertUnixTimeStamp(timestamp:  self.productDetailsData?.service.immediate_service.date ?? 0))
        self.immediateServiceNoteLabel.text = self.productDetailsData?.service.immediate_service.notes
        
        if (self.productDetailsData?.service.immediate_service.status)!{
            self.setImmediateServiceDisplay()
        }
        else{
            self.setCancelImmediateServiceDisplay()
        }
    }

    @IBAction func requestImmidiateServiceButtonAction(_ sender: Any) {
        
    }
    
    func setImmediateServiceDisplay(){
        UIView.animate(withDuration: 0.2) {
            self.immidiateServiceView.isHidden = false
            self.immidiateServiceViewBottomConstraint.priority = UILayoutPriority(rawValue: 999)
            self.maintenanceInformationBottomConstraint.priority = .defaultLow
            self.requestImmidiateServiceButtonHeightConstraint.constant = 1
            self.requestImmidiateServiceButton.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func cancelImmidiateServiceButtonAction(_ sender: Any) {
        let userLat = "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)"
        let userLong = "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)"
        
        let lastServiceDate = "\(self.productDetailsData?.service.last_service ?? 0)"
        let nextServiceDate = "\(self.productDetailsData?.service.next_service ?? 0))"
        
        let immediateServiceDate = "\(self.productDetailsData?.service.immediate_service.date ?? 0)"
        let serviceInfo = self.productDetailsData?.service.service_info ?? ""
        let maintenanceInfo = self.productDetailsData?.service.maintenance_info ?? ""
        let notes = self.productDetailsData?.service.immediate_service.notes ?? ""
        
        let params : [String:Any]! = ["user":["user_id":Int(User.shared.id) ?? 0,
                                              "user_latitude":userLat,
                                              "user_longitude":userLong],
                                      "service":["product_id": self.productDetailsData?.id ?? "",
                                                 "last_service": lastServiceDate,
                                                 "next_service": nextServiceDate,
                                                 "service_info": serviceInfo,
                                                 "maintenance_info": maintenanceInfo,
                                                 "immediate_service": ["status": false,
                                                                       "date": immediateServiceDate,
                                                                       "notes": notes]]]
        
        ServiceScheduleViewControllerPresenter.sharedInstance.setService(param : params)
    }
    
    func setCancelImmediateServiceDisplay(){
        UIView.animate(withDuration: 0.2) {
            self.immidiateServiceView.isHidden = true
            self.immidiateServiceViewBottomConstraint.priority = .defaultLow
            self.maintenanceInformationBottomConstraint.priority = UILayoutPriority(rawValue: 999)
            self.requestImmidiateServiceButtonHeightConstraint.constant = 54
            self.requestImmidiateServiceButton.isHidden = false
            self.view.layoutIfNeeded()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editservice" {
            let vc = segue.destination as? EditServiceScheduleViewController
            vc?.productDetailsHostViewController = self.productDetailsHostViewController
            vc?.delegate = self
            
        }
        
        if segue.identifier == "requestService" {
            let vc = segue.destination as? RequestImmediateServiceViewController
            vc?.productDetailsHostViewController = self.productDetailsHostViewController
            vc?.delegate = self
            
        }
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setServiceSuccess(data: Product) {
        self.productDetailsData = data
        self.productDetailsHostViewController?.productDetailsData = data
        self.showData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
}

extension ServiceScheduleViewController: ServiceChangeDelegate, RequestImmediateServiceDelegate{
    func productServiceDidChange(data: Product) {
        self.productDetailsData = data
        self.productDetailsHostViewController?.productDetailsData = data
        self.showData()
    }
}

extension ServiceScheduleViewController: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return self.scrollView
    }
}
