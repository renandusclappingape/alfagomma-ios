//
//  RequestImmediateServiceViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 13/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol RequestImmediateServiceDelegate : NSObjectProtocol {
    func productServiceDidChange(data : Product)
}

class RequestImmediateServiceViewController: BaseViewController, RequestImmediateServiceView {

    
    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var noteTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dateCalendarView: UIView!
    @IBOutlet weak var dateFIeldSection: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var dateViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var calendarView: JTAppleCalendarView!
    
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var prevMonthLabel: UILabel!
    @IBOutlet weak var nextMonthLabel: UILabel!
    
    
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    
    var tap : UITapGestureRecognizer!
    
    var picker: UIPickerView!
    
    var delegate : RequestImmediateServiceDelegate?
    
    let formatter = DateFormatter()
    
    var iii: Date?
    
    var selectedDate = Date()
    
    var shouldScroll = false
    
    var yearList : [Int] = []
    var selectedYear : Int?
    var visibleDateOnCalendar : DateSegmentInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
        
        RequestImmediateServiceViewControllerPresenter.sharedInstance.attachView(view: self)

        self.dateTextField.delegate = self
        self.noteTextView.delegate = self
        self.setupDisplay()
        self.showData()
        self.registerNotification()
        
        self.calendarView.reloadData()
        
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
            self.setSelectedDate(date: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.immediate_service.date ?? 0))
            self.calendarView.scrollToDate(convertUnixTimeStamp(timestamp: self.productDetailsData?.service.immediate_service.date ?? 0))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay() {
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.dateCalendarView.isHidden = true
        self.dateViewBottomConstraint.constant = -self.dateViewHeightConstraint.constant
        
        self.noteTextView.layer.borderWidth = 1
        self.noteTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.noteTextView.textContainerInset = UIEdgeInsetsMake(16.0, 16.0, 16.0, 16.0);
        
        self.dateFIeldSection.layer.borderWidth = 1
        self.dateFIeldSection.layer.borderColor = UIColor.lightGray.cgColor
    
        self.picker = UIPickerView()
        self.picker.delegate = self
        self.picker.dataSource = self
        self.yearTextField.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.yearTextField.inputView = picker
        self.yearTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func cancelPicker(){
        self.yearTextField.resignFirstResponder()
    }
    
    @objc func donePicker(){
        if self.selectedYear == nil || self.selectedYear == 0 {
            self.yearTextField.resignFirstResponder()
            return
        }
        self.yearTextField.text = String(self.selectedYear ?? 0)
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from: self.visibleDateOnCalendar?.monthDates.first?.date ?? Date())
        dateComponents.year = selectedYear
        
        let date: Date = calendar.date(from: dateComponents)!
        self.calendarView.scrollToDate(date, triggerScrollToDateDelegate: true, animateScroll: true, preferredScrollPosition: nil, extraAddedOffset: 0) {
            
        }
        self.yearTextField.resignFirstResponder()
    }

    
    func showData(){
        self.navigationItem.title = "Request Immediate Services"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        
        self.view.layoutIfNeeded()
        self.noteTextView.text = self.productDetailsData?.service.immediate_service.notes
        let sizeToFitIn = CGSize(width : self.noteTextView.frame.size.width,height: CGFloat(MAXFLOAT))
        let newSize = self.noteTextView.sizeThatFits(sizeToFitIn)
        self.noteTextViewHeightConstraint.constant = newSize.height
        
        let currentDateUnix = convertDateToUnixTimeStamp(date: Date())
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMMM yyyy"
        self.dateTextField.text = dateFormatter.string(from: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.immediate_service.date ?? currentDateUnix))
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        let userLat = "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)"
        let userLong = "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)"
        
        let lastServiceDate = "\(self.productDetailsData?.service.last_service ?? 0)"
        let nextServiceDate = "\(self.productDetailsData?.service.next_service ?? 0))"
        
        let immediateServiceDate = "\(convertDateToUnixTimeStamp(date: self.selectedDate))"
        let serviceInfo = self.productDetailsData?.service.service_info ?? ""
        let maintenanceInfo = self.productDetailsData?.service.maintenance_info ?? ""
        let notes = self.noteTextView.text ?? ""
        
        let params : [String:Any]! = ["user":["user_id":Int(User.shared.id) ?? 0,
                                              "user_latitude":userLat,
                                              "user_longitude":userLong],
                                      "service":["product_id": self.productDetailsData?.id ?? "",
                                                 "last_service": lastServiceDate,
                                                 "next_service": nextServiceDate,
                                                 "service_info": serviceInfo,
                                                 "maintenance_info": maintenanceInfo,
                                                 "immediate_service": ["status": true,
                                                                       "date": immediateServiceDate,
                                                                       "notes": notes]]]
        
        RequestImmediateServiceViewControllerPresenter.sharedInstance.setService(param : params)
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setServiceSuccess(data: Product) {
        self.delegate?.productServiceDidChange(data: data)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
}

extension RequestImmediateServiceViewController: UITextFieldDelegate, UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let sizeToFitIn = CGSize(width : self.noteTextView.frame.size.width,height: CGFloat(MAXFLOAT))
        let newSize = self.noteTextView.sizeThatFits(sizeToFitIn)
        
        if self.noteTextViewHeightConstraint.constant < newSize.height{
            shouldScroll = true
        }
        else{
            shouldScroll = false
        }
        
        self.noteTextViewHeightConstraint.constant = newSize.height
        
        if shouldScroll{
            let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
            scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.textFieldDidEndEditing(self.dateTextField)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.dateTextField{
            self.noteTextView.resignFirstResponder()
            self.dateCalendarView.isHidden = false
            self.dateViewBottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.scrollView.addGestureRecognizer(tap)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.dateTextField{
            self.dateViewBottomConstraint.constant = -self.dateViewHeightConstraint.constant
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finish) in
                self.dateCalendarView.isHidden = false
                self.scrollView.removeGestureRecognizer(self.tap)
            }
        }
    }

}

extension RequestImmediateServiceViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
        self.textFieldDidEndEditing(self.dateTextField)
    }
}

extension RequestImmediateServiceViewController{
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        self.visibleDateOnCalendar = visibleDates
        
        self.setCurrentMonth(visibleDates: visibleDates)
        self.setPreviousMonth(visibleDates: visibleDates)
        self.setNextMonth(visibleDates: visibleDates)
        
    }
    
    func setCurrentMonth(visibleDates: DateSegmentInfo){
        guard let startDate = visibleDates.monthDates.first?.date else {
            monthLabel.text = ""
            yearTextField.text = ""
            return
        }
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = Calendar.current.component(.year, from: startDate)
        monthLabel.text = monthName
        yearTextField.text = String(year)
    }
    
    func setPreviousMonth(visibleDates: DateSegmentInfo){
        guard let prevDate = visibleDates.monthDates.first?.date else {
            prevMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:prevDate)
        dateComponents.month = dateComponents.month!-1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        prevMonthLabel.text = monthName
    }
    
    func setNextMonth(visibleDates: DateSegmentInfo){
        guard let nextDate = visibleDates.monthDates.first?.date else {
            nextMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:nextDate)
        dateComponents.month = dateComponents.month!+1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        nextMonthLabel.text = monthName
    }
    
    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else { return }
        handleCellTextColor(view: myCustomCell, cellState: cellState)
        handleCellSelection(view: myCustomCell, cellState: cellState)
    }
    
    func handleCellSelection(view: CellView, cellState: CellState) {
        if calendarView.allowsMultipleSelection {
            switch cellState.selectedPosition() {
            case .full: view.backgroundColor = .green
            case .left: view.backgroundColor = .yellow
            case .right: view.backgroundColor = .red
            case .middle: view.backgroundColor = .blue
            case .none: view.backgroundColor = nil
            }
        } else {
            if cellState.isSelected {
                view.selectedView.backgroundColor = UIColor.red
                view.dayLabel.textColor = .white
            } else {
                view.selectedView?.backgroundColor = UIColor.white
                handleCellTextColor(view: view, cellState: cellState)
            }
        }
    }
    func handleCellTextColor(view: CellView, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth{
            view.dayLabel.textColor = UIColor.black
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTMedium, size: 14)
        } else {
            view.dayLabel.textColor = UIColor.gray
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTBook, size: 14)
        }
        
        switch cellState.day{
        case .sunday: view.dayLabel.textColor = UIColor.red
        case .monday: break
        case .tuesday: break
        case .wednesday: break
        case .thursday: break
        case .friday: break
        case .saturday: break
        }
    }
    
    func setSelectedDate(date: Date){
        self.calendarView.selectDates([date])
    }
    
    @IBAction func nextMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.next)
    }
    
    @IBAction func prevMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.previous)
    }
}

extension RequestImmediateServiceViewController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "cell", for: indexPath) as! CellView
        configureCell(view: cell, cellState: cellState)
        cell.dayLabel .text = cellState.text
        
        return cell
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let endDate = formatter.date(from: "2100 02 01")!
        
        let startYear = Calendar.current.component(.year, from: Date())
        let endYear = Calendar.current.component(.year, from: endDate)
        for i in Int(startYear)..<Int(endYear)+1{
            self.yearList.append(i)
        }
        
        if yearList.count > 0{
            self.yearTextField?.text = String(yearList[0])
        }
        else{
            self.yearTextField?.text = String(startYear)
        }
        
        var myCalendar = Calendar(identifier: .gregorian)
        myCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
        myCalendar.locale = Calendar.current.locale
        
        let parameters = ConfigurationParameters(startDate: Date(),
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: myCalendar)
        return parameters
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        return true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
        self.selectedDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMMM yyyy"
        self.dateTextField.text = dateFormatter.string(from:date)
        self.handleTap()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        calendarView.viewWillTransition(to: size, with: coordinator, anchorDate: iii)
    }
    
    
}

extension RequestImmediateServiceViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(yearList[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedYear = yearList[row]
    }
}
