//
//  EditServiceScheduleViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 29/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol EditServiceScheduleView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setServiceSuccess(data : Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class EditServiceScheduleViewControllerPresenter {
    
    weak fileprivate var editServiceScheduleView : EditServiceScheduleView?
    
    private init() {}
    static let sharedInstance = EditServiceScheduleViewControllerPresenter()
    
    func attachView(view: EditServiceScheduleView) {
        self.editServiceScheduleView = view
    }
    
    func detachView() {
        self.editServiceScheduleView = nil
    }
    
    
    func setService(param: [String:Any]){
        self.editServiceScheduleView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SetProductServiceAPI(parameters: param,callBack: { [weak self](data) in
            self?.editServiceScheduleView?.finishLoading()
            self?.editServiceScheduleView?.setServiceSuccess(data: data)
        }) { (message: String) in
            self.editServiceScheduleView?.finishLoading()
            self.editServiceScheduleView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
