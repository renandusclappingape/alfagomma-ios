//
//  RequestImmediateServiceViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 14/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol RequestImmediateServiceView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setServiceSuccess(data : Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class RequestImmediateServiceViewControllerPresenter {
    
    weak fileprivate var requestImmediateServiceView : RequestImmediateServiceView?
    
    private init() {}
    static let sharedInstance = RequestImmediateServiceViewControllerPresenter()
    
    func attachView(view: RequestImmediateServiceView) {
        self.requestImmediateServiceView = view
    }
    
    func detachView() {
        self.requestImmediateServiceView = nil
    }
    
    
    func setService(param: [String:Any]){
        self.requestImmediateServiceView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SetProductServiceAPI(parameters: param,callBack: { [weak self](data) in
            self?.requestImmediateServiceView?.finishLoading()
            self?.requestImmediateServiceView?.setServiceSuccess(data: data)
        }) { (message: String) in
            self.requestImmediateServiceView?.finishLoading()
            self.requestImmediateServiceView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
