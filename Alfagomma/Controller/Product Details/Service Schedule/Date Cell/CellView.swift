//
//  CellView.swift
//  testApplicationCalendar
//
//  Created by JayT on 2016-03-04.
//  Copyright © 2016 OS-Tech. All rights reserved.
//


import JTAppleCalendar

class CellView: JTAppleCell {
    @IBOutlet var selectedView: UIView!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            
        self.selectedView.layer.cornerRadius = self.selectedView.frame.size.height/2
        self.selectedView.layer.masksToBounds = true
    }
}
