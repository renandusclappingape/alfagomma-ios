//
//  ServiceScheduleViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 13/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol ServiceScheduleView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setServiceSuccess(data : Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class ServiceScheduleViewControllerPresenter {
    
    weak fileprivate var serviceScheduleView : ServiceScheduleView?
    
    private init() {}
    static let sharedInstance = ServiceScheduleViewControllerPresenter()
    
    func attachView(view: ServiceScheduleView) {
        self.serviceScheduleView = view
    }
    
    func detachView() {
        self.serviceScheduleView = nil
    }
    
    
    func setService(param: [String:Any]){
        self.serviceScheduleView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SetProductServiceAPI(parameters: param,callBack: { [weak self](data) in
            self?.serviceScheduleView?.finishLoading()
            self?.serviceScheduleView?.setServiceSuccess(data: data)
        }) { (message: String) in
            self.serviceScheduleView?.finishLoading()
            self.serviceScheduleView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
