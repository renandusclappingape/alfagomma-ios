//
//  SelectProductLocationViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 28/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol SelectProductLocationView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setLocationSuccess(data : Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class SelectProductLocationViewControllerPresenter {
    
    weak fileprivate var selectProductLocationView : SelectProductLocationView?
    
    private init() {}
    static let sharedInstance = SelectProductLocationViewControllerPresenter()
    
    func attachView(view: SelectProductLocationView) {
        self.selectProductLocationView = view
    }
    
    func detachView() {
        self.selectProductLocationView = nil
    }
    
    
    func setLocation(param: [String:Any]){
        self.selectProductLocationView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SelectProductLocationAPI(parameters: param,callBack: { [weak self](data) in
            self?.selectProductLocationView?.finishLoading()
            self?.selectProductLocationView?.setLocationSuccess(data: data)
        }) { (message: String) in
            self.selectProductLocationView?.finishLoading()
            self.selectProductLocationView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
