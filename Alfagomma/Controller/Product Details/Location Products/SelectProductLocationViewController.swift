//
//  SelectProductLocationViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 31/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import GoogleMaps

protocol LocationChangeDelegate : NSObjectProtocol {
    func productLocationDidChange(data : Product)
}

class SelectProductLocationViewController: BaseViewController, GMSMapViewDelegate, SelectProductLocationView, CLLocationManagerDelegate {
    
    @IBOutlet weak var productAddressLabel: VerticalAlignUILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    var delegate : LocationChangeDelegate?
    
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    var marker = GMSMarker()
    let baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?"
    let apikey = Constant.GoogleAPIKey
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
        self.productAddressLabel.text = self.productDetailsData?.location.address

        let camera = GMSCameraPosition.camera(withLatitude: self.productDetailsData?.location.latitude ?? 0, longitude: self.productDetailsData?.location.longitude ?? 0, zoom: 11.0)
        mapView.camera = camera
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        showMarker(position: camera.target)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        self.setupDisplay()
        
        SelectProductLocationViewControllerPresenter.sharedInstance.attachView(view: self)
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        marker = GMSMarker()
        marker.position = position
        marker.icon = #imageLiteral(resourceName: "detailproduct-map-icon-pin")
        marker.map = mapView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay() {
        self.navigationItem.title = "Set Location"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
    }
   
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        self.mapView.clear()
        self.marker = GMSMarker(position: coordinate)
        self.marker.position.latitude = coordinate.latitude
        self.marker.position.longitude = coordinate.longitude
        self.marker.icon = #imageLiteral(resourceName: "detailproduct-map-icon-pin")
        self.marker.map = self.mapView
        self.productAddressLabel.text = self.getAddressForLatLng(latitude: "\(coordinate.latitude)", longitude: "\(coordinate.longitude)")
    }
    
    @IBAction func useLocationButtonAction(_ sender: Any) {
        let params : [String:Any] = ["user":[
                                             "user_id":Int(User.shared.id) ?? 0,
                                             "user_latitude":"\(self.mapView.myLocation?.coordinate.latitude ?? 0)",
                                             "user_longitude":"\(self.mapView.myLocation?.coordinate.longitude ?? 0)"],
                                     "product":[
                                             "product_id": self.productDetailsData?.id ?? "",
                                             "product_latitude": "\(self.marker.position.latitude)",
                                             "product_longitude": "\(self.marker.position.longitude)",
                                             "product_address": self.getAddressForLatLng(latitude : "\(self.marker.position.latitude)", longitude: "\(self.marker.position.longitude)"),
                                             "country_id": "\(self.productDetailsData?.location.country_id ?? "")"]
                                    ]
        SelectProductLocationViewControllerPresenter.sharedInstance.setLocation(param : params)
    }
    
    // MARK: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setLocationSuccess(data: Product) {
        data.location.latitude = self.marker.position.latitude
        data.location.longitude = self.marker.position.longitude
        self.delegate?.productLocationDidChange(data: data)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }

    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.authorizedWhenInUse)
            
        {
            self.mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    }
    
}

extension SelectProductLocationViewController{
    
    func getAddressForLatLng(latitude: String, longitude: String) -> String{
        let url = URL(string: "\(baseUrl)latlng=\(latitude),\(longitude)&key=\(apikey)")
        let data = NSData.init(contentsOf: url!)
        let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
        if let result = json["results"] as? [[String:Any]] {
            if result.count == 0 {return ""}
            if let address = result[0]["address_components"] as? [[String:Any]] {
                var number = ""
                if address.count > 0
                {
                    number = address[0]["short_name"] as? String ?? ""
                }
                var street = ""
                if address.count > 1
                {
                    street = address[1]["short_name"] as? String ?? ""
                }
                var city = ""
                if address.count > 2
                {
                     city = address[2]["short_name"] as? String ?? ""
                }
                var state = ""
                if address.count > 4
                {
                    state = address[4]["short_name"] as? String ?? ""
                }
                var zip = ""
                if address.count > 6
                {
                    zip = address[6]["short_name"] as? String ?? ""
                }
                return "\(number) \(street), \(city), \(state) \(zip)"
            }
            else{
                return ""
            }
        }
        else{
            return ""
        }
    }
}
