//
//  LocationProductsViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 30/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationProductsViewController: UIViewController, LocationChangeDelegate {

    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productNumberLabel: UILabel!
    @IBOutlet weak var productAddressLabel: VerticalAlignUILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var setLocationButton: UIButton!
    
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
        self.setupDisplay()
        self.showData()
        isFirstLoad = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isFirstLoad{
            self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
            self.showData()
        }
    }
    
    func setupDisplay() {
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.setLocationButton.titleLabel?.textAlignment = .center
        self.setLocationButton.titleLabel?.numberOfLines = 0
        self.setLocationButton.alignTextBelow(spacing: 3)
    }
    
    func showData(){
        self.productLabel.text = self.productDetailsData?.title
        self.productNumberLabel.text = "Product Number: \(self.productDetailsData?.number ?? "")"
        self.productAddressLabel.text = self.productDetailsData?.location.address
        
        let camera = GMSCameraPosition.camera(withLatitude: self.productDetailsData?.location.latitude ?? 0, longitude: self.productDetailsData?.location.longitude ?? 0, zoom: 11.0)
        mapView.camera = camera
        showMarker(position: camera.target)
    }

    func showMarker(position: CLLocationCoordinate2D){
        self.mapView.clear()
        let marker = GMSMarker()
        marker.position = position
        marker.icon = #imageLiteral(resourceName: "detailproduct-map-icon-pin")
        marker.map = mapView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectlocation" {
            let vc = segue.destination as? SelectProductLocationViewController
            vc?.productDetailsHostViewController = self.productDetailsHostViewController
            vc?.delegate = self
            
        }
    }
    
    func productLocationDidChange(data: Product) {
        self.productDetailsData = data
        self.productDetailsHostViewController?.productDetailsData = data
        self.showData()
    }
}

extension LocationProductsViewController: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return self.scrollView
    }
}
