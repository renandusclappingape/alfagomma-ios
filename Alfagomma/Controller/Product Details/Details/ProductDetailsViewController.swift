//
//  ProductDetailsViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 28/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    
    @IBOutlet weak var qrCodeLabel: UILabel!
    @IBOutlet weak var arrowLabel: UILabel!
    @IBOutlet weak var productDetailArrowLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productNumberLabel: UILabel!
    @IBOutlet weak var lengthValueLabel: UILabel!
    @IBOutlet weak var hwpPsiValueLabel: UILabel!
    @IBOutlet weak var hwpBarValueLabel: UILabel!
    @IBOutlet weak var hwpIDValueLabel: UILabel!
    @IBOutlet weak var lastServiceValueLabel: UILabel!
    @IBOutlet weak var nextServiceValueLabel: UILabel!
    @IBOutlet weak var serviceInfoLabel: UILabel!
    
    //Product Details section
    @IBOutlet weak var customerStockCodeValueLabel: UILabel!
    @IBOutlet weak var customerPOValueLabel: UILabel!
    @IBOutlet weak var customerNameValueLabel: UILabel!
    @IBOutlet weak var end2ValueLabel: UILabel!
    @IBOutlet weak var end1ValueLabel: UILabel!
    @IBOutlet weak var retention2ValueLabel: UILabel!
    @IBOutlet weak var retention1ValueLabel: UILabel!
    @IBOutlet weak var orientation2ValueLabel: UILabel!
    @IBOutlet weak var orientation1ValueLabel: UILabel!
    @IBOutlet weak var testPressureValueLabel: UILabel!
    @IBOutlet weak var maxWorkingPreasureValueLabel: UILabel!
    @IBOutlet weak var nominalBoreValueLabel: UILabel!
    @IBOutlet weak var overallLengthValueLabel: UILabel!
    @IBOutlet weak var serviceTypeValueLabel: UILabel!
    @IBOutlet weak var descriptionValueLabel: UILabel!
    @IBOutlet weak var workshopValueLabel: UILabel!
    @IBOutlet weak var assembledDateValueLabel: UILabel!
    @IBOutlet weak var stockCodeValueLabel: UILabel!
    @IBOutlet weak var salesOrderNumber: UILabel!
    @IBOutlet weak var commentsValueLabel: UILabel!
    @IBOutlet weak var operatorValueLabel: UILabel!
    @IBOutlet weak var firstScanDateValueLabel: UILabel!
    @IBOutlet weak var retestDateValueLabel: UILabel!
    @IBOutlet weak var serviceDateValueLabel: UILabel!
    @IBOutlet weak var workOrderValueLabel: UILabel!
    
    
    @IBOutlet weak var qrImageView: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var qrCodeView: UIView!
    @IBOutlet weak var productDetailsContentView: UIView!
    
    
    @IBOutlet weak var qrCodeViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var productDetailsViewHeightConstraint: NSLayoutConstraint!
    
    var isQRHidden : Bool = true
    var isDetailHidden : Bool = true
    
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    var isFirstLoad = true
    var productDetailsViewHeightConstant: CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.qrCodeViewConstraint.constant = 0
        self.bottomViewTopConstraint.constant = 0
        self.qrCodeView.isHidden = true
        self.arrowLabel.text =  "▼"
        self.qrCodeLabel.text = "Show QR Code"
        self.productDetailArrowLabel.text =  "▼"
        self.view.layoutIfNeeded()
        
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
        
        self.setupDisplay()
        self.showData()
        isFirstLoad = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isFirstLoad{
            self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
            self.showData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay() {
        if #available(iOS 11.0, *) {
            self.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func showData(){
        self.productLabel.text = self.productDetailsData?.title
        self.productNumberLabel.text = "Product Number: \(self.productDetailsData?.number ?? "")"
        
        self.lengthValueLabel.text = "\(self.productDetailsData?.length.value ?? "") \(self.productDetailsData?.length.unit ?? "")"
        self.hwpPsiValueLabel.text = self.productDetailsData?.hwp_psi
        self.hwpBarValueLabel.text = self.productDetailsData?.hwp_bar
        self.hwpIDValueLabel.text = "\(self.productDetailsData?.hwp_id.value ?? "") \(self.productDetailsData?.hwp_id.unit ?? "")"
        
        let imageURL = URL(string:"\(Constant.BaseURL)\(self.productDetailsData?.qr_image ?? "")")
        self.qrImageView.image = UIImage(named: "placeholder-image")
        qrImageView.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder-image"), options: .lowPriority) { (image, error, cacheType, url) in
            if image != nil{
                self.qrImageView.image = image
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd MMMM yyyy"
        if self.productDetailsData?.service.last_service == 0 {
            self.lastServiceValueLabel.text = "-"
        }
        else{
            self.lastServiceValueLabel.text = dateFormatter.string(from: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.last_service ?? 0))
        }
        
        if self.productDetailsData?.service.next_service == 0 {
            self.nextServiceValueLabel.text = "-"
        }
        else{
            self.nextServiceValueLabel.text = dateFormatter.string(from: convertUnixTimeStamp(timestamp: self.productDetailsData?.service.next_service ?? 0))
        }
        
        self.serviceInfoLabel.text = self.productDetailsData?.service.service_info
        
        //setup constraint
        self.productDetailsViewHeightConstraint.isActive = false
        self.view.layoutIfNeeded()
        
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        //set product detail value
        
        self.workOrderValueLabel.text =  self.productDetailsData?.work_order_number
        if self.workOrderValueLabel.text == "" {
            self.workOrderValueLabel.text = "-"
        }
        
        if self.productDetailsData?.service_date == "" {
            self.serviceDateValueLabel.text = "-"
        }
        else{
            let serviceDateValueDate = dateFormat.date(from: self.productDetailsData?.service_date ?? "")
            if serviceDateValueDate != nil {
                self.serviceDateValueLabel.text = dateFormatter.string(from: serviceDateValueDate!)
            }
            else{
                self.serviceDateValueLabel.text = "-"
            }
        }
        
        if self.productDetailsData?.retest_date == "" {
            self.retestDateValueLabel.text = "-"
        }
        else{
            let retestDateValueDate = dateFormat.date(from: self.productDetailsData?.retest_date ?? "")
            if retestDateValueDate != nil {
                self.retestDateValueLabel.text = dateFormatter.string(from: retestDateValueDate!)
            }
            else{
                self.retestDateValueLabel.text = "-"
            }
        }
        
        if self.productDetailsData?.first_scan_date == "" {
            self.firstScanDateValueLabel.text = "-"
        }
        else{
            let firstScanDateValueDate = dateFormat.date(from: self.productDetailsData?.first_scan_date ?? "")
            if firstScanDateValueDate != nil {
                self.firstScanDateValueLabel.text = dateFormatter.string(from: firstScanDateValueDate!)
            }
            else{
                self.firstScanDateValueLabel.text = "-"
            }
        }
        
        self.operatorValueLabel.text = self.productDetailsData?.product_operator
        if self.operatorValueLabel.text == "" {
            self.operatorValueLabel.text = "-"
        }
        
        self.commentsValueLabel.text = self.productDetailsData?.comments
        if self.commentsValueLabel.text == "" {
            self.commentsValueLabel.text = "-"
        }
        
        self.salesOrderNumber.text = self.productDetailsData?.sales_order_number
        if self.salesOrderNumber.text == "" {
            self.salesOrderNumber.text = "-"
        }
        
        self.stockCodeValueLabel.text = self.productDetailsData?.stock_code
        if self.stockCodeValueLabel.text == "" {
            self.stockCodeValueLabel.text = "-"
        }
        
        self.assembledDateValueLabel.text = self.productDetailsData?.assembled_date
        if self.assembledDateValueLabel.text == "" {
            self.assembledDateValueLabel.text = "-"
        }
        
        self.workshopValueLabel.text = self.productDetailsData?.workshop
        if self.workshopValueLabel.text == "" {
            self.workshopValueLabel.text = "-"
        }
        
        self.descriptionValueLabel.text = self.productDetailsData?.description
        if self.descriptionValueLabel.text == "" {
            self.descriptionValueLabel.text = "-"
        }
        
        self.serviceTypeValueLabel.text = self.productDetailsData?.service_type
        if self.serviceTypeValueLabel.text == "" {
            self.serviceTypeValueLabel.text = "-"
        }
        
        self.overallLengthValueLabel.text = self.productDetailsData?.overall_length_mm
        if self.overallLengthValueLabel.text == "" {
            self.overallLengthValueLabel.text = "-"
        }
        
        self.nominalBoreValueLabel.text = self.productDetailsData?.nominal_bore
        if self.nominalBoreValueLabel.text == "" {
            self.nominalBoreValueLabel.text = "-"
        }
        
        self.maxWorkingPreasureValueLabel.text = self.productDetailsData?.max_working_pressure
        if self.maxWorkingPreasureValueLabel.text == "" {
            self.maxWorkingPreasureValueLabel.text = "-"
        }
        
        self.testPressureValueLabel.text = self.productDetailsData?.test_pressure
        if self.testPressureValueLabel.text == "" {
            self.testPressureValueLabel.text = "-"
        }
        
        self.orientation1ValueLabel.text = self.productDetailsData?.orientation_1
        if self.orientation1ValueLabel.text == "" {
            self.orientation1ValueLabel.text = "-"
        }
        
        self.orientation2ValueLabel.text = self.productDetailsData?.orientation_2
        if self.orientation2ValueLabel.text == "" {
            self.orientation2ValueLabel.text = "-"
        }
        
        self.retention1ValueLabel.text = self.productDetailsData?.retention_1
        if self.retention1ValueLabel.text == "" {
            self.retention1ValueLabel.text = "-"
        }
        
        self.retention2ValueLabel.text = self.productDetailsData?.retention_2
        if self.retention2ValueLabel.text == "" {
            self.retention2ValueLabel.text = "-"
        }
        
        self.end1ValueLabel.text = self.productDetailsData?.end_1
        if self.end1ValueLabel.text == "" {
            self.end1ValueLabel.text = "-"
        }
        
        self.end2ValueLabel.text = self.productDetailsData?.end_2
        if self.end2ValueLabel.text == "" {
            self.end2ValueLabel.text = "-"
        }
        
        self.customerNameValueLabel.text = self.productDetailsData?.customer_name
        if self.customerNameValueLabel.text == "" {
            self.customerNameValueLabel.text = "-"
        }
        
        self.customerPOValueLabel.text = self.productDetailsData?.customer_PO
        if self.customerPOValueLabel.text == "" {
            self.customerPOValueLabel.text = "-"
        }
        
        self.customerStockCodeValueLabel.text = self.productDetailsData?.customers_stock_code
        if self.customerStockCodeValueLabel.text == "" {
            self.customerStockCodeValueLabel.text = "-"
        }
        
        //end of set product detail value
        
        self.view.layoutIfNeeded()
        self.productDetailsViewHeightConstant = self.productDetailsContentView.frame.size.height
        self.productDetailsViewHeightConstraint?.isActive = true
        self.view.layoutIfNeeded()
        self.productDetailsViewHeightConstraint?.constant = 0
        self.view.layoutIfNeeded()
        //end of setup constraint
    }
    
    @IBAction func showQRCodeButtonAction(_ sender: Any) {
        if isQRHidden {
            self.showQR()
        }
        else{
            self.hideQR()
        }
        
    }
    
    func showQR(){
        UIView.animate(withDuration: 0.2) {
            self.qrCodeView.isHidden = false
            self.isQRHidden = false
            self.qrCodeViewConstraint.constant = self.view.frame.width
            self.bottomViewTopConstraint.constant = 16
            self.arrowLabel.text = "▲"
            self.qrCodeLabel.text = "Hide QR Code"
            self.view.layoutIfNeeded()
        }
    }
    
    func hideQR(){
        UIView.animate(withDuration: 0.2) {
            self.qrCodeView.isHidden = true
            self.isQRHidden = true
            self.qrCodeViewConstraint.constant = 0
            self.bottomViewTopConstraint.constant = 0
            self.arrowLabel.text =  "▼"
            self.qrCodeLabel.text = "Show QR Code"
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func showProductDetailsButtonAction(_ sender: Any) {
        if isDetailHidden {
            self.showDetails()
        }
        else{
            self.hideDetails()
        }
        
    }
    
    func showDetails(){
        UIView.animate(withDuration: 0.2) {
            self.isDetailHidden = false
            self.productDetailsViewHeightConstraint.constant = self.productDetailsViewHeightConstant
            self.productDetailArrowLabel.text = "▲"
            self.view.layoutIfNeeded()
        }
    }
    
    func hideDetails(){
        UIView.animate(withDuration: 0.2) {
            self.isDetailHidden = true
            self.productDetailsViewHeightConstraint.constant = 0
            self.productDetailArrowLabel.text =  "▼"
            self.view.layoutIfNeeded()
        }
    }
    

}

extension ProductDetailsViewController: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        
        return self.scrollView
    }
}
