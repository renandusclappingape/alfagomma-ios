//
//  NoteAttachmentCollectionViewCell.swift
//  Alfagomma
//
//  Created by Clapping Ape on 31/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class NoteAttachmentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var noteImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.noteImageView.layer.cornerRadius = self.noteImageView.frame.size.height/4
    }
}
