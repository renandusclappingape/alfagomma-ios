//
//  NoteAttachmentViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 31/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class NoteAttachmentViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, AddNoteView {
    
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var noteTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewUploadPhoto: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableViewNote: UITableView!
    
    var picker : UIImagePickerController? = UIImagePickerController()
    let cellSpacing : CGFloat = 16
    var noteList : [Product.notes_object]? = []
    
    var imageList : [UIImage] = [#imageLiteral(resourceName: "icon-addphoto")]
    var productDetailsData : Product?
    var productDetailsHostViewController : ProductDetailsHostViewController?
    var isFirstLoad = true
    
    var tap : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NoteAttachmentViewControllerPresenter.sharedInstance.attachView(view: self)
        
        self.productDetailsData = self.productDetailsHostViewController?.productDetailsData

        picker?.delegate = self
        self.noteTextView.delegate = self
        self.setupDisplay()
        self.showData()
        isFirstLoad = false
        self.registerNotification()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isFirstLoad{
            self.productDetailsData = self.productDetailsHostViewController?.productDetailsData
            self.showData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay() {
        if #available(iOS 11.0, *) {
            self.tableViewNote.contentInsetAdjustmentBehavior = .never
            self.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.scrollView.isHidden = true
        
        self.tableViewNote.contentInset = UIEdgeInsetsMake(0, 0, 86, 0)
        
        self.noteTextView.layer.borderWidth = 1
        self.noteTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.noteTextView.textContainerInset = UIEdgeInsetsMake(16.0, 16.0, 16.0, 16.0);
    }
    
    func showData(){
        self.noteList = self.productDetailsData?.notes
        self.tableViewNote.reloadData()
    }
    
    @IBAction func createNewNoteButtonAction(_ sender: Any) {
        self.scrollView.isHidden = false
    }
    
    @IBAction func submitNoteButtonAction(_ sender: Any) {
        let userLat = "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)"
        let userLong = "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)"
        var encodeImage : [String] = []
        for image in self.imageList{
            let imageData:NSData = UIImageJPEGRepresentation(image, 0.5)! as NSData
            let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
            encodeImage.append("data:image/jpeg;base64,\(strBase64)")
        }
        
        let params : [String : Any] = ["user":["user_id":Int(User.shared.id) ?? 0,
                              "user_latitude":userLat,
                              "user_longitude":userLong],
                      "notes":["photos": encodeImage ,
                               "product_id":self.productDetailsData?.id ?? "",
                               "description": self.noteTextView.text]]
        NoteAttachmentViewControllerPresenter.sharedInstance.setNote(param : params)
    }
    
    @IBAction func cancelNoteButtonAction(_ sender: Any) {
        self.imageList = [#imageLiteral(resourceName: "icon-addphoto")]
        self.collectionViewUploadPhoto.reloadData()
        self.noteTextView.text = ""
        self.scrollView.isHidden = true
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        let sizeToFitIn = CGSize(width : self.noteTextView.bounds.size.width,height: CGFloat(MAXFLOAT))
        let newSize = self.noteTextView.sizeThatFits(sizeToFitIn)
        self.noteTextViewHeightConstraint.constant = newSize.height
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.noteList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteAttachmentTableViewCell") as! NoteAttachmentTableViewCell
        
        cell.noteLabel.text = self.noteList?[indexPath.row].description ?? ""
        cell.imageList = self.noteList?[indexPath.row].photos ?? []
       
        return cell
    }
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }

    // MARK: Presnter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setAddNoteSuccess(data: Product) {
        self.imageList = [#imageLiteral(resourceName: "icon-addphoto")]
        self.collectionViewUploadPhoto.reloadData()
        self.noteTextView.text = ""
        self.scrollView.isHidden = true
        self.productDetailsData = data
        self.productDetailsHostViewController?.productDetailsData = data
        self.showData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
}

extension NoteAttachmentViewController: SJSegmentedViewControllerViewSource {
    
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        if self.scrollView.isHidden {
            return self.tableViewNote
        }
        else{
            return self.scrollView
        }
        
    }
}

extension NoteAttachmentViewController : UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return imageList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadPhotoCollectionViewCell",
                                                            for: indexPath) as? UploadPhotoCollectionViewCell else {
                                                                preconditionFailure("Unregistered table view cell")
        }
        
        cell.uploadImageView.image = self.imageList[indexPath.row]
        
        if cell.uploadImageView.image != #imageLiteral(resourceName: "icon-addphoto") {
            cell.deleteButton.isHidden = false
            cell.deleteButton.tag = indexPath.row+3333
            cell.uploadImageView.contentMode = .scaleAspectFill
            cell.deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
        }
        else{
            cell.deleteButton.isHidden = true
            cell.deleteButton.tag = indexPath.row+3333
            cell.uploadImageView.contentMode = .center
        }
        
        return cell
    }
    
    @objc func deleteImage(sender: UIButton!) {
        let index = sender.tag-3333
        self.imageList.remove(at: index)
        self.collectionViewUploadPhoto.reloadData()
        self.collectionViewHeightConstraint.constant = self.collectionViewUploadPhoto.collectionViewLayout.collectionViewContentSize.height
        self.view.layoutIfNeeded()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == self.imageList.count-1 {
            let alert:UIAlertController = UIAlertController(title: "Profile Picture Options", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.openCamera()
            }
            
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
                UIAlertAction in self.openGallery()
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in self.cancel()
            }
            
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker!, animated: true, completion: nil)
        }
        else
        {
            let alertController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker!.allowsEditing = false
        present(picker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageList.insert(pickedImage, at: 0)
            self.collectionViewUploadPhoto.reloadData()
            self.collectionViewHeightConstraint.constant = self.collectionViewUploadPhoto.collectionViewLayout.collectionViewContentSize.height
            self.view.layoutIfNeeded()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func cancel(){
        
    }
}

extension NoteAttachmentViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let itemSize = (collectionView.frame.size.width-(cellSpacing*2)-32)/3
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
}

extension NoteAttachmentViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}
