//
//  UploadPhotoCollectionViewCell.swift
//  Alfagomma
//
//  Created by Clapping Ape on 11/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class UploadPhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.uploadImageView.layer.cornerRadius = self.uploadImageView.frame.size.height/4
        self.deleteButton.isHidden = true
    }
}
