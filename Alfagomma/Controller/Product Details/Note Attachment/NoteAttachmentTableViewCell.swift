//
//  NoteAttachmentTableViewCell.swift
//  Alfagomma
//
//  Created by Clapping Ape on 31/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class NoteAttachmentTableViewCell: UITableViewCell {

    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    let cellSpacing : CGFloat = 16
    
    var imageList : [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionViewAttachment.delegate = self
        self.collectionViewAttachment.dataSource = self
        
        let itemSize = (self.collectionViewAttachment.frame.size.width-(cellSpacing*2)-32)/3
        self.collectionViewHeightConstraint.constant = itemSize
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension NoteAttachmentTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return imageList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoteAttachmentCollectionViewCell",
                                                            for: indexPath) as? NoteAttachmentCollectionViewCell else {
                                                                preconditionFailure("Unregistered table view cell")
        }
        
        cell.noteImageView.image = UIImage(named: "placeholder-image")
        let imageURL = URL(string:"\(Constant.BaseURL)\(self.imageList[indexPath.row])")
        cell.noteImageView.sd_setImage(with: imageURL, completed: {
            (image, error, cacheType, url) in
            if image != nil{
                cell.noteImageView.image = image
            }
        })
        
        
        return cell
    }
}

extension NoteAttachmentTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let itemSize = (collectionView.frame.size.width-(cellSpacing*2)-32)/3
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
}
