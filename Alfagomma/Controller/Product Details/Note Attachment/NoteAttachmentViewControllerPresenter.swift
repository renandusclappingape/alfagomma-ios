//
//  NoteAttachmentViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 29/06/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol AddNoteView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setAddNoteSuccess(data : Product)
    func setErrorMessageFromAPI(errorMessage: String)
}

class NoteAttachmentViewControllerPresenter {
    
    weak fileprivate var addNoteView : AddNoteView?
    
    private init() {}
    static let sharedInstance = NoteAttachmentViewControllerPresenter()
    
    func attachView(view: AddNoteView) {
        self.addNoteView = view
    }
    
    func detachView() {
        self.addNoteView = nil
    }
    
    
    func setNote(param: [String:Any]){
        self.addNoteView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.SetProductNoteAPI(parameters: param,callBack: { [weak self](data) in
            self?.addNoteView?.finishLoading()
            self?.addNoteView?.setAddNoteSuccess(data: data)
        }) { (message: String) in
            self.addNoteView?.finishLoading()
            self.addNoteView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
