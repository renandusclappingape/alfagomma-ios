//
//  PProductDetailsHostViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 25/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class ProductDetailsHostViewController: SJSegmentedViewController {

    var selectedSegment: SJSegmentTab?
    var productDetailsData : Product?
    var isFromSearch : Bool = false
    
    private var productDetailsHostViewController: ProductDetailsHostViewController?
    
    override func viewDidLoad() {
        
        self.productDetailsHostViewController = self
        
        var storyboard = UIStoryboard(name: "ProductDetailsHeader", bundle: nil)
        let headerController = storyboard
            .instantiateViewController(withIdentifier: "ProductDetailsHeaderViewController") as! ProductDetailsHeaderViewController
        headerController.productDetailsData = self.productDetailsData
        headerController.isFromSearch = self.isFromSearch
        
        storyboard = UIStoryboard(name: "ProductDetails", bundle: nil)
        let firstViewController = storyboard
            .instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        firstViewController.title = "Information//detailproduct-tab-icon-info-inactive//detailproduct-tab-icon-info-active"
        firstViewController.productDetailsHostViewController = self.productDetailsHostViewController
        
        storyboard = UIStoryboard(name: "LocationProducts", bundle: nil)
        let secondViewController = storyboard
            .instantiateViewController(withIdentifier: "LocationProductsViewController") as! LocationProductsViewController
        secondViewController.title = "Location//detailproduct-tab-icon-location-inactive//detailproduct-tab-icon-location-active"
        secondViewController.productDetailsHostViewController = self.productDetailsHostViewController
        
        storyboard = UIStoryboard(name: "ServiceSchedule", bundle: nil)
        let thirdViewController = storyboard
            .instantiateViewController(withIdentifier: "ServiceScheduleViewController") as! ServiceScheduleViewController
        thirdViewController.title = "Schedule//detailproduct-tab-icon-service-inactive//detailproduct-tab-icon-service-active"
        thirdViewController.productDetailsHostViewController = self.productDetailsHostViewController
        
        storyboard = UIStoryboard(name: "NoteAttachment", bundle: nil)
        let forthViewController = storyboard
            .instantiateViewController(withIdentifier: "NoteAttachmentViewController") as! NoteAttachmentViewController
        forthViewController.title = "Note//detailproduct-tab-icon-note-inactive//detailproduct-tab-icon-note-active"
        forthViewController.productDetailsData = self.productDetailsData
        forthViewController.productDetailsHostViewController = self.productDetailsHostViewController
        
        
        headerViewController = headerController
        segmentControllers = [firstViewController,secondViewController,thirdViewController,forthViewController]
        headerViewHeight = 326
        segmentViewHeight = 88
        selectedSegmentViewHeight = 2.0
        headerViewOffsetHeight = 0.0
        segmentTitleColor = .gray
        selectedSegmentViewColor = .red
        segmentShadow = SJShadow.light()
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        segmentBounces = false
        delegate = self
        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = self.backButton()
        
        self.setupDisplay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "Detail Product"
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Detail Product"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.extendedLayoutIncludesOpaqueBars = true
        
        if #available(iOS 11.0, *) {
            self.segmentedScrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    func backButton() -> UIBarButtonItem {
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "icon-back"), style: .plain, target: self, action: #selector(dismissVC))
        
        return leftBarButtonItem
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true) {
            
        }
    }
}

extension ProductDetailsHostViewController: SJSegmentedViewControllerDelegate, SJSegmentedViewControllerViewSource {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        
        
        if selectedSegment != nil {
            for i in 0..<self.segmentControllers.count{
                let tabPropertiesArray : [String] = (self.segmentControllers[i].title?.components(separatedBy: "//"))!
                segments[i].titleColor(.lightGray)
                segments[i].image(UIImage(named: tabPropertiesArray[1] ) ?? UIImage())
            }
        }
        
        if segments.count > 0 {
            let tabPropertiesArray : [String] = (controller.title?.components(separatedBy: "//"))!
            selectedSegment = segments[index]
            selectedSegment?.titleColor(.red)
            selectedSegment?.image(UIImage(named: tabPropertiesArray[2] ) ?? UIImage())
            
        }
    }
    
}
