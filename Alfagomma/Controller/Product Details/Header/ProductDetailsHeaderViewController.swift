//
//  ProductDetailsHeaderViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 28/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import FSPagerView

class ProductDetailsHeaderViewController: UIViewController {
    
    //UITextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UIView
    @IBOutlet weak var searchTextFieldView: UIView!
    @IBOutlet weak var bannerView: UIView!
    
    var productDetailsData : Product?
    var isFromSearch : Bool = false
    var pagerView : FSPagerView = FSPagerView()
    var pageControl : FSPageControl = FSPageControl()
    var tap : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initBanner()
        self.setupDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchTextField.text = SearchTextField.shared.text
        SearchTextField.shared.setActiveViewController(viewController: self)
    }
    
    func setupDisplay(){
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageViewSearchBox = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageViewSearchBox.image = image;
        viewField.addSubview(imageViewSearchBox)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        if self.isFromSearch{
            self.searchTextField.delegate = self
        }
        else{
            self.searchTextField.delegate = SearchTextField.shared
        }
        
        self.searchTextField.text = SearchTextField.shared.text
    }

    func initBanner(){
        // Create a pager view
        self.view.layoutIfNeeded()
        let bannerFreme = CGRect(x: 0, y: 0, width: self.bannerView.frame.size.width, height: self.bannerView.frame.size.height)
        self.pagerView = FSPagerView(frame: bannerFreme)
        self.pagerView.dataSource = self
        self.pagerView.delegate = self
        self.pagerView.isInfinite = true
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.automaticSlidingInterval = 5.0
        self.bannerView.addSubview(self.pagerView)
        // Create a page control
        let pageControlFrame = CGRect(x: 0, y: bannerFreme.height-48, width: bannerFreme.width, height: 24)
        self.pageControl = FSPageControl(frame: pageControlFrame)
        self.pageControl.numberOfPages = self.productDetailsData?.product_images?.count ?? 0
        self.pageControl.contentHorizontalAlignment = .center
        self.pageControl.currentPage = 0
        self.pageControl.setFillColor(UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1), for: .normal)
        self.pageControl.setFillColor(UIColor(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 1), for: .selected)
        self.bannerView.addSubview(self.pageControl)
    }
}

extension ProductDetailsHeaderViewController: FSPagerViewDataSource, FSPagerViewDelegate{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.productDetailsData?.product_images!.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        let thumbImageURL = URL(string:"\(Constant.BaseURL)\(self.productDetailsData?.product_images![index].image_thumb ?? "")")
        let imageURL = URL(string:"\(Constant.BaseURL)\(self.productDetailsData?.product_images![index].image ?? "")")
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.image = UIImage(named: "placeholder-image")
            cell.imageView?.sd_setImage(with: thumbImageURL, completed: {
                (image, error, cacheType, url) in
                if image != nil{
                    cell.imageView?.image = image
                }
                cell.imageView?.sd_setImage(with: imageURL, completed: {
                    (image, error, cacheType, url) in
                    if image != nil{
                        cell.imageView?.image = image
                    }
                })
            })
        return cell
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex
    }
}

extension ProductDetailsHeaderViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}

extension ProductDetailsHeaderViewController: UITextFieldDelegate{
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: Notification.Name("load_search"), object: nil)

        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        SearchTextField.shared.text = updatedString
        
        return true
    }
}

