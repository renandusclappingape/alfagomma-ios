//
//  ContactUsViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 14/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

protocol ContactUsView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func contactSuccess()
    func setErrorMessageFromAPI(errorMessage: String)
}

class ContactUsViewControllerPresenter {
    
    weak fileprivate var contactUsView : ContactUsView?
    
    private init() {}
    static let sharedInstance = ContactUsViewControllerPresenter()
    
    func attachView(view: ContactUsView) {
        self.contactUsView = view
    }
    
    func detachView() {
        self.contactUsView = nil
    }
    
    func sendMessage(param: [String:Any]) {
        self.contactUsView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.ContactAPI(params: param,callBack: { () in
            self.contactUsView?.finishLoading()
            self.contactUsView?.contactSuccess()
        }) { (message: String) in
            self.contactUsView?.finishLoading()
            self.contactUsView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
}
