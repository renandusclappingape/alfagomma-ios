//
//  LocationSelectViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct LocationData{
    var location_list : [Location.location] = []
    
}

protocol LocationView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setLocationData (data: LocationData)
    func setLocationSuccess()
    func setErrorMessageFromAPI(errorMessage: String)
}

class LocationViewControllerPresenter {
    
    weak fileprivate var locationView : LocationView?
    
    private init() {}
    static let sharedInstance = LocationViewControllerPresenter()
    
    func attachView(view: LocationView) {
        self.locationView = view
    }
    
    func detachView() {
        self.locationView = nil
    }
    
    func load() {
        self.locationView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.LocationRequestAPI(callBack: { [weak self](data) in
            self?.locationView?.finishLoading()
            self?.locationView?.setLocationData(data: LocationData(location_list: data.location_list))
        }) { (message: String) in
            self.locationView?.finishLoading()
            self.locationView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
    func setLocation(locationID: String){
        self.locationView?.startLoading(text : "Loading")
        
        let param : [String : Any] = ["id":Int(User.shared.id) ?? 0,"country_id":locationID] 
        
        APIService.sharedInstance.updateProfileAPI(params: param,callBack: { () in
            self.locationView?.finishLoading()
            self.locationView?.setLocationSuccess()
        }) { (message: String) in
            self.locationView?.finishLoading()
            self.locationView?.setErrorMessageFromAPI(errorMessage: message)
        }
    }
}
