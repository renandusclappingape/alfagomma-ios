//
//  LocationSelectViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 22/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class LocationSelectViewController: BaseViewController, LocationView, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    
    //UITableView
    @IBOutlet weak var tableViewLocation: UITableView!
    
    //UItextField
    @IBOutlet weak var searchTextField: UITextField!
    
    //UIButton
    @IBOutlet weak var setLocationButton: UIButton!
    
    //Other
    var tap : UITapGestureRecognizer!
    var locationToShow : LocationData?
    
    var selectedLocation : Location.location?
    var completion : ((_ selectedLocation : Location.location) -> Void)? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationViewControllerPresenter.sharedInstance.attachView(view: self)
        LocationViewControllerPresenter.sharedInstance.load()
        
        self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.searchTextField.delegate = self
        
        self.setupDisplay()
        self.registerNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupDisplay(){
        let backImage = UIImage(named: "icon-back")
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationItem.title = "Choose Your Location"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        let viewField = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 20))
        let imageView = UIImageView(frame: CGRect(x: 6, y: 0, width: 20, height: 20))
        let image = UIImage(named: "icon-search-red");
        imageView.image = image;
        viewField.addSubview(imageView)
        self.searchTextField.leftView = viewField;
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.leftViewMode = .always
        
        self.setLocationButton.layer.cornerRadius = 4
        self.setLocationButton.layer.masksToBounds = true
    }
    
    
    @IBAction func setLocationButtonAction(_ sender: Any) {
        if self.completion != nil {
            self.completion!(self.selectedLocation!)
        }
        else{
            if self.selectedLocation == nil{
                return
            }
            LocationViewControllerPresenter.sharedInstance.setLocation(locationID: (self.selectedLocation?.id)!)
        }
    }
    
    // Mark: Presenter Delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setLocationData(data: LocationData) {
        self.locationToShow = data
        self.tableViewLocation.reloadData()
    }
    
    func setLocationSuccess() {
        let storyboard = UIStoryboard(name: "Host", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "RootViewController")
        self.present(dashboardVC, animated: true) {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    
    // UItableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locationToShow?.location_list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationSelectTableViewCell") as! LocationSelectTableViewCell
        cell.countryNameLabel.text = self.locationToShow?.location_list[indexPath.row].title
        
        if self.locationToShow?.location_list[indexPath.row].id == self.selectedLocation?.id {
            cell.accessoryType = .checkmark
        }
        else{
            cell.accessoryType = .none
        }
        
        cell.selectionStyle = .none
        cell.tintColor = Color.redErrorColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            self.selectedLocation = self.locationToShow?.location_list[indexPath.row]
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            cell.accessoryType = .none
        }
    }
    
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    // Mark: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.searchTextField {
            self.view.endEditing(true)
        }
        
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        var arr = Location.shared.location_list.filter {
            $0.title?.range(of: self.searchTextField.text ?? "", options: .caseInsensitive) != nil
        }
        
        if self.searchTextField.text == "" {
            arr = Location.shared.location_list
        }
        self.locationToShow?.location_list = arr
        self.tableViewLocation.reloadData()
    }
}
