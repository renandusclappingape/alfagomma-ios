//
//  SearchControlHostViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 24/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

protocol SearchControlHostDelegate: NSObjectProtocol {
    func completion(data:[String:Any])
}

class SearchControlHostViewController: UIViewController, SortViewControllerDelegate, FilterViewControllerDelegate {
    
    var searchControlHostDelegate : SearchControlHostDelegate?
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    
    @IBOutlet weak var dismissPopupButton: UIButton!
    
    var showFilter : Bool = false
    var showSort : Bool = false
    
    var isDropDownOpened = false
    @IBOutlet weak var dropDownTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropDownHeightConstraint: NSLayoutConstraint!
    
    var defaultHeightConstraint : CGFloat = 0.0
    
    var titleView = UILabel()
    
    var sortViewController: SortViewController?
    var filterViewController: FilterViewController?
    
    var param : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if showSort {
            self.sortView.isHidden = false
            self.filterView.isHidden = true
        }
        if showFilter {
            self.sortView.isHidden = true
            self.filterView.isHidden = false
        }
        
        self.defaultHeightConstraint = self.dropDownHeightConstraint.constant
        self.navigationItem.leftBarButtonItem = self.backButton()
        
        self.setupDisplay()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if showSort {
            self.setNavTitle(title: "Sort ▼", color: .black)
        }
        if showFilter {
            self.setNavTitle(title: "Filter ▼", color: .black)
        }
        
    }
    
    @objc private func titleWasTapped() {
        
        if self.isDropDownOpened{
            self.dismissDropDown()
        }
        else{
            self.showDropDown()
        }
    }
    
    func dismissDropDown(){
        self.dismissPopupButton.isHidden = false
        let animation = CATransition()
        self.navigationController?.navigationBar.layer.add(animation, forKey: nil)
        UIView.animate(withDuration: 0.2, animations: {
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            
            self.navigationItem.leftBarButtonItem = self.backButton()
            
            if self.showSort {
                self.setNavTitle(title: "Sort ▼",color: .black)
            }
            if self.showFilter {
                self.setNavTitle(title: "Filter ▼",color: .black)
            }
            
            self.dropDownHeightConstraint.constant = 0
            
            self.dismissPopupButton.alpha = 0
            
            self.view.layoutIfNeeded()
        }, completion: { (done: Bool) in
             self.dismissPopupButton.isHidden = true
            self.dropDownView.alpha = 0
        })
        UIView.animate(withDuration: 0.2) {
            
            
        }
        self.isDropDownOpened = false
    }
    
    func showDropDown(){
        self.dismissPopupButton.isHidden = false
        self.dropDownView.alpha = 1
        let animation = CATransition()
        self.navigationController?.navigationBar.layer.add(animation, forKey: nil)
        UIView.animate(withDuration: 0.2, animations: {
            self.navigationController?.navigationBar.shadowImage = UIColor.white.as1ptImage()
            self.navigationController?.navigationBar.setBackgroundImage(UIColor(red: 236.0/255.0, green: 29.0/255.0, blue: 36.0/255.0, alpha: 1).as1ptImage(), for: .default)
            
            self.navigationItem.leftBarButtonItem = nil
            
            self.setNavTitle(title: "Close ▲",color: .white)
            self.dropDownHeightConstraint.constant = self.defaultHeightConstraint
            
            self.dismissPopupButton.alpha = 0.8
            
            self.view.layoutIfNeeded()
        }, completion: { (done: Bool) in
            
            
        })
        
        self.isDropDownOpened = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    func setNavTitle(title: String, color : UIColor){
        titleView.text = title
        titleView.textColor = color
        titleView.font = UIFont(name: FontFamilyName.ClanOTMedium, size: 14)
        let width = titleView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)).width
        titleView.frame = CGRect(origin:CGPoint.zero, size:CGSize(width: width, height: 500))
        self.navigationItem.titleView = titleView
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(titleWasTapped))
        titleView.isUserInteractionEnabled = true
        titleView.addGestureRecognizer(recognizer)
    }
    
    func backButton() -> UIBarButtonItem {
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "icon-back"), style: .plain, target: self, action: #selector(dismissVC))
        
        return leftBarButtonItem
    }
    
    func setupDisplay(){
        self.navigationController?.navigationBar.tintColor = UIColor.red
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        
        self.dropDownHeightConstraint.constant = 0
        
        self.dropDownView.alpha = 0
        
        self.dismissPopupButton.alpha = 0
        self.dismissPopupButton.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sort" {
            self.sortViewController = segue.destination as? SortViewController
            self.sortViewController?.sortViewControllerDelegate = self
            self.sortViewController?.param = self.param
            
        }
        if segue.identifier == "filter" {
            self.filterViewController = segue.destination as? FilterViewController
            self.filterViewController?.filterViewControllerDelegate = self
            self.filterViewController?.param = self.param
        }
    }
    
    @objc func dismissVC(){
        self.dismiss(animated: true) {
            
        }
    }

    @IBAction func dismissPopupButtonAction(_ sender: Any) {
        self.dismissDropDown()
    }
    
    @IBAction func filterButtonAction(_ sender: Any) {
        self.sortView.isHidden = true
        self.filterView.isHidden = false
        self.dismissDropDown()
        self.setNavTitle(title: "Filter ▼", color: .black)
    }
    
    @IBAction func sortButtonAction(_ sender: Any) {
        self.sortView.isHidden = false
        self.filterView.isHidden = true
        self.dismissDropDown()
        self.setNavTitle(title: "Sort ▼", color: .black)
    }
    
    func completion(data:[String:Any]){
        self.searchControlHostDelegate?.completion(data: data)
        self.dismissVC()
    }
}
