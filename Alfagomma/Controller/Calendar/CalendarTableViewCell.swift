//
//  CalendarTableViewCell.swift
//  Alfagomma
//
//  Created by Clapping Ape on 04/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit

class CalendarTableViewCell: UITableViewCell {

    @IBOutlet weak var requestImmidiateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var productCodeLabel: UILabel!
    @IBOutlet weak var productNumberLabel: UILabel!
    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var noteLabel: VerticalAlignUILabel!
    @IBOutlet weak var backgroundCellView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowView.layer.shadowOffset = CGSize(width:0, height:0);
        shadowView.layer.shadowColor = UIColor.black.cgColor;
        shadowView.layer.cornerRadius = 5.0;
        shadowView.layer.shadowRadius = 2.0;
        shadowView.layer.shadowOpacity = 0.2;
        
        backgroundCellView.layer.cornerRadius = 4
        backgroundCellView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setImmediateStatus(status : Bool){
        if status{
            requestImmidiateLabel.text = "Request Immediate Service"
            requestImmidiateLabel.textColor = Color.redErrorColor
            self.noteLabel.text = ""
            self.noteTitleLabel.text = ""
        }
        else{
            requestImmidiateLabel.text = "Next Service"
            requestImmidiateLabel.textColor = UIColor.init(red: 124.0/255.0, green:  138.0/255.0, blue: 157.0/255.0, alpha: 1)
            self.noteTitleLabel.text = "Note :"
        }
        
    }
}
