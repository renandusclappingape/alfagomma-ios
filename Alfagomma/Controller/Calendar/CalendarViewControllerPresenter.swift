//
//  CalendarViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 04/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct CalendarData{
    var my_product_list : [CalendarProduct.product] = []
    
}

protocol CalendarView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setProductData (data: CalendarData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class CalendarViewControllerPresenter {
    
    weak fileprivate var calendarView : CalendarView?
    
    private init() {}
    static let sharedInstance = CalendarViewControllerPresenter()
    
    func attachView(view: CalendarView) {
        self.calendarView = view
    }
    
    func detachView() {
        self.calendarView = nil
    }
    
    func load(parameter: [String:Any]? = nil) {
        self.calendarView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.CalendarRequestAPI(parameters: parameter,callBack: { [weak self](data) in
            self?.calendarView?.finishLoading()
            self?.calendarView?.setProductData(data: CalendarData(my_product_list: data.product_list))
        }) { (message: String) in
            self.calendarView?.finishLoading()
            self.calendarView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
}
