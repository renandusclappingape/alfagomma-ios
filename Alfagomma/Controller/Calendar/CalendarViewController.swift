//
//  CalendarViewController.swift
//  Alfagomma
//
//  Created by Clapping Ape on 04/07/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarViewController: BaseViewController, CalendarView {
    
    @IBOutlet var calendarView: JTAppleCalendarView!
    
    //UITextField
    @IBOutlet weak var yearTextField: UITextField!
    
    //UILabel
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var prevMonthLabel: UILabel!
    @IBOutlet weak var nextMonthLabel: UILabel!
    
    //UIButton
    @IBOutlet weak var nextServiceButton: UIButton!
    @IBOutlet weak var lastServiceButton: UIButton!
    
    //UITableView
    @IBOutlet weak var tableViewProduct: UITableView!
    
    //ScrollView
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Constraint
    @IBOutlet weak var tableViewConstraint: NSLayoutConstraint!
    
    var picker: UIPickerView!
    
    var tap : UITapGestureRecognizer!
    var iii: Date?
    let formatter = DateFormatter()
    var calendarData : CalendarData?
    var activeDate : [Date] = []
    var yearList : [Int] = []
    var selectedYear : Int?
    var visibleDateOnCalendar : DateSegmentInfo?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CalendarViewControllerPresenter.sharedInstance.attachView(view: self)

        self.tableViewProduct.separatorColor = .clear
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        self.setupDisplay()
        self.registerNotification()
    }
    
    func setupDisplay() {
        self.navigationItem.title = "Calendar"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: FontFamilyName.ClanOTMedium, size: 14) ?? UIFont.systemFontSize]
        
        self.picker = UIPickerView()
        self.picker.delegate = self
        self.picker.dataSource = self
        self.yearTextField.inputView = picker
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        
        toolBar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.yearTextField.inputView = picker
        self.yearTextField.inputAccessoryView = toolBar
        
    }
    
    @objc func cancelPicker(){
        self.yearTextField.resignFirstResponder()
    }
    
    @objc func donePicker(){
        if self.selectedYear == nil || self.selectedYear == 0 {
            self.yearTextField.resignFirstResponder()
            return
        }
        self.yearTextField.text = String(self.selectedYear ?? 0)
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from: self.visibleDateOnCalendar?.monthDates.first?.date ?? Date())
        dateComponents.year = selectedYear
        
        let date: Date = calendar.date(from: dateComponents)!
        self.calendarView.scrollToDate(date, triggerScrollToDateDelegate: true, animateScroll: true, preferredScrollPosition: nil, extraAddedOffset: 0) {
            
        }
        self.yearTextField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setSelectedDate(date: Date){
        self.calendarView.selectDates([date])
    }
    
    @IBAction func nextMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.next)
    }
    
    @IBAction func prevMonthButtonAction(_ sender: Any) {
        self.calendarView.scrollToSegment(.previous)
    }

    // MARK: Presenter delegate
    func startLoading(text : String) {
        self.showLoadingIndicator(text: text)
    }
    
    func finishLoading() {
        self.hideLoadingIndicator()
    }
    
    func setProductData(data: CalendarData) {
        self.calendarData = data
       
        self.setTableHeight()
        
        for date in (self.calendarData?.my_product_list)!{
            if date.immediate_service.status {
                self.activeDate.append(convertUnixTimeStamp(timestamp: date.immediate_service.date) )
            }
            else{
                self.activeDate.append(convertUnixTimeStamp(timestamp: date.next_service) )
            }
            
        }
        self.calendarView.reloadData()
    }
    
    func setErrorMessageFromAPI(errorMessage: String) {
        self.basicAlertView(title: "", message: errorMessage, successBlock: {})
    }
    
    func setTableHeight() {
        self.view.layoutIfNeeded()
        self.tableViewConstraint.constant = 600
        self.tableViewProduct.reloadData()
        self.tableViewProduct.layoutIfNeeded()
        
        let height : CGFloat = self.tableViewProduct.contentSize.height
        
        self.tableViewConstraint.constant = height
    }
    
    func loadData(date: Date){
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let param = [
            "user_id": User.shared.id!,
            "latitude": "\(UserLocation.shared.userLocation?.coordinate.latitude ?? 0)",
            "longitude": "\(UserLocation.shared.userLocation?.coordinate.longitude ?? 0)",
            "date": dateFormatter.string(from:date)
        ]
        CalendarViewControllerPresenter.sharedInstance.load(parameter: param)
    }
}

extension CalendarViewController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        self.visibleDateOnCalendar = visibleDates
        
        self.setCurrentMonth(visibleDates: visibleDates)
        self.setPreviousMonth(visibleDates: visibleDates)
        self.setNextMonth(visibleDates: visibleDates)
        
    }
    
    func setCurrentMonth(visibleDates: DateSegmentInfo){
        guard let startDate = visibleDates.monthDates.first?.date else {
            monthLabel.text = ""
            yearTextField.text = ""
            return
        }
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = Calendar.current.component(.year, from: startDate)
        monthLabel.text = monthName
        yearTextField.text = String(year)
        self.loadData(date: startDate)
    }
    
    func setPreviousMonth(visibleDates: DateSegmentInfo){
        guard let prevDate = visibleDates.monthDates.first?.date else {
            prevMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:prevDate)
        dateComponents.month = dateComponents.month!-1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        prevMonthLabel.text = monthName
    }
    
    func setNextMonth(visibleDates: DateSegmentInfo){
        guard let nextDate = visibleDates.monthDates.first?.date else {
            nextMonthLabel.text = ""
            return
        }
        
        let calendar = Calendar.current
        
        var dateComponents: DateComponents = calendar.dateComponents([.day,.month,.year], from:nextDate)
        dateComponents.month = dateComponents.month!+1
        
        let date: Date = calendar.date(from: dateComponents)!
        
        let month = Calendar.current.dateComponents([.month], from: date).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        nextMonthLabel.text = monthName
    }
    
    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else { return }
        handleCellTextColor(view: myCustomCell, cellState: cellState)
        handleCellSelection(view: myCustomCell, cellState: cellState)
    }
    
    func handleCellSelection(view: CellView, cellState: CellState) {
        if cellState.isSelected {
            view.selectedView.backgroundColor = UIColor.red
            view.dayLabel.textColor = .white
        } else {
            view.selectedView?.backgroundColor = UIColor.white
            handleCellTextColor(view: view, cellState: cellState)
        }
        
    }
    
    func handleCellTextColor(view: CellView, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth{
            view.dayLabel.textColor = UIColor.black
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTMedium, size: 14)
        } else {
            view.dayLabel.textColor = UIColor.gray
            view.dayLabel.font = UIFont(name: FontFamilyName.ClanOTBook, size: 14)
        }
        
        for date in self.activeDate{
            if cellState.date.hasSame(.weekOfYear, .day, as: date) {
                view.selectedView.backgroundColor = UIColor.red
                view.dayLabel.textColor = UIColor.white
            }
        }
       
        switch cellState.day{
        case .sunday: view.dayLabel.textColor = UIColor.red
        case .monday: break
        case .tuesday: break
        case .wednesday: break
        case .thursday: break
        case .friday: break
        case .saturday: break
        }
    }

    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "cell", for: indexPath) as! CellView
        configureCell(view: cell, cellState: cellState)
        cell.dayLabel .text = cellState.text
        
        return cell
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        var startDate = Date()
        
        if self.calendarData?.my_product_list.count ?? 0 > 0{
            if (self.calendarData?.my_product_list[0].immediate_service.status)! {
                startDate = convertUnixTimeStamp(timestamp: self.calendarData?.my_product_list[0].immediate_service.date ?? 0)
            }
            else{
                startDate = convertUnixTimeStamp(timestamp: self.calendarData?.my_product_list[0].next_service ?? 0)
            }
            
        }
        
        let endDate = formatter.date(from: "2100 02 01")!
        
        let startYear = Calendar.current.component(.year, from: startDate)
        let endYear = Calendar.current.component(.year, from: endDate)
        for i in Int(startYear)..<Int(endYear)+1{
            self.yearList.append(i)
        }
        
        var myCalendar = Calendar(identifier: .gregorian)
        myCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
        myCalendar.locale = Calendar.current.locale
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: myCalendar)
        return parameters
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
       
        return false
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        
        return false
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
        print(date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        calendarView.viewWillTransition(to: size, with: coordinator, anchorDate: iii)
    }
}

extension CalendarViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.calendarData?.my_product_list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarTableViewCell") as! CalendarTableViewCell
        formatter.dateFormat = "dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        var dateUnix = self.calendarData?.my_product_list[indexPath.row].next_service ?? 0
        var serviceInfo = self.calendarData?.my_product_list[indexPath.row].service_info ?? ""
        
        if self.calendarData?.my_product_list[indexPath.row].immediate_service.status ?? false {
            dateUnix = self.calendarData?.my_product_list[indexPath.row].immediate_service.date ?? 0
            serviceInfo = self.calendarData?.my_product_list[indexPath.row].immediate_service.notes ?? ""
        }
        
        cell.dateLabel.text = formatter.string(from: convertUnixTimeStamp(timestamp: dateUnix))
        formatter.dateFormat = "MMM"
        cell.monthLabel.text = formatter.string(from: convertUnixTimeStamp(timestamp: dateUnix))
        
        cell.productCodeLabel.text = self.calendarData?.my_product_list[indexPath.row].title ?? ""
        cell.productNumberLabel.text = self.calendarData?.my_product_list[indexPath.row].number ?? ""
        cell.noteLabel.text = serviceInfo
        
        cell.setImmediateStatus(status: self.calendarData?.my_product_list[indexPath.row].immediate_service.status ?? false)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}

extension CalendarViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(yearList[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row)
        self.selectedYear = yearList[row]
    }
}

extension CalendarViewController{
    // MARK: Keyboard Control
    func registerNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.removeGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
}

extension Date {
    func hasSame(_ components: Calendar.Component..., as date: Date, using calendar: Calendar = .autoupdatingCurrent) -> Bool {
        return components.filter { calendar.component($0, from: date) != calendar.component($0, from: self) }.isEmpty
    }
}
