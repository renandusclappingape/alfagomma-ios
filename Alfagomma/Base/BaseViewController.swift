//
//  BaseViewController.swift
//  iOS-Base
//
//  Created by Lutfi Azhar on 8/24/17.
//  Copyright © 2017 Lutfi Azhar. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var loadingHUD : Loading!
    var loadView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadView.frame = self.view.frame
        loadView.backgroundColor = .clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func basicAlertView(title: String, message: String, successBlock: (() -> Swift.Void)? = nil,retryBlock: (() -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        if retryBlock != nil{
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) in
                retryBlock!()
            }
            ))
        }
        else{
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                (alert: UIAlertAction!) in
                successBlock!()
            }
            ))
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func showLoadingIndicator(text: String) {
        self.view.addSubview(self.loadView)
        self.loadingHUD = Loading(text: text)
        self.view.addSubview(loadingHUD)
        loadingHUD.show()
    }
    
    func hideLoadingIndicator() {
        self.loadingHUD.hide()
        self.loadView.removeFromSuperview()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
