//
//  ViewControllerPresenter.swift
//  Alfagomma
//
//  Created by Clapping Ape on 21/05/18.
//  Copyright © 2018 Clapping Ape. All rights reserved.
//

import Foundation

struct TokenData{
    var access_token : String?
    var refresh_token : String?
    var token_type : String?
    var expires_in : Int?
}

protocol AuthView: NSObjectProtocol {
    func startLoading(text : String)
    func finishLoading()
    func setUserToken (data: TokenData)
    func setErrorMessageFromAPI(errorMessage: String)
}

class ViewControllerPresenter {
    
    weak fileprivate var loginView : AuthView?
    
    private init() {}
    static let sharedInstance = ViewControllerPresenter()
    
    func attachView(view: AuthView) {
        self.loginView = view
    }
    
    func detachView() {
        self.loginView = nil
    }
    
    func loadToken() {
        self.loginView?.startLoading(text : "Loading")
        
        APIService.sharedInstance.tokenRequestAPI(callBack: { [weak self](data) in
            self?.loginView?.finishLoading()
            self?.loginView?.setUserToken(data: TokenData(access_token: data.access_token, refresh_token: data.refresh_token, token_type: data.token_type, expires_in: data.expires_in))
        }) { (message: String) in
            self.loginView?.finishLoading()
            self.loginView?.setErrorMessageFromAPI(errorMessage: message)
        }
        
    }
    
}
